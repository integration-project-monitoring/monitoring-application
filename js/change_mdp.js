/**
 * Script en JQuery qui va vérifier que le champs mdp_old est bien rempli,
 * que les champs mdp_new et mdp_new2 sont bien identiques
 * Si les conditions sont remplies, on va rendre cliquable le bouton "btn_change_mdp"
 */

$(document).ready(function(){
    $("#mdp_new").keyup(validate);
    $("#mdp_new2").keyup(validate);
    $("#mdp_old").keyup(validate);

    var error = $("#error_message").text();

    if(error.length > 0){
        $("#error_message").removeClass("validate_status_ok");
        $("#error_message").addClass("validate_status_nok");
    }


});

function validate(){
    var password = $("#mdp_new").val();
    var password_confirm = $("#mdp_new2").val();

    if(password != password_confirm){
        $("#validate_status").removeClass("validate_status_ok");
        $("#validate_status").addClass("validate_status_nok");
        $("#validate_status").text("Les mots de passes doivent être identiques!");
        $("#btn_change_mdp").prop("disabled",true);
    }else{
        if(password.length > 0){
            $("#validate_status").removeClass("validate_status_nok");
            $("#validate_status").addClass("validate_status_ok");
            $("#validate_status").text("");
            var mdp_old = $("#mdp_old").val();
            if(mdp_old.length > 0){
                $("#btn_change_mdp").prop("disabled",false);
            }
        }
    }
}