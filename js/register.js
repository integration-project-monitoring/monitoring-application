/**
 * Script en JQuery qui va vérifier que le champs username est bien rempli,
 * que les champs txt_mdp et txt_mdp_confirm sont bien identiques
 * Si les conditions sont remplies, on va rendre cliquable le bouton "btn_register"
 */

$(document).ready(function(){
    $("#txt_mdp_confirm").keyup(validate);
    $("#txt_username").keyup(validate);

    var error = $("#error_message").text();

    if(error.length > 0){
        $("#error_message").removeClass("validate_status_ok");
        $("#error_message").addClass("validate_status_nok");
    }


});

function validate(){
    var password = $("#txt_mdp").val();
    var password_confirm = $("#txt_mdp_confirm").val();

    if(password != password_confirm){
        $("#validate_status").removeClass("validate_status_ok");
        $("#validate_status").addClass("validate_status_nok");
        $("#validate_status").text("Les mots de passes doivent être identiques!");
        $("#btn_register").prop("disabled",true);
    }else{
        if(password.length > 0){
            $("#validate_status").removeClass("validate_status_nok");
            $("#validate_status").addClass("validate_status_ok");
            $("#validate_status").text("");
            var username = $("#txt_username").val();
            if(username.length > 0){
                $("#btn_register").prop("disabled",false);
            }
        }
    }
}