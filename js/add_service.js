
/**
 * Script qui va gérer l'ajout dynamique de champs de formulaire pour l'ajout de services
 * Quand on clique sur "ajouter un n-ieme service" on affiche un champ supplémentaire dans le formulaire
 */

let nbDiv = 1;

const btn = document.getElementById("btn_dynamic"); 

btn.addEventListener("click", () =>{
    const tmp = nbDiv+2;
    const str = tmp + "eme service";
   
    /*
    if(btn.innerText === "2eme service"){
        btn.innerText = "3eme service";
    }
    */

    btn.innerText = str;
    

    if(nbDiv<=5){
        let name_div = "div".concat('',nbDiv.toString());
        togglediv(name_div);
        nbDiv++;
        btn.disabled =  nbDiv == 6 ? true : false ;
    }
})



function togglediv (id) {
    var div = document.getElementById(id);

    div.style.display = div.style.display == "none" ? "block" : "none";
}

