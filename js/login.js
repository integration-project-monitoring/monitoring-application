/**
 * Script qui va rendre visible le message d'erreur s'il est instancié 
 */

$(document).ready(function(){
    
    var error = $("#error_message").text();
    console.log(error);
    if(error.length > 0){
        $("#error_message").removeClass("validate_status_ok");
        $("#error_message").addClass("validate_status_nok");
    }


});