/**
 * Script qui va gérer l'affichage des alertes quand on veut supprimer un élément de la base de données
 */



/**
 * Fonction qui va afficher une alerte lorsqu'un utilisateur voudra supprimer un archipel standalone
 * @param {int} id l'id de l'archipel standalone
 * @param {string} name_archipel le nom de l'archipel standalone
 */
function suppress_standalone (id, name_archipel) {
    var r = confirm("Voulez vous vraiment supprimer ArchiPEL " + name_archipel + " ? ");

    if(r == true){
        location.href='suppress_apl_standalone.php?id=' + id;
    }
}

/**
 * Fonction qui va afficher une alerte lorsqu'un utilisateur voudra supprimer un archipel HA
 * @param {int} id l'id de l'archipel HA
 * @param {string} name_archipel le nom de l'archipel HA
 */
function suppress_ha (id, name_archipel) {
    var r = confirm("Voulez vous vraiment supprimer ArchiPEL " + name_archipel + " ? ");

    if(r == true){
        location.href='suppress_apl_ha.php?id=' + id;
    }
}

/**
 * Fonction qui va afficher une alerte lorsqu'un utilisateur voudra supprimer une vm 
 * Uniquement si aucun archipel ou reef n'est lié à la vm 
 * @param {int} id L'id de la vm
 * @param {string} name_vm Le nom de la vm
 * @param {boolean} is_linked_to_archipel Booléen qui est à True si un archipel est lié à la vm 
 * @param {boolean} is_linked_to_reef Booléen qui est à True si un reef est lié à la vm 
 */
function suppress_vm (id, name_vm, is_linked_to_archipel, is_linked_to_reef) {
    
    if(is_linked_to_archipel || is_linked_to_reef){
        alert("VEUILLEZ SUPPRIMER LES ARCHIPELS OU REEF LIES A CETTE VM AVANT DE LA SUPPRIMER");

    }
    else{
        var r = confirm("Voulez vous vraiment supprimer ArchiPEL " + name_vm + " ? ");

        if(r == true){
            location.href='suppress_vm.php?id=' + id;
        }
    }
}

/**
 * Fonction qui va afficher une alerte lorsqu'un utilisateur voudra supprimer un reef HA
 * @param {int} id L'id du reef HA 
 * @param {boolean} name_reef Le nom du reef HA
 */
function suppress_reef_ha(id, name_reef){
    var r = confirm("Voulez vous vraiment supprimer Reef " + name_reef + " ? ");

    if(r == true){
        location.href = 'suppress_reef_ha.php?id=' + id;
    }
}

