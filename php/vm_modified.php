<?php 

/**
 * Page qui va construire la requete et indiquer si l'archipel a bien ete ajouté
 * On va dans un premier temps construire la requete, avec le parsage des champs entrés dans la derniere page,
 * puis ensuite on fera un petit affichage indiquant a l'utilisateur si c'est OK ou KO
 */


require_once "library_monitoring.php";

session_start();

if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
        if(isset($_GET["id"])){
            $current_id = $_GET["id"];
        }else{
            header("Location: vms_overview.php");
            exit;
        }
    }else{
        header("Location: right_error.php");
        exit;
    }
}else{
    if(isset($_GET["id"])){
        $current_id = $_GET["id"];
        header("Location: login.php?errno=modify_vm&id=$current_id");
        exit;
    }else{
        header("Location: vms_overview.php");
        exit;
    }
    
    $username ="";
    $is_connected = 0;
}

print_head('Ajout de VM - EPI ', 'monitoring.css');

$pdo = connectToBdd();
$current_vm = get_vm($pdo, $current_id);

 echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

    print_header();

    print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">';


 //On commence par parser la requete 

 //1. on extrait tous les inputs utilisateurs 

 if(strlen($_POST["txt_name_group"])>0){
    $name_group = "'" . addslashes(htmlentities($_POST["txt_name_group"])) . "'";
    $id_group = get_group_id_from_name($pdo, $name_group);
 }else{
     $id_group = "NULL";
 }
 

 $name_lab = "'" . addslashes(htmlentities($_POST["txt_lab"])) . "'";

 $id_lab = (int) filter_var($name_lab,FILTER_SANITIZE_NUMBER_INT);

 if($id_lab==0){
     $id_lab = "NULL";
 }

 $name_vm = "'" . addslashes(htmlentities($_POST["txt_name_vm"])) . "'";

 $ip = "'" . addslashes(htmlentities($_POST["txt_ip_vm"])) . "'";

 $type_vm = "'" . addslashes(htmlentities($_POST["txt_type_vm"])) . "'";

 $port_ssh = "'" . addslashes(htmlentities($_POST["txt_ssh_vm"])) . "'";

 if(strlen($_POST["txt_portauto_vm"]>0)){
    $port_auto = "'" . addslashes(htmlentities($_POST["txt_portauto_vm"])) . "'";    
 }else{
     $port_auto = "NULL";
 }
 
 $sql = "UPDATE VMS 
        SET name_vm=$name_vm, ip=$ip, vm_type=$type_vm, id_grp=$id_group, id_lab=$id_lab, port_ssh=$port_ssh, port_test_auto=$port_auto
        WHERE  id_vm=$current_vm->id";
 
 try{
    $pdo->query($sql); 

    
   echo "<h3>La VM <strong>$name_vm</strong> a bien été modifiée ! </h3>", 
       '<p>Vous allez être redirigé vers la liste des vms dans <strong id="countdown">5 secondes</strong></p><br>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';

   echo '<script>', 
           'let i = 5;', 
           'let tmp = setInterval(countdown, 1000);',
           'function countdown() {',
               'if(i!=0) {',
                   'document.getElementById("countdown").innerHTML = i + " secondes";',
                   'i = i -1; ',
               '}',
               'else {',
                   'window.location.replace("vms_overview.php");',
               '}',
           '}',
       '</script>';
   
   print_scripts();

   echo '</body>','</htlm>';
}catch (Exception $e){

   echo "<h3>Il y a eu une erreure technique dans la modification de la VM $name_vm</h3>", 
       '<p>La commande est : ',$sql,'<br>Veuillez contacter le développeur en lui copiant collant cette commande pour voir quel est le soucis </p>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';


   print_scripts();

   echo '</body>','</htlm>';
}
