<?php 

require_once "library_monitoring.php";

session_start();


if(isset($_SESSION["username_logged"])){
    $username = $_SESSION["username_logged"];
    $is_connected = 1;
}else{
    $username ="";
    $is_connected = 0;
}

//Affichage de l'entete en html 
print_head('VMs - EPI','monitoring.css');

//Connexion a la base de donnee 
$pdo = connectToBdd();
$array_vm = get_array_vm($pdo);



echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

print_header($is_connected,$username);

print_sidebar();

echo '<div class="content-wrapper">',
        '<section class="content-header">',
            '<h1>VMs EPI</h1>',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3 class="box-title">Liste des différentes VM EPI et leur informations</h3>',
            '<button class = "btn btn_monitoring btn_add" onclick="location.href=\'add_vm.php\';"> Ajouter une VM</button>',
        '</div>',
        '<div class="box-body table-responsive no-padding">',
            '<table class="table table-stripped table-condensed">';

print_thead_vms();


echo '<tbody>'; 

foreach ($array_vm as $item){
    print_tbody_vm($item, $pdo);
}


echo '</tbody>';

echo '</table>',
'</div>',
'</div>',
'</section>';

echo '</div>',
'</div>';




//Finalement, on inclue les scripts
print_scripts();

echo '</body>','</html>';