<?php 

/**
 * Page qui va afficher un formulaire pour ajouter une nouvelle VM 
 * Champs nécessaires : 
 *      - Le lab auquel fait parti la VM (Toujours de la forme "LABx" Si la VM est hors lab ça sera "NO LAB")
 *      - Le nom de la VM
 *      - L'IP de la VM (pas celle du lab)
 *      - Le type de la VM ("reef";"archipel"...)
 *      - Port SSH de la VM
 * Champs falcultatifs : 
 *      - Le groupe auquel fait parti la VM
 *      - Port sur lequel le sensor de tests automatiques écoute
 */

//On inclut la librairie de fonctions 
require_once "library_monitoring.php";

//On initialise la session
session_start();

//Vérification du rôle de l'utilisateur connecté 
// Roles acceptés : "integrator" ; "administrator"
//Si l'utilisateur est bien connecté, et a les bons droits, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
    }else{
        //L'utilisateur n'a pas les bons droits
        header("Location: right_error.php");
        exit;
    }
}else{
    //L'utilisateur n'est pas connecté
    header("Location: login.php?errno=add_vm");
    exit;
}


//Affichage de l'entete html 
print_head('Ajout VM - EPI', 'monitoring.css'); 

//Connexion à la base de données 
$pdo = connectToBdd(); 

//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();

echo '<div class="content-wrapper">',
        '<section class="content-header">',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3>Ajout d\'une VM :</h3>';

//DEBUT DU FORMULAIRE 
echo '<form action="vm_added.php" method="post">';

//Champs txt_name_group --> nom du groupe auquel fait parti la VM  
echo    '<label class="label_form">De quel groupe fait parti la VM ? (Laisser vide si standalone) </label> <input type="text" name="txt_name_group" id="txt_name_group" list="list_name_group">';
    
//On va récupérer dans un tableau tous les groupes 
$array_group = get_array_group($pdo);
    
//On va créer la datalist dans laquelle on a tous les noms de groupes d'inscrits
echo '<datalist id=list_name_group>';
    
    foreach ($array_group as $item){
        echo '<option>' , $item , '</option>';
    }
    
echo '</datalist><span style="margin-left:10px">Vous ne trouvez pas votre groupe ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_group.php\';">L\'ajouter</button> <br>';
    
//Champs txt_lab --> nom du lab dans lequel se trouve la vm ("LABx" ou "NO LAB" pour une VM hors lab)
echo '<label class="label_form required">De quel lab fait parti la VM ? ("NO LAB" pour une vm qui est hors lab) </label>', 
    '<input required type="text" name="txt_lab" id="txt_lab" list="list_lab">';

//On va récupérer dans un tableau tous les labs
$array_lab = get_array_lab($pdo);

//On va créer la datalist dans laquelle on a tous les noms de labs d'inscrits
echo '<datalist id=list_lab>'; 


foreach ($array_lab as $item){
    echo '<option>', $item, '</option>';
}

echo '<option>NO LAB</option></datalist>';

echo '<span style="margin-left:10px">Vous ne trouvez pas votre lab ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_lab.php\';">L\'ajouter</button> <br>';

//Champs txt_name_vm--> Nom de la vm
echo     '<label class="label_form required">Quel est le nom de la VM ? (Par exemple rd-srv528) </label>',
        '<input required type="text" name="txt_name_vm" id="txt_name_vm"></br>';            
    
//Champs txt_ip_vm --> IP de la vm (pas celle du lab)
echo     '<label class="label_form required">Quel est l\'ip de la VM ? </label>',
        '<input required type="text" name="txt_ip_vm" id="txt_ip_vm"></br>';      

//Champs txt_type_vm --> Type de la vm ("archipel";"reef"...)
echo '<label class="label_form required">Quel est le type de la VM ? </label>', 
    '<input required type="text" name="txt_type_vm" id="txt_type_vm" list="list_type"><br>';

//On va récupérer dans un tableau toutes les VMs
$array_type_vm = get_array_type_vm($pdo);

//On va créer la datalist dans laquelle on a tous les noms de VMs d'inscrits
echo '<datalist id=list_type>'; 

foreach ($array_type_vm as $item){
    echo '<option>', $item, '</option>';
}

echo '</datalist>';

//Champs txt_ssh_vm --> Port SSH de la VM
echo     '<label class="label_form required">Quel est le port ssh de la VM ? </label>',
        '<input required type="text" name="txt_ssh_vm" id="txt_ssh_vm"></br>';    

//Champs txt_portauto_vm --> port sur lequel le sensor de test automatiques écoute
echo     '<label class="label_form ">Quel est le port de test auto de la VM ? </label>',
        '<input type="text" name="txt_portauto_vm" id="txt_portauto_vm"></br>';    

echo '<button class="btn btn_monitoring"> Ajouter la vm </button> ';

echo '</form>';


echo '</div>';

echo '</div>',
'</section>',
'</div>',
'</div>';

//On inclue les scripts JS (mis à la fin pour l'optimisation)
print_scripts();

echo '</body>', '</html>';