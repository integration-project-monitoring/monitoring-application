<?php 

require_once "library_monitoring.php";

session_start();

if(isset($_SESSION["username_logged"])){
    $username = $_SESSION["username_logged"];
    $is_connected = 1;
}else{
    $username ="";
    $is_connected = 0;
    header("Location: login.php?errno=my_account");
    exit;
}

print_head("Monitoring - EPI", "monitoring.css");

$pdo = connectToBdd();

echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

    print_header($is_connected,$username);

    print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">';

$old_mdp = htmlentities($_POST["mdp_old"]);
$new_mdp = htmlentities($_POST["mdp_new"]);
$new_mdp2 = htmlentities($_POST["mdp_new2"]);

$is_old_mdp_valid = false;

$sql = "SELECT * FROM USERS WHERE username_user='$username'";
$sql_uncritical = "";

try{
    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    
    if($pdostat->rowCount()==0){
        header("Location: login.php");
        exit;
    }

    foreach($pdostat as $item){
        $is_old_mdp_valid = password_verify($old_mdp,$item["mdp_user"]);
        if($is_old_mdp_valid){
            $new_hashed_mdp = password_hash($new_mdp,PASSWORD_DEFAULT);
            $sql = "UPDATE USERS SET mdp_user='$new_hashed_mdp' WHERE username_user='$username'";

            $sql_uncritical = str_replace($new_hashed_mdp,"",$sql);
            
            $pdo->query($sql);

            echo "<h3>Votre mot de passe a bien été modifié!</h3>", 
            '<p>Vous allez être redirigé vers l\'accueil dans <strong id="countdown">5 secondes</strong></p><br>';

        echo '</div>';

        echo '</div>',
        '</section>',
        '</div>',
        '</div>';

        echo '<script>', 
           'let i = 5;', 
           'let tmp = setInterval(countdown, 1000);',
           'function countdown() {',
               'if(i!=0) {',
                   'document.getElementById("countdown").innerHTML = i + " secondes";',
                   'i = i -1; ',
               '}',
               'else {',
                   'window.location.replace("reef_ha_overview.php");',
               '}',
           '}',
       '</script>';
   
   print_scripts();

   echo '</body>','</htlm>';

        }else{
            header("Location: myaccount.php?errno=1");
            exit;
        }
    }
}catch (Exception $e){
    echo "<h3>Il y a eu une erreure technique dans la modification du mdp</h3>", 
       '<p>La commande est : ',$sql_uncritical,'<br>Veuillez contacter le développeur en lui copiant collant cette commande pour voir quel est le soucis </p>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';


   print_scripts();

   echo '</body>','</htlm>';
}

