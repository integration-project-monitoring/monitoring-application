<?php

//require_once les classes PHP
require_once "classes/Archipel.php";
require_once "classes/Hsm.php";
require_once "classes/VM.php";
require_once "classes/ServiceApl.php";
require_once "classes/Reef.php";
require_once "classes/Comment.php";

/**
 * BIBLIOTHEQUE DE FONCTIONS POUR LE MONITORING D'INFOS SERVEURS
 */

/**
 * Fonction qui va retourner vers la sortie standard le code de l'entete HTML
 * @param string $titre Le titre de la page
 * @param string $css Le nom du css que l'on va utiliser (il sera accessible sur "../css/$css")
 * 
 * @return void
 */
function print_head($titre = '', $css = '')
{
    $titre = htmlentities($titre, ENT_COMPAT, 'ISO-8859-1');

    echo '<!DOCTYPE html>',
    '<html lang=fr>',
    '<head>',
    '<meta charset="UTF-8">',
    '<meta http-equiv="X-UA-Compatible" content="IE=edge"/>',
    '<title>', $titre, '</title>',
    '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>',
        //CSS
    '<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />',
    '<link href="../jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css"/>',
    '<link rel="stylesheet" href="../css/ionicons.min.css"/>',
    '<link rel="stylesheet" href="../css/AdminLTE.min.css"/>',
    '<link rel="stylesheet" href="../css/skin-black.min.css"/>',
    '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,600italic"/>',
        //NOTRE CSS
    "<link href='../css/$css' rel='stylesheet' type='text/css'>",
    '</head>';
}

/**
 * Fonction qui va retourner vers la sortie standard le code d'inclusion de script
 * (a mettre en fin de code pour l'optimisation)
 * 
 * @return void
 */

function print_scripts(){
    echo '<script src="../js/jquery-3.6.0.min.js"></script>',
    '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>',
    '<script src="../js/bootstrap.min.js"></script>',
    '<script src="../js/adminlte.min.js"></script>';
}

/**
 * Fonction qui se connecte avec la base de données
 * 
 * @return PDO L'objet PDO representant la connexion avec la BD
 */
function connectToBdd()
{
    $dsn = "mysql:dbname=monitoring_epi;host=172.16.105.52";
    $user = "vis";
    $pass = "ArchiPEL$1";
    $pdo = new PDO($dsn, $user, $pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
}


/**
 * Fonction qui va retourner vers la sortie standard le header principale
 *
 * @param  boolean $is_connected
 * @param  string $username
 * @return void
 */
function print_header($is_connected=0, $username=""){
    echo '<header class="main-header">',
            '<a href="new_index.php" class="logo">',
                '<span class="logo-mini"><b>EPI</b></span>',
                '<span class="logo-lg"><img src="../img/logo_apl_little.png" alt="Logo EPI" width="45px"/></span>',
            '</a>';

    echo '<nav class="navbar navbar-static-top" role="navigation">',
            '<a href="#" class="ion-android-more-horizontal btn-toggle" data-toggle="push-menu" role="button">',
                '<span class="sr-only"></span>',
            '</a>';

    //Si on est connecté, on affichera le bouton "mon compte" au lieu du bouton "connexion"
    if($is_connected){
        echo "<a href=\"myaccount.php\" class=\"mx-2\"><i class=\"ion-ios-contact\"></i>Mon Compte</a>";
        echo "<a href=\"logout.php\" class=\"mx-2\"><i class=\"ion-log-out\"></i></a>";
    }else {
        echo '<button class="btn btn_monitoring btn_login" onclick="location.href=\'login.php\'">CONNEXION</button>';
    }
    
    
    echo '</nav>',
     '</header>';
}


/**
 * Fonction qui va retourner vers la sortie standard la sidebar
 * 
 * @return void
 */

function print_sidebar(){
    echo '<aside class="main-sidebar">',
            '<section class="sidebar">',
                '<ul class="sidebar-menu" data-widget="tree">',
                    '<li class="header">NAVIGATION</li>',
                    '<li>',
                        '<a href="vms_overview.php"><i class="ion-android-laptop"></i><span>Machines Virtuelles</span></a>',
                    '</li>',

                    '<li class="treeview">',
                        '<a href="#"><i class="ion-card"></i><span>ArchiPEL</span>',
                            '<span class="pull-right-container">',
                                '<i class="pull-right"></i>',
                            '</span>',
                        '</a>',
                        '<ul class="treeview-menu">',
                            '<li><a href="new_index.php">HA</a></li>',
                            '<li><a href="apl_standalone_overview.php">Standalone</a></li>',
                        '</ul>',
                    '</li>',

                    '<li class="treeview">',
                        '<a href="#"><i class="ion-android-cloud-circle"></i><span>Reef</span>',
                            '<span class="pull-right-container">',
                                '<i class="pull-right"></i>',
                            '</span>',
                        '</a>',
                        '<ul class="treeview-menu">',
                            '<li><a href="reef_ha_overview.php">HA</a></li>',
                            '<li><a href="#">Standalone</a></li>',
                        '</ul>',
                    '</li>',

                    '<li>',
                        '<a href="#"><i class="ion-android-settings"></i><span>Outillage</span></a>',
                    '</li>',
                '</ul>',
            '</section>',
        '</aside>';

}


/**
 * Fonction qui va retourner vers la sortie standard l'entete n°1 de la page ArchiPEL details
 * Contient : 
 * - Nom ArchiPEL
 * - (vide) pour l'icone de monitoring
 * - Nom stack docker 
 * - Lien IHM
 * - LAB
 * - VM1
 * - VM2
 * - VM3
 * - Schémas oracle
 * - Instance oracle
 * - UUID Publication 
 * - Version APL
 * - Version Core
 * - Version Java
 * - Docker compose 
 * - Path installation 
 * @return void
 */
function print_thead1_archipel_ha_details(){
    echo '<thead>',
    '<tr>',
    '<th>Nom ArchiPEL</th>',
    '<th></th>',
    '<th>Nom stack docker</th>',
    '<th>IHM</th>',
    '<th>LAB</th>',
    '<th>vm1</th>',
    '<th>vm2</th>',
    '<th>vm3</th>',
    '<th>Schémas Oracle</th>',
    '<th>Instance Oracle</th>',
    '<th>UUID Publication</th>',
    '<th>Version APL</th>',
    '<th>Version Core</th>',
    '<th>Version Java</th>',
    '<th>HSM</th>',
    '<th>Docker compose</th>',
    '<th>Path Installation</th>',
    '</tr>',
    '</thead>';
}

/**
 * Fonction qui va print les premières entêtes de la page de détail d'ArchiPEL Standalone
 * Contient : 
 * - Nom ArchiPEL
 * - (vide) pour icone monitoring
 * - IHM
 * - LAB
 * - VM
 * - Schémas Oracle
 * - Instance Oracle
 * - UUID Publication
 * - Version APL
 * - Version Core
 * - Version Java
 * - HSM
 * - Path installation
 * 
 * @return void
 */
function print_thead1_archipel_standalone_details(){
    echo '<thead>',
    '<tr>',
    '<th>Nom ArchiPEL</th>',
    '<th></th>',
    '<th>IHM</th>',
    '<th>LAB</th>',
    '<th>VM</th>',
    '<th>Schémas Oracle</th>',
    '<th>Instance Oracle</th>',
    '<th>UUID Publication</th>',
    '<th>Version APL</th>',
    '<th>Version Core</th>',
    '<th>Version Java</th>',
    '<th>HSM</th>',
    '<th>Path Installation</th>',
    '</tr>',
    '</thead>';
}




/**
 * Fonction qui va print les deuxième lignes d'entêtes de la page de détail d'ArchiPEL HA
 * Contient : 
 * - IP Vm1
 * - SSH Vm1
 * - Port test auto Vm1
 * - IP Vm2
 * - SSH Vm2
 * - Port test auto Vm2
 * - IP Vm3
 * - SSH Vm3
 * - Port test auto Vm3
 * 
 * @return void
 */
function print_thead2_archipel_ha_details(){
    echo '<thead>',
    '<tr>',
    '<th>IP Vm1</th>',
    '<th>ssh vm1</th>',
    '<th>Port test auto vm1</th>',
    '<th>IP Vm2</th>',
    '<th>ssh vm2</th>',
    '<th>Port test auto vm2</th>',
    '<th>IP Vm3</th>',
    '<th>ssh vm3</th>',
    '<th>Port test auto vm3</th>',
        '</tr>',
    '</thead>';
}


/**
 * Fonction qui va print les deuxièmes lignes d'entêtes de la page de détail d'ArchiPEL Standalone
 * Contient : 
 * - IP Vm
 * - SSH Vm
 * - Port test auto Vm
 * @return void
 */
function print_thead2_archipel_standalone_details(){
    echo '<thead>',
    '<tr>',
    '<th>IP VM</th>',
    '<th>ssh VM</th>',
    '<th>Port test auto VM</th>',
    '</tr>',
    '</thead>';
}



/**
 * Fonction qui va print les troisièmes lignes d'entêtes de la page de détail d'ArchiPEL HA
 * Contient : 
 * - Version Nfast
 * - Version Docker
 * - Version GlusterFS
 * - Version Wildfly
 * - Version Synchro
 * - Version Ubuntu 
 * 
 * @return void
 */
function print_thead3_archipel_ha_details(){
    echo '<thead>',
    '<tr>',
    '<th>Version NFast</th>',
    '<th>Version docker</th>',
    '<th>Version GlusterFS</th>',
    '<th>Version Wildfly</th>',
    '<th>Version synchro</th>',
    '<th>Version Ubuntu</th>',
    '</tr>',
    '</thead>';
}

/**
 * Fonction qui va print les troisièmes lignes d'entêtes de la page de détail d'ArchiPEL standalone
 * Contient : 
 * - Version NFast
 * - Version Wildfly
 * - Version Synchro
 * - Version Ubuntu
 * 
 * @return void
 */
function print_thead3_archipel_standalone_details(){
    echo '<thead>',
    '<tr>',
    '<th>Version NFast</th>',
    '<th>Version Wildfly</th>',
    '<th>Version synchro</th>',
    '<th>Version Ubuntu</th>',
    '</tr>',
    '</thead>';
}


/**
 * Fonction va print les services dans la page de détails des ArchiPEL HA (les entêtes et les valeurs) 
 *
 * @param  Archipel $apl
 * @param  PDO $pdo
 * @return void
 */
function print_service_archipel_ha_details(Archipel $apl, PDO $pdo){
    echo '<thead>',
    '<tr>';



    $array_port = [];
    
    //1. On récupère tous les services associé à l'id d'ArchiPEL qu'on a en paramètre

    $sql = "SELECT * FROM GRP_APL_HA WHERE id_apl_associated = $apl->id";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    foreach ($pdostat as $item){
        //Pour chaque résultat on va print une entête avec le nom du service, et on va inserer dans $array_port le port correspondant
        echo '<th>'. $item["name_service"] . '</th>';
        array_push($array_port,$item["port_service"]);
    }
    echo '<th></th>';

    echo '</tr>',
    '</thead>';

    //Une fois toutes les entêtes printées, on va afficher tous les ports dans l'ordre
    echo '<tbody><tr>';

    foreach ($array_port as $item){
        echo '<td> Port '. $item . '</td>';
    }

    //Bouton d'ajout d'un service
    echo "<td><button class = \"btn btn_monitoring btn_add\" onclick=\"location.href='add_service_ha.php?id=$apl->id';\"> Ajouter un service</button></td>";

    echo '</tr></tbody>';



}


/**
 * Fonction qui va print un tableau pour afficher l'id d'un archipel dans les différentes pages de détails
 *
 * @param  Archipel $apl
 * 
 * @return void
 */
function print_id_archipel_details(Archipel $apl){
    echo '<thead>',
            '<tr>',
                '<th>ID APL</th>',
            '</tr>',
        '</thead>',
        '<tbody><tr>',
            "<td>$apl->id</td>",
        '</tr></tbody>';
}

/**
 * Retourne sur la sortie standard l'entête de tableau pour ArchiPEL HA (sur la page new_index.php par exemple)
 * Contient : 
 * - Nom ArchiPEL
 * - (vide) Pour icone monitored
 * - IHM
 * - LAB
 * - VM1
 * - VM2
 * - VM3
 * - Schémas Oracle
 * - Instance Oracle
 * - UUID Publication
 * - Version APL
 * - Version Core
 * - Version Java
 * - HSM
 * - Docker compose 
 * - Path installation
 * 
 * @return void
 */

function print_thead_archipel_ha(){
    echo '<thead>',
            '<tr>',
                '<th>Nom ArchiPEL</th>',
                '<th></th>',
                '<th>Nom stack docker</th>',
                '<th>IHM</th>',
                '<th>LAB</th>',
                '<th>vm1</th>',
                '<th>vm2</th>',
                '<th>vm3</th>',
                '<th>Schémas Oracle</th>',
                '<th>Instance Oracle</th>',
                '<th>UUID Publication</th>',
                '<th>Version APL</th>',
                '<th>Version Core</th>',
                '<th>Version Java</th>',
                '<th>HSM</th>',
                '<th>Docker compose</th>',
                '<th>Path Installation</th>',
            '</tr>',
        '</thead>';
}


/**
 * Retourne sur la sortie standard l'entête de tableau pour Reef HA
 * Contient : 
 * - Nom reef 
 * - (vide) pour icone monitored
 * - Nom stack docker 
 * - LAB
 * - VM1
 * - VM2
 * - VM3
 * - Version reef 
 * - Version Java 
 * - HSM 
 * - Docker compose 
 * - Path installation 
 * 
 * @return void
 */

function print_thead_reef_ha(){
    echo '<thead>',
            '<tr>',
                '<th>Nom Reef</th>',
                '<th></th>',
                '<th>Nom stack docker</th>',
                '<th>LAB</th>',
                '<th>vm1</th>',
                '<th>vm2</th>',
                '<th>vm3</th>',
                '<th>Version Reef</th>',
                '<th>Version Java</th>',
                '<th>HSM</th>',
                '<th>Docker compose</th>',
                '<th>Path Installation</th>',
            '</tr>',
        '</thead>';
}


/**
 * Fonction qui renvoie vers la sortie standard les entêtes relatives aux version de reef_ha
 * Contient : 
 * - Version Nfast 
 * - Version Ubuntu
 * - HSM Associé
 * 
 * TODO : PKI ASSOCIEE
 * 
 * @return void
 */
function print_thead_reef_ha_versions(){
    echo '<thead>',
            '<tr>',
                '<th>Version Nfast</th>',
                '<th>Version Ubuntu</th>',
                '<th>HSM Associé</th>',
                //TODO : PKI ASSOCIE 
            '</tr>',
        '</thead>';
}


/**
 * Fonction qui retourne vers la sortie standard les entêtes relatives aux ports pour reef HA 
 * Contient : 
 * - Port Neops
 * - Port Ethernet
 * - Port NSquare
 * - Port Simuhost
 *
 * @return void
 */
function print_thead_reef_ha_ports(){
    echo '<thead>',
            '<tr>',
                '<th>Port Neops</th>',
                '<th>Port ethernet</th>',
                '<th>Port NSquare</th>',
                '<th>Port Simuhost</th>',
            '</tr>',
        '</thead>';
}



/**
 * Retourne sur la sortie standard l'entête de tableau pour ArchiPEL Standalone (comme dans apl_standalone_overview.php)
 * Contient : 
 * - Nom ArchiPEL 
 * - (vide) pour icone monitored
 * - IHM 
 * - LAB 
 * - VM 
 * - Schémas Oracle 
 * - Instance Oracle 
 * - UUID Publication
 * - Version APL 
 * - Version Core
 * - Version Java
 * - HSM
 * - Path Installation 
 * 
 * @return void 
 */

function print_thead_archipel_standalone(){
    echo '<thead>',
    '<tr>',
    '<th>Nom ArchiPEL</th>',
    '<th></th>',
    '<th>IHM</th>',
    '<th>LAB</th>',
    '<th>VM</th>',
    '<th>Schémas Oracle</th>',
    '<th>Instance Oracle</th>',
    '<th>UUID Publication</th>',
    '<th>Version APL</th>',
    '<th>Version Core</th>',
    '<th>Version Java</th>',
    '<th>HSM</th>',
    '<th>Path Installation</th>',
    '</tr>',
    '</thead>';
}

/**
 * Retourne sur la sortie standard l'entête de tableau pour la liste des VMs (comme sur vms_overview.php)
 * Contient : 
 * - ID 
 * - Nom VM
 * - (vide) pour icone monitored
 * - IP 
 * - LAB 
 * - Port SSH
 * - Port test auto 
 * - Version Ubuntu 
 * - Docker 
 * - GlusterFS 
 * - Disque total 
 * - Disque restant 
 * - RAM 
 * 
 * @return void
 */

function print_thead_vms(){
    echo '<thead>',
    '<tr>',
    '<th>ID</th>',
    '<th>Nom VM</th>',
    '<th></th>',
    '<th>IP</th>',
    '<th>LAB</th>',
    '<th>Port SSH</th>',
    '<th>Port test auto</th>',
    '<th>Ubuntu</th>',
    '<th>Docker</th>',
    '<th>GlusterFS</th>',
    '<th>Disque total</th>',
    '<th>Disque restant</th>',
    '<th>RAM</th>',
    '</tr>',
    '</thead>';
}

/**
 * Retourne sur la sortie standard l'entête de tableau pour les details des VMs
 * Contient : 
 * - ArchiPEL associés
 * - Reef associés
 * 
 * @return void 
 */

function print_thead_vms_details(){
    echo '<thead>',
    '<tr>',
    '<th>ArchiPEL associés</th>',
    '<th>Reef associés</th>',
    '</tr>',
    '</thead>';
}



/**
 * Fonction qui récupère tous les archipel HA et les mets dans un tableau
 * 
 * @param PDO $pdo objet de connexion à la base
 * 
 * @return array Le tableau avec tous les archipel HA
 */
function get_array_apl_ha(PDO $pdo){
    $sql = "SELECT * FROM ARCHIPEL ORDER BY name_archipel ASC";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $array = array();
    foreach ($pdostat as $item){
        //A chaque résultat on va construire un archipel, puis s'il possède un docker compose, il est HA donc on le push dans le tableau
        $archipel = new Archipel($item["id_apl"],$item["name_archipel"],$item["name_stack"],$item["id_vm_associated"],$item["id_vm_associated2"],$item["id_vm_associated3"],$item["schemas_db"],$item["instance_db"],$item["uuid_publication"],$item["version"],$item["version_core"],$item["version_java"],$item["version_nfast"],$item["version_wildfly"],$item["version_ubuntu_apl"],$item["version_synchro"],$item["id_hsm_associated"],$item["url_ihm"],$item["docker_compose"],$item["installation_path"],$item["is_monitored"]);
        if($archipel->docker_compose != null){
            array_push($array,$archipel);
        }
    }

    return $array;
}

//667 MMS LDO EKIP EKIP EKIP 



/**
 * Fonction qui va retourner un tableau avec tous les archipel lié à une vm dont l'id est donné
 *
 * @param  PDO $pdo Objet de connexion à la DB 
 * @param  int $id_vm Id de la VM dont on veut avoir les archiPEL associés
 * 
 * @return array Le tableau contenant tous les ArchiPEL installés sur la VM 
 */
function get_array_apl_linked(PDO $pdo, $id_vm){
    $sql = "SELECT * FROM ARCHIPEL WHERE id_vm_associated=$id_vm OR id_vm_associated2=$id_vm OR id_vm_associated3=$id_vm ORDER BY name_archipel ASC";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $array = array();

    foreach($pdostat as $item){
        //On construit l'ArchiPEL puis ensuite on le push dans le tableau que l'on va retourner
        $archipel = new Archipel($item["id_apl"],$item["name_archipel"],$item["name_stack"],$item["id_vm_associated"],$item["id_vm_associated2"],$item["id_vm_associated3"],$item["schemas_db"],$item["instance_db"],$item["uuid_publication"],$item["version"],$item["version_core"],$item["version_java"],$item["version_nfast"],$item["version_wildfly"],$item["version_ubuntu_apl"],$item["version_synchro"],$item["id_hsm_associated"],$item["url_ihm"],$item["docker_compose"],$item["installation_path"], $item["is_monitored"]);
        array_push($array, $archipel);
    }

    return $array;
}

/**
 * Fonction qui va retourner un tableau avec tous les reef liés à une vm dont l'id est donné
 *
 * @param  PDO $pdo Objet de connexion à la DB
 * @param  int $id_vm Id de la VM dont on veut avoir les reefs associés
 * 
 * @return array Le tableau contenant tous les reefs installés sur la VM
 */
function get_array_reef_linked(PDO $pdo, $id_vm){
    $sql = "SELECT * FROM REEF WHERE id_vm_associated=$id_vm OR id_vm_associated2=$id_vm OR id_vm_associated3=$id_vm ORDER BY name_reef ASC";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $array = array();

    foreach($pdostat as $item){
        //On construit le reef puis ensuite on le push dans le tableau que l'on va retourner
        $reef = new Reef($item["id_reef"],$item["id_vm_associated"], $item["id_vm_associated2"], $item["id_vm_associated3"], $item["name_reef"], $item["name_stack"], $item["port_neops"], $item["port_ethernet"], $item["port_nsquare"], $item["port_simuhost"], $item["version_reef"], $item["version_java"], $item["version_nfast"], $item["version_ubuntu_reef"], $item["id_pki_associated"], $item["id_hsm_associated"], $item["docker_compose"], $item["installation_path"], $item["type"], $item["is_monitored"]);
        array_push($array,$reef);
    }

    return $array;
}

/**
 * Fonction qui récupère tous les reefs HA et les mets dans un tableau
 * 
 * @param PDO $pdo objet de connexion à la base 
 * 
 * @return array Le tableau avec tous les reefs HA 
 */

 function get_array_reef_ha(PDO $pdo){
     $sql = "SELECT * FROM REEF ORDER BY name_reef ASC";

     $pdostat = $pdo->query($sql);
     $pdostat->setFetchMode(PDO::FETCH_ASSOC);

     $array = array();
     foreach($pdostat as $item){
         //On construit le reef, on vérifie s'il a un docker compose, si c'est le cas alors c'est un HA, on le push dans le tableau
         $reef = new Reef($item["id_reef"],$item["id_vm_associated"], $item["id_vm_associated2"], $item["id_vm_associated3"], $item["name_reef"], $item["name_stack"], $item["port_neops"], $item["port_ethernet"], $item["port_nsquare"], $item["port_simuhost"], $item["version_reef"], $item["version_java"], $item["version_nfast"], $item["version_ubuntu_reef"], $item["id_pki_associated"], $item["id_hsm_associated"], $item["docker_compose"], $item["installation_path"], $item["type"], $item["is_monitored"] );
         if($reef->docker_compose != null){
             array_push($array, $reef);
         }
     }

     return $array;
 }


 
/**
 * Fonction qui récupère tous les archipel Standalone et les mets dans un tableau
 * 
 * @param PDO $pdo objet de connexion à la base
 * 
 * @return array Le tableau avec tous les archipel HA
 */
function get_array_apl_standalone(PDO $pdo){
    $sql = "SELECT * FROM ARCHIPEL ORDER BY name_archipel ASC";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $array = array();
    foreach ($pdostat as $item){
        //On construit l'archipel, on vérifie qu'il n'a pas de docker compose, s'il n'en a pas, alors c'est un standalone, on le push dans le tableau
        $archipel = new Archipel($item["id_apl"],$item["name_archipel"],$item["name_stack"],$item["id_vm_associated"],$item["id_vm_associated2"],$item["id_vm_associated3"],$item["schemas_db"],$item["instance_db"],$item["uuid_publication"],$item["version"],$item["version_core"],$item["version_java"],$item["version_nfast"],$item["version_wildfly"],$item["version_ubuntu_apl"],$item["version_synchro"],$item["id_hsm_associated"],$item["url_ihm"],$item["docker_compose"],$item["installation_path"], $item["is_monitored"]);
        if($archipel->docker_compose == null){
            array_push($array,$archipel);
        }
    }

    return $array;
}

/**
 * Fonction qui récupère toutes les VM et les mets dans un tableau
 * 
 * @param PDO $pdo objet de connexion à la base
 * 
 * @return array Le tableau avec toutes les VMs
 */
function get_array_vm(PDO $pdo){
    $sql = "SELECT * FROM VMS ORDER BY name_vm ASC";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $array = array();
    foreach ($pdostat as $item){
        //On contruit la vm puis on la push dans le tableau
        $vm = new VM($item["id_vm"],$item["name_vm"],$item["ip"],$item["id_lab"],$item["port_ssh"],$item["port_test_auto"],$item["version_ubuntu"],$item["version_docker"],$item["version_glusterfs"],$item["total_disk"],$item["free_disk"],$item["total_ram"], $item["id_grp"], $item["vm_type"], $item["is_monitored"]);
        array_push($array,$vm);
    }
    return $array;
}

/**
 * Fonction qui récupère tous les labs et les mets dans un tableau
 * 
 * @param PDO $pdo objet de connexion à la base
 * 
 * @return array Le tableau avec tous les labs
 */
function get_array_lab(PDO $pdo){
    $sql = "SELECT * FROM LABS ORDER BY name_lab ASC";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $array = array();
    foreach ($pdostat as $item){
        array_push($array,$item["name_lab"]);
    }
    return $array;
}

/**
 * Fonction qui récupère tous les HSM et les mets dans un tableau
 * 
 * @param PDO $pdo objet de connexion à la base
 * 
 * @return array Le tableau avec tous les HSM
 */
function get_array_hsm(PDO $pdo){
    $sql = "SELECT * FROM HSM ORDER BY id_hsm ASC";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $array = array();
    foreach ($pdostat as $item){
        array_push($array,new Hsm($item["id_hsm"],$item["ip_hsm"],$item["port_hsm"],$item["version_hsm"],$item["label_hsm"]));
    }
    return $array;
}

/**
 * Fonction qui récupère l'id d'un groupe dont on a le nom, et qui le retourne 
 * 
 * @param PDO $pdo objet de connexion à la base
 * @param String $name_group le nom du groupe
 *  
 * @return int l'id du groupe donné 
 */
function get_group_id_from_name(PDO $pdo, $name_group){
    $sql = "SELECT id_grp FROM VMS_GROUPS WHERE name_grp=$name_group";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    $id = 0;
    foreach($pdostat as $item){
        $id = $item["id_grp"];
        return $id;
    }

}

/**
 * Fonction qui récupère le nom d'un groupe dont on a l'id, et qui le retourne 
 * 
 * @param PDO $pdo objet de connexion à la base 
 * @param String $id_group l'id du groupe donné  
 * 
 * @return String le nom du groupe
 */
function get_group_name_from_id(PDO $pdo, $id_group){
    $sql = "SELECT name_grp FROM VMS_GROUPS WHERE id_grp=$id_group";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    $name_group = "";
    foreach($pdostat as $item){
        $name_group = $item["name_grp"];
        return $name_group;
    }

}




/**
 * Fonction qui récupère l'id d'un hsm dont on a le label, et qui le retourne 
 * 
 * @param PDO $pdo objet de connexion à la base 
 * @param String $label_hsm le label du hsm 
 * 
 * @return int l'id du hsm donné 
 */
function get_hsm_id_from_label(PDO $pdo, $label_hsm){
    $sql = "SELECT id_hsm FROM HSM WHERE label_hsm='$label_hsm'";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    
    $id = 0;

    foreach($pdostat as $item){
        $id = $item["id_hsm"];
        return $id;
    }
    return 1;
}


/**
 * Fonction qui récupère le label d'un hsm dont on a l'id et qui le retourne
 *
 * @param  PDO $pdo L'objet de connexion à la base
 * @param  int $id L'ID du hsm dont on veut le label
 * @return String Le label de l'HSM
 */
function get_hsm_label_from_id(PDO $pdo, $id){
    $sql = "SELECT label_hsm FROM HSM WHERE id_hsm=$id";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $label_hsm = "";

    foreach($pdostat as $item){
        $label_hsm = $item["label_hsm"];
        return $label_hsm;
    }
}

/**
 * Fonction qui récupère l'id d'une vm dont on a le nom, et qui le retourne 
 * 
 * @param PDO $pdo objet de connexion à la base 
 * @param String $name_vm le nom de la vm
 * 
 * @return int l'id de la vm donnée 
 */
function get_vm_id_from_name(PDO $pdo, $name_vm){
    $sql = "SELECT id_vm FROM VMS WHERE name_vm='$name_vm'";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    
    $id = 0;

    foreach($pdostat as $item){
        $id = $item["id_vm"];
        return $id;
    }
}


/**
 * Fonction qui retourne le nom d'une vm dont on a l'id 
 *
 * @param  PDO $pdo L'objet de connexion à la base
 * @param  int $id l'id de la vm dont on veut le nom 
 * @return String le nom de la vm
 */
function get_name_vm_from_id(PDO $pdo, $id){
    $sql = "SELECT name_vm FROM VMS WHERE id_vm=$id";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $name_vm = "";

    foreach($pdostat as $item){
        $name_vm = $item["name_vm"];
        return $name_vm;
    }
}

/**
 * Fonction qui récupère tous les types de vm et les mets dans un tableau
 * 
 * @param PDO $pdo objet de connexion à la base
 * 
 * @return array Le tableau avec tous les types de vm
 */
function get_array_type_vm(PDO $pdo){
    $sql = "SELECT DISTINCT vm_type FROM VMS ORDER BY vm_type ASC";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $array = array();
    foreach ($pdostat as $item){
        array_push($array,$item["vm_type"]);
    }
    return $array;
}

/**
 * Fonction qui récupère tous les groups et les mets dans un tableau
 * 
 * @param PDO $pdo objet de connexion à la base
 * 
 * @return array Le tableau avec tous les groups
 */
function get_array_group(PDO $pdo){
    $sql = "SELECT * FROM VMS_GROUPS ORDER BY name_grp ASC";

    //IT'S OVER 1 THOUSAND m(0_o)m

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $array = array();
    foreach ($pdostat as $item){
        array_push($array,$item["name_grp"]);
    }
    return $array;
}


/**
 * Fonction qui retourne dans la sortie standard les valeurs de l'entete dans new_index.php
 * Répond à la fonction print_thead_archipel_ha() 
 * 
 * @param  Archipel $apl L'ArchiPEL que l'on veut afficher
 * @param  PDO $pdo L'objet de connexion à la base de données
 * 
 * @return void
 */
function print_tbody_archipel_ha(Archipel $apl, PDO $pdo){
    
    //1. On va récuperer la version du hsm (vu qu'on a que l'ID dans l'ArchiPEL)

    $sql = "SELECT version_hsm FROM HSM WHERE id_hsm=$apl->id_hsm";
    $pdostat = $pdo->query($sql);

    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    $version_hsm = "";
    foreach ($pdostat as $item){
        $version_hsm = $item["version_hsm"];
    }


    //TODO : Recuperer tous les HSM et faire une correspondance HSM -->APL 

    //Si ArchiPEL est installé sur + d'une VM, on rentre dans cette condition    
    if($apl->vm2!="") {
        //On récupère tous les noms de VM sur lesquelles ArchiPEL est installé
        $sql2 = "SELECT name_vm FROM VMS WHERE id_vm=$apl->vm1 OR id_vm=$apl->vm2 OR id_vm=$apl->vm3";
        $pdostat2 = $pdo->query($sql2);
        $pdostat2->setFetchMode(PDO::FETCH_ASSOC);
        $cpt = 0;
        $vm1 = "";
        $vm2 = "";
        $vm3 = "";

        foreach ($pdostat2 as $value) {
            //On associe $vm1 $vm2 $vm3 aux vm que l'on a récupéré dans la dernière requete SQL
            if ($cpt == 0) $vm1 = $value["name_vm"];
            if ($cpt == 1) $vm2 = $value["name_vm"];
            if ($cpt == 2) $vm3 = $value["name_vm"];

            $cpt++;
        }
    }else{
        //On est dans le cas d'un ArchiPEL standalone dockerisé, donc il n'a qu'une seule vm
        //On la récupère et on l'assigne à $vm1
        $sql2 = "SELECT name_vm FROM VMS WHERE id_vm=$apl->vm1";
        $pdostat2 = $pdo->query($sql2);
        $pdostat2->setFetchMode(PDO::FETCH_ASSOC);
        $vm1 = "";
        $vm2 = "";
        $vm3 = "";

        foreach ($pdostat2 as $value) {
            $vm1 = $value["name_vm"];
            break;
        }
    }


    //Maintenant on va récuperer le nom du LAB sur lequel ArchiPEL est installé
    $sql3 = "SELECT name_lab FROM LABS INNER JOIN VMS ON VMS.id_lab = LABS.id_lab WHERE LABS.id_lab = (SELECT id_lab FROM VMS WHERE id_vm = $apl->vm1)";
    $pdostat3 = $pdo->query($sql3);
    $pdostat3->setFetchMode(PDO::FETCH_ASSOC);
    $lab = "";

    foreach ($pdostat3 as $val){
        $lab = $val["name_lab"];
        break;
    }

    //Si on arrive dans la page en cliquant sur un APL, on le surligne : 
    if(isset($_GET["apl"])){
        $current_apl = $_GET["apl"];

        if($current_apl == $apl->id){
            echo "<tr class='highlighted_row'>";
        }else{
            echo "<tr>";
        }
    }

    $is_monitored_html = "";

    if($apl->is_monitored){
        //Le script de monitoring est dans la crontab de la vm sur laquelle est installé l'ArchiPEL, on ajoute l'icone de monitoring
        $is_monitored_html = "<i class=\"ion-android-wifi\"></i>";
    }

    //On print le tbody du tableau
    echo "<td><a href='apl_ha_details.php?id=$apl->id'>$apl->nom</a></td>",
    "<td>$is_monitored_html</td>",
    "<td>$apl->name_stack</td>",
    "<td><a href='$apl->url_ihm' target='_blank'>IHM</a></td>",
    "<td>$lab</td>",
    "<td><a href='vms_overview.php?vm=$vm1'>$vm1</a></td>",
    "<td><a href='vms_overview.php?vm=$vm2'>$vm2</a></td>",
    "<td><a href='vms_overview.php?vm=$vm3'>$vm3</a></td>",
    "<td>$apl->schemas_db</td>",
    "<td>$apl->instance_db</td>",
    "<td>$apl->uuid</td>",
    "<td>$apl->version</td>",
    "<td>$apl->version_core</td>",
    "<td>$apl->version_java</td>",
    "<td>$version_hsm</td>",
    "<td>$apl->docker_compose</td>",
    "<td>$apl->path_installation</td>",
    '</tr>';
}


/**
 * Fonction qui retourne dans la sortie standard les valeurs de l'entete dans reef_ha_overview.php
 * Répond à la fonction print_thead_reef_ha
 * 
 * @param  Reef $reef Le reef HA que l'on veut afficher
 * @param  PDO $pdo L'objet de connexion à la base
 * 
 * @return void
 */
function print_tbody_reef_ha(Reef $reef, PDO $pdo){

    //1. On va récuperer la version du hsm (vu qu'on a que l'ID dans l'ArchiPEL)

    $sql = "SELECT version_hsm FROM HSM WHERE id_hsm=$reef->id_hsm_associated";
    $pdostat = $pdo->query($sql);

    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    $version_hsm = "";
    foreach ($pdostat as $item){
        $version_hsm = $item["version_hsm"];
    }

    //TODO : Recuperer tous les HSM et faire une correspondance HSM -->REEF

    //Si Reef est installé sur + d'une VM, on rentre dans cette condition    
    if($reef->vm2!="") {
        //On récupère tous les noms de VM sur lesquelles Reef est installé
        $sql2 = "SELECT name_vm FROM VMS WHERE id_vm=$reef->vm1 OR id_vm=$reef->vm2 OR id_vm=$reef->vm3";
        $pdostat2 = $pdo->query($sql2);
        $pdostat2->setFetchMode(PDO::FETCH_ASSOC);
        $cpt = 0;
        $vm1 = "";
        $vm2 = "";
        $vm3 = "";

        foreach ($pdostat2 as $value) {
            //On associe $vm1 $vm2 $vm3 aux vm que l'on a récupéré dans la dernière requete SQL
            if ($cpt == 0) $vm1 = $value["name_vm"];
            if ($cpt == 1) $vm2 = $value["name_vm"];
            if ($cpt == 2) $vm3 = $value["name_vm"];

            $cpt++;
        }
    }else{
        //On est dans le cas d'un Reef standalone dockerisé, donc il n'a qu'une seule vm
        //On la récupère et on l'assigne à $vm1
        $sql2 = "SELECT name_vm FROM VMS WHERE id_vm=$reef->vm1";
        $pdostat2 = $pdo->query($sql2);
        $pdostat2->setFetchMode(PDO::FETCH_ASSOC);
        $vm1 = "";
        $vm2 = "";
        $vm3 = "";

        foreach ($pdostat2 as $value) {
            $vm1 = $value["name_vm"];
            break;
        }
    }

    //Maintenant on va récuperer le nom du LAB sur lequel reef est installé
    $sql3 = "SELECT name_lab FROM LABS INNER JOIN VMS ON VMS.id_lab = LABS.id_lab WHERE LABS.id_lab = (SELECT id_lab FROM VMS WHERE id_vm = $reef->vm1)";
    $pdostat3 = $pdo->query($sql3);
    $pdostat3->setFetchMode(PDO::FETCH_ASSOC);
    $lab = "";

    foreach ($pdostat3 as $val){
        $lab = $val["name_lab"];
        break;
    }

    //Si on arrive dans la page en cliquant sur un reef, on le surligne : 
    if(isset($_GET["reef"])){
        $current_reef = $_GET["reef"];

        if($current_reef == $reef->id){
            echo "<tr class='highlighted_row'>";
        }else{
            echo "<tr>";
        }
    }

    $is_monitored_html = "";

    if($reef->is_monitored){
        //Le script de monitoring est dans la crontab de la vm sur laquelle est installé le reef, on ajoute l'icone de monitoring
        $is_monitored_html ="<i class=\"ion-android-wifi\"></i>";
    }

    //On print le tbody du tableau
    echo  '<tr>',
    "<td><a href='reef_ha_details.php?id=$reef->id'>$reef->name_reef</a></td>",
    "<td>$is_monitored_html</td>",
    "<td>$reef->name_stack</td>",
    "<td>$lab</td>",
    "<td><a href='vms_overview.php?vm=$vm1'>$vm1</a></td>",
    "<td><a href='vms_overview.php?vm=$vm2'>$vm2</a></td>",
    "<td><a href='vms_overview.php?vm=$vm3'>$vm3</a></td>",
    "<td>$reef->version_reef</td>",
    "<td>$reef->version_java</td>",
    "<td>$version_hsm</td>",
    "<td>$reef->docker_compose</td>",
    "<td>$reef->installation_path</td>",
    '</tr>';
}


/**
 * Fonction qui retourne vers la sortie standard le tbody concernant les ports d'un reef donné 
 * Répond à la fonction print_thead_reef_ha_ports()
 * 
 * @param  Reef $reef Le reef dont on veut afficher les ports 
 * @param  PDO $pdo L'objet de connexion à la base de données 
 * 
 * @return void
 */
function print_tbody_reef_ha_ports(Reef $reef, PDO $pdo){
    echo '<tr>',
    "<td>$reef->port_neops</td>",
    "<td>$reef->port_ethernet</td>",
    "<td>$reef->port_nsquare</td>",
    "<td>$reef->port_simuhost</td>",
    '</tr>';
}


/**
 * Fonction qui retourne vers la sortie standard le tbody concernants les versions d'un reef HA donné
 * Répond à la fonction print_thead_reef_ha_versions()
 * 
 * @param  Reef $reef Le reef dont on veut afficher les versions 
 * @param  PDO $pdo L'objet de connexion à la base de données 
 * 
 * @return void
 */
function print_tbody_reef_ha_versions(Reef $reef, PDO $pdo){
    //On récupère le label du HSM associé à Reef (vu qu'on a que son ID)
    $hsm_label = get_hsm_label_from_id($pdo, $reef->id_hsm_associated);
    
    echo '<tr>',
    "<td>$reef->version_nfast</td>",
    "<td>$reef->version_ubuntu</td>",
    "<td>$hsm_label</td>",
    '</tr>';
}


/**
 * Fonction qui retourne vers la sortie standard les valeurs associées aux entêtes de la page apl_standalone_overview.php
 * Répond à la fonction print_thead_archipel_standalone()
 * 
 * @param  Archipel $apl L'ArchiPEL standalone dont on veut afficher les informations en bref 
 * @param  PDO $pdo L'objet de connexion à la base de données 
 * 
 * @return void
 */
function print_tbody_archipel_standalone(Archipel $apl, PDO $pdo){
    $sql = "SELECT version_hsm FROM HSM WHERE id_hsm=$apl->id_hsm";
    $pdostat = $pdo->query($sql);

    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    $version_hsm = "";
    foreach ($pdostat as $item){
        $version_hsm = $item["version_hsm"];
    }

    $sql2 = "SELECT name_vm FROM VMS WHERE id_vm=$apl->vm1";
    $pdostat2 = $pdo->query($sql2);
    $pdostat2->setFetchMode(PDO::FETCH_ASSOC);
    $vm1 = "";
    foreach ($pdostat2 as $value) {
        $vm1 = $value["name_vm"];
        break;
    }

    $sql3 = "SELECT name_lab FROM LABS INNER JOIN VMS ON VMS.id_lab = LABS.id_lab WHERE LABS.id_lab = (SELECT id_lab FROM VMS WHERE id_vm = $apl->vm1)";
    $pdostat3 = $pdo->query($sql3);
    $pdostat3->setFetchMode(PDO::FETCH_ASSOC);
    $lab = "";

    foreach ($pdostat3 as $val){
        $lab = $val["name_lab"];
        break;
    }

    if(isset($_GET["apl"])){
        $current_apl = $_GET["apl"];

        if($current_apl == $apl->id){
            echo "<tr class='highlighted_row'>";
        }else{
            echo "<tr>";
        }
    }

    $is_monitored_html = "";

    if($apl->is_monitored){
        $is_monitored_html = "<i class=\"ion-android-wifi\"></i>";
    }

    echo "<td><a href='apl_standalone_details.php?id=$apl->id'>$apl->nom</a></td>",
    "<td>$is_monitored_html</td>",
    "<td><a href='$apl->url_ihm' target='_blank'>IHM</a></td>",
    "<td>$lab</td>",
    "<td><a href='vms_overview.php?vm=$vm1'>$vm1</a></td>",
    "<td>$apl->schemas_db</td>",
    "<td>$apl->instance_db</td>",
    "<td>$apl->uuid</td>",
    "<td>$apl->version</td>",
    "<td>$apl->version_core</td>",
    "<td>$apl->version_java</td>",
    "<td>$version_hsm</td>",
    "<td>$apl->path_installation</td>",
    '</tr>';
}

/**
 * Fonction qui retourne vers la sortie standard les valeurs associées aux entêtes sur la page d'accueil des vm (vms_overview.php)
 * Répond à la fonction print_thead_vms()
 * 
 * @param  VM $vm La vm dont on veut afficher les infos en bref
 * @param  PDO $pdo L'objet de connexion à la base de données 
 * 
 * @return void
 */
function print_tbody_vm(VM $vm, PDO $pdo){
    //On commence par traiter les champs total_disk et free_disk pour avoir un affichage propre, et en couleur  
    //Si on a moins de 30% de disque disponible, on applique la classe .espace_warning (orange)
    //Si on a moins de 10% de disque disponible, on applique la classe .espace_critique (rouge)
    $array_total_disk = explode(";",$vm->total_disk);
    $array_free_disk = explode(";",$vm->free_disk);

    $td_space_disk = "<td>";

    for ($i = 0, $size = count($array_total_disk);$i<$size;++$i){
        $total_tmp = explode(" ",$array_total_disk[$i]);
        $free_tmp = explode(" ", $array_free_disk[$i]);
        $total_int = 0;
        $free_int = 0;

        if ( substr($total_tmp[count($total_tmp)-1],-1) == 'M'){
            $total_int = substr($total_tmp[count($total_tmp)-1],0,-1);
        }else {
            $total_int = substr($total_tmp[count($total_tmp)-1],0,-1) * 1000;
        } 

        if ( substr($free_tmp[count($free_tmp)-1],-1) == 'M'){
            $free_int = substr($free_tmp[count($free_tmp)-1],0,-1);
        }else {
            $free_int = substr($free_tmp[count($free_tmp)-1],0,-1) * 1000;
        }
        

        if($free_int < ($total_int/10)){
            $td_space_disk .= "<mark class = 'espace_critique'>" . $array_free_disk[$i] . "</mark>";
            if($i != ($size-1)){
                $td_space_disk .= "<br/>";
            }
        }else{
            if($free_int < (($total_int*3)/10)){
                $td_space_disk .= "<mark class = 'espace_warning'>" . $array_free_disk[$i] . "</mark>";
                if($i != ($size-1)){
                    $td_space_disk .= "<br/>";
                }
            }else{
                $td_space_disk .= $array_free_disk[$i];
                if($i != ($size-1)){
                    $td_space_disk .= "<br/>";
                }
            }
        }
    }

    $td_space_disk .= "</td>";
    
    //Si on arrive dans la page vms_overview.php en cliquant sur une VM elle sera surlignée
    if(isset($_GET["vm"])){
        $current_vm = $_GET["vm"];
        if($current_vm == $vm->name_vm){
            echo "<tr class='highlighted_row'>";
        }else{
            echo "<tr>";
        }
    }

    //Affichage ou non de l'icone de monitoring 
    $is_monitored_html = "";

    if($vm->is_monitored){
        $is_monitored_html = "<i class=\"ion-android-wifi\"></i>";
    }

    echo "<td>$vm->id</td>",
    "<td><a href=\"vms_details.php?id=$vm->id\">$vm->name_vm</a></td>",
    "<td>$is_monitored_html</td>",
    "<td>$vm->ip_vm</td>",
    "<td>$vm->id_lab</td>",
    "<td>$vm->port_ssh</td>",
    "<td>$vm->port_test_auto</td>",
    "<td>$vm->version_ubuntu</td>",
    "<td>$vm->version_docker</td>",
    "<td>$vm->version_glusterfs</td>",
    "<td>",str_replace(";","<br/>",$vm->total_disk),"</td>",
    //"<td>",str_replace(";","<br/>",$vm->free_disk),"</td>",
    $td_space_disk,
    "<td>$vm->total_ram</td>",
    '</tr>';
}


/**
 * Fonction qui retourne vers la sortie standard le tbody concernant le détail des versions d'un ArchiPEL HA (sur apl_ha_details.php)
 * Répond à la fonction print_thead3_archipel_ha_details()
 *
 * @param  Archipel $apl l'ArchiPEL HA dont on veut afficher les versions
 * @param  PDO $pdo L'objet de connexion à la base de données 
 * 
 * @return void
 */
function print_tbody_archipel_ha_details_versions(Archipel $apl, PDO $pdo){
    //On va récupérer la version de docker et de glusterfs de la vm principale de l'apl
    $sql = "SELECT version_docker, version_glusterfs FROM VMS WHERE id_vm = $apl->vm1";
    $row = $pdo->query($sql)->fetch();

    $version_glusterfs = $row["version_glusterfs"];
    $version_docker = $row["version_docker"];


    echo  '<tr>',
    "<td>$apl->version_nfast</td>",
    "<td>$version_docker</td>",
    "<td>$version_glusterfs</td>",
    "<td>$apl->version_wildfly</td>",
    "<td>$apl->version_synchro</td>",
    "<td>$apl->version_ubuntu_apl</td>",
    '</tr>';
}


/**
 * Fonction qui retourne vers la sortie standard les valeurs associées au détail des versions d'un ArchiPEL Standalone (sur apl_standalone_details.php)
 * Répond à la fonction print_thead3_archipel_standalone_details
 * 
 * @param  Archipel $apl L'ArchiPEL standalon
 * @param  PDO $pdo L'objet de connexion à la base de données 
 * 
 * @return void
 */
function print_tbody_archipel_standalone_details_versions(Archipel $apl, PDO $pdo){

    echo  '<tr>',
    "<td>$apl->version_nfast</td>",
    "<td>$apl->version_wildfly</td>",
    "<td>$apl->version_synchro</td>",
    "<td>$apl->version_ubuntu_apl</td>",
    '</tr>';
}


/**
 * Fonction qui retourne vers la sortie standard le tbody correspondant au détails des VM d'un ArchiPEL HA (apl_ha_details.php) 
 * Répond à la fonction print_thead2_archipel_ha_details() 
 * 
 * @param  Archipel $apl L'ArchiPEL dont on veut les infos de VM
 * @param  PDO $pdo L'objet de connexion à la base de données
 * 
 * @return void
 */
function print_tbody_archipel_ha_details_vm(Archipel $apl, PDO $pdo){
    //On récupére dans un premier temps les info de la première VM 
    //En effet, l'ArchiPEL pourrait être un standalone dockerisé, donc n'être installé sur qu'une seule vm
    $sql = "SELECT ip, port_ssh, port_test_auto FROM VMS WHERE id_vm = $apl->vm1";
    $row = $pdo->query($sql)->fetch();

    $ip1 = $row["ip"];
    $ssh1 = $row["port_ssh"];
    $auto1 = $row["port_test_auto"];

    //On va maintenant vérifier si ArchiPEL est installé sur d'autres VMs, dans ce cas la on va assigner aux variables les differentes infos de ces dites vms
    if($apl->vm2!="") {
        
        $sql = "SELECT ip, port_ssh, port_test_auto FROM VMS WHERE id_vm = $apl->vm2";
        $row = $pdo->query($sql)->fetch();

        $ip2 = $row["ip"];
        $ssh2 = $row["port_ssh"];
        $auto2 = $row["port_test_auto"];
    }else{
        $ip2 = "";
        $ssh2 = "";
        $auto2 = "";
    }

    if($apl->vm3!="") {
        $sql = "SELECT ip, port_ssh, port_test_auto FROM VMS WHERE id_vm = $apl->vm3";
        $row = $pdo->query($sql)->fetch();

        $ip3 = $row["ip"];
        $ssh3 = $row["port_ssh"];
        $auto3 = $row["port_test_auto"];
    }else{
        $ip3 = "";
        $ssh3 = "";
        $auto3 = "";
    }


    echo  '<tr>',
    "<td>$ip1</td>",
    "<td>$ssh1</td>",
    "<td>$auto1</td>",
    "<td>$ip2</td>",
    "<td>$ssh2</td>",
    "<td>$auto2</td>",
    "<td>$ip3</td>",
    "<td>$ssh3</td>",
    "<td>$auto3</td>",
    '</tr>';
}



/**
 * Fonction qui va retourner vers la sortie standard les info de la vm sur laquelle est installé un ArchiPEL standalone (apl_standalone_details.php)
 * Répond à la fonction print_thead2_archipel_standalone_details()
 *
 * @param  Archipel $apl L'ArchiPEL Standalone dont on veut les données de VM
 * @param  PDO $pdo L'objet de connexion en base de données 
 * 
 * @return void 
 */
function print_tbody_archipel_standalone_details_vm(Archipel $apl, PDO $pdo){
    //On fait une requête pour récuperer les infos de la VM
    $sql = "SELECT ip, port_ssh, port_test_auto FROM VMS WHERE id_vm = $apl->vm1";
    $row = $pdo->query($sql)->fetch();

    $ip1 = $row["ip"];
    $ssh1 = $row["port_ssh"];
    $auto1 = $row["port_test_auto"];

    echo  '<tr>',
    "<td>$ip1</td>",
    "<td>$ssh1</td>",
    "<td>$auto1</td>",
    '</tr>';
}



/**
 * Fonction qui va retourner un ArchiPEL dont on a l'ID 
 *
 * @param  PDO $pdo L'objet de connexion en base de données
 * @param  int $id L'ID de l'ArchiPEL que l'on veut récupérer 
 * 
 * @return Archipel L'archiPEL voulu
 */
function get_apl(PDO $pdo, $id){
    //On fait une requête, on construit l'ArchiPEL et on le renvoie 

    $sql = "SELECT * FROM ARCHIPEL WHERE id_apl=$id";
    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    $archipel = null;

    foreach ($pdostat as $item){
        $archipel = new Archipel($item["id_apl"],$item["name_archipel"],$item["name_stack"],$item["id_vm_associated"],$item["id_vm_associated2"],$item["id_vm_associated3"],$item["schemas_db"],$item["instance_db"],$item["uuid_publication"],$item["version"],$item["version_core"],$item["version_java"],$item["version_nfast"],$item["version_wildfly"],$item["version_ubuntu_apl"],$item["version_synchro"],$item["id_hsm_associated"],$item["url_ihm"],$item["docker_compose"],$item["installation_path"], $item["is_monitored"]);
    }

    return $archipel;
}


/**
 * Fonction qui va retourner une VM dont on a l'ID
 *
 * @param  PDO $pdo L'objet de connexion en base de données 
 * @param  int $id L'ID de la vm que l'on veut récupérer
 * 
 * @return VM La VM voulue 
 */
function get_vm(PDO $pdo, $id){
    //On fait une requête avec l'id en paramètre, on construit la VM puis on la renvoie
    $sql = "SELECT * FROM VMS WHERE id_vm=$id";
    
    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    $vm = null;

    foreach($pdostat as $item){
        $vm = new VM($item["id_vm"],$item["name_vm"],$item["ip"],$item["id_lab"],$item["port_ssh"],$item["port_test_auto"],$item["version_ubuntu"],$item["version_docker"],$item["version_glusterfs"],$item["total_disk"],$item["free_disk"],$item["total_ram"], $item["id_grp"], $item["vm_type"], $item["is_monitored"]);
    }

    return $vm;

}


/**
 * Fonction qui va retourner un reef dont on a l'ID
 *
 * @param  PDO $pdo L'objet de connexion en base de données 
 * @param  int $id L'id du reef que l'on veut récupérer 
 * 
 * @return Reef Le Reef voulu
 */
function get_reef(PDO $pdo, $id){
    //On fait une requête avec l'id en paramètre, on contruit le reef et on le renvoie
    $sql = "SELECT * FROM REEF WHERE id_reef=$id";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    $reef = null;

    foreach($pdostat as $item){
        $reef = new Reef($item["id_reef"],$item["id_vm_associated"], $item["id_vm_associated2"], $item["id_vm_associated3"], $item["name_reef"], $item["name_stack"], $item["port_neops"], $item["port_ethernet"], $item["port_nsquare"], $item["port_simuhost"], $item["version_reef"], $item["version_java"], $item["version_nfast"], $item["version_ubuntu_reef"], $item["id_pki_associated"], $item["id_hsm_associated"], $item["docker_compose"], $item["installation_path"], $item["type"], $item["is_monitored"] );
    }
    return $reef;
}


/**
 * Fonction qui va renvoyer le nombre d'ArchiPEL installés sur une VM
 *
 * @param  PDO $pdo L'objet de connexion en base de données 
 * @param  int $id L'id de la vm où on va compter 
 * 
 * @return int Le nombre d'ArchiPEL installés sur la VM dont on a l'ID
 */
function get_nb_apl_linked(PDO $pdo, $id){
    //On fait simplement un count sql
    return $pdo->query("SELECT COUNT(*) FROM ARCHIPEL WHERE id_vm_associated=$id OR id_vm_associated2=$id OR id_vm_associated3=$id")->fetchColumn();
} 

/**
 * Fonction qui va renvoyer le nombre de Reef installés sur une VM
 *
 * @param  PDO $pdo L'objet de connexion en base de données 
 * @param  int $id L'id de la vm où on va compter 
 * 
 * @return int Le nombre de Reef installés sur la VM dont on a l'ID
 */
function get_nb_reef_linked(PDO $pdo, $id){
    return $pdo->query("SELECT COUNT(*) FROM REEF WHERE id_vm_associated=$id OR id_vm_associated2=$id OR id_vm_associated3=$id")->fetchColumn();
}


/**
 * Fonction qui va retourner vers la sortie standard le bloc de commentaires (Affichage des commentaires actuels et publication d'un nouveau)
 *
 * @param  PDO $pdo L'objet de connexion à la base de données
 * @param  int $id Id de la vm, reef, apl sur lequel on veut afficher/publier un commentaire
 * @param  String $type_associated type de l'objet sur lequel on veut afficher/publier un commentaire ("reef", "archipel", "vm")
 * @param  String $username_logged Nom de l'utilisateur courant
 * @param  String $role_logged Rôle de l'utilisateur courant 
 * 
 * @return void
 */
function print_comments(PDO $pdo, $id, $type_associated, $username_logged="",$role_logged=""){
    //On va mettre tous les commentaire de l'objet courant dans un tableau 
    $array_comments = array();

    $sql = "SELECT * FROM COMMENTS WHERE id_associated='$id' AND type_associated='$type_associated'";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    foreach($pdostat as $item){
        //On construit le commentaire et on le push dans le tableau 
        $comment = new Comment($item["id_comment"],$item["txt_comment"],$item["name_author"],$item["id_associated"],$item["type_associated"],$item["time_comment"]);
        array_push($array_comments,$comment);
    }

    //Affichage de la box commentaires
    echo "<section class=\"content container-fluid\">",
            "<div class=\"box box-warning\">",
                "<div class=\"box-header with-border\">",
                    "<h3 class=\"box-title my-3\">Commentaires</h3>";

    foreach($array_comments as $comment){
        //Conversion d'un DATETIME SQL En date écrite en Français
        list($date, $time) = explode(" ", $comment->time_comment);
        list($year,$month,$day) = explode("-",$date);
        list($hour,$min,$sec) = explode(":",$time);

        $months = array("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
        $time_html = "Le $day " . $months[$month-1] . " $year à ${hour}h ${min}m ${sec}s";

        echo "<div class=\"box box-solid box-warning px-3\">",
                "<div class=\"author_name_comment\"><h4>$comment->name_author</h4><p>$time_html</p></div>",
                "<p>$comment->txt_comment<p>",
            "</div>";
    }

    //Si on est du role "integrator" ou "administrator" on affiche le bloc qui permet d'ajouter un commentaire
    if($role_logged=="integrator" || $role_logged=="administrator"){
        echo '<div class="box box-solid box-warning px-3 flex_form">',
                '<form action="comment_added.php?type=',$type_associated,'&id=',$id,'" method="POST">',
                    '<label class = "label_form">Commentaire : </label>',
                    '<textarea required id="txt_comment" name="txt_comment" class="txt_comment"></textarea>',
                    '<button class="btn btn_commentating">Ajouter le commentaire</button>',
                '</form>',
            '</div>';
    }

    echo "</div></div></section>";



                    
}