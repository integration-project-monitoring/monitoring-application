<?php 
/**
 * Page qui va afficher un formulaire pour ajouter un archipel HA en base 
 * Champs nécessaires : 
 *      - VM de Tx1
 *      - Nom de l'ArchiPEL
 *      - Nom de la stack ArchiPEL
 *      - Path absolu vers l'installation d'ArchiPEL
 *      - HSM associé
 *      - Lien vers l'IHM
 * Champs falcultatifs : 
 *      - VM de Tx2 (vide si standalone dockerisé)
 *      - VM du Worker (vide si standalone dockerisé)
 */

//On inclut la librairie de fonctions 
require_once "library_monitoring.php";

//On initialise la session
session_start();

//Vérification du rôle de l'utilisateur connecté 
// Roles acceptés : "integrator" ; "administrator"
//Si l'utilisateur est bien connecté, et a les bons droits, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
    }else{
        //L'utilisateur n'a pas les bons droits
        header("Location: right_error.php");
        exit;
    }
}else{
    //L'utilisateur n'est pas connecté
    header("Location: login.php?errno=add_apl_ha");
    exit;
}

//Affichage de l'entete en html 
print_head('Ajout ArchiPEL HA - EPI','monitoring.css');


//Connexion a la base de donnee 
$pdo = connectToBdd();

//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();

echo '<div class="content-wrapper">',
        '<section class="content-header">',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3>Ajout d\'un ArchiPEL HA :</h3>';

//DEBUT DU FORMULAIRE 

echo '<form action="apl_ha_added_new.php" method="post">';

//Champs txt_name_vm --> nom de la vm1 (Tx1 pour les HA)
echo '<label class="label_form required">Sur quelle VM est installé ArchiPEL Tx1 ? </label> <input required type="text" name="txt_name_vm" id="txt_name_vm" list="list_name_vm">';
            
//On va récupérer dans un tableau toutes les vm 
$array_vm = get_array_vm($pdo);


//On va créer la datalist dans laquelle on a tous les noms de vm d'inscrits
echo '<datalist id=list_name_vm>';

foreach ($array_vm as $item){
    echo '<option>' , $item->name_vm , '</option>';
}

echo '</datalist><span style="margin-left:10px">Vous ne trouvez pas votre VM ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">L\'ajouter</button><br>';


//Champs txt_name_vm2 --> nom de la vm2 (Tx2)
echo '<label class="label_form">Sur quelle VM est installé ArchiPEL Tx2 ? (Laisser vide si standalone dockerisé) </label> <input type="text" name="txt_name_vm2" id="txt_name_vm2" list="list_name_vm">';
echo '<span style="margin-left:10px">Vous ne trouvez pas votre VM ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">L\'ajouter</button><br>';

//Champs txt_name_vm3 --> nom de la vm3 (Worker)
echo '<label class="label_form">Sur quelle VM est installé ArchiPEL Worker ? (Laisser vide si standalone dockerisé) </label> <input type="text" name="txt_name_vm3" id="txt_name_vm3" list="list_name_vm">';
echo '<span style="margin-left:10px">Vous ne trouvez pas votre VM ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">L\'ajouter</button><br>';

//Champs txt_name_apl --> nom de l'archiPEL HA
echo '<label class="label_form required">Quel est le nom de l\'archipel ?</label>',
        '<input required type="text" name="txt_name_apl" id="txt_name_apl"><br>';

//Champs txt_name_stack_apl --> nom de la stack de l'ArchiPEL HA
echo '<label class="label_form required">Quel est le nom de la stack ?</label>',
    '<input required type="text" name="txt_name_stack_apl" id="txt_name_stack_apl"><br>';

//Champs txt_installation_path_apl --> path absolu vers le dossier d'installation d'ArchiPEL
echo '<label class="label_form required">Quel est le path d\'installation d\'archipel ?</label>',
    '<input required type="text" name="txt_installation_path_apl" id="txt_installation_path_apl" value="/opt/ArchiPEL"><br>';

//Champs txt_dockercompose_path_apl --> path absolu vers le docker compose d'ArchiPEL HA
echo '<label class="label_form required">Quel est le path vers le docker compose ?</label>',
    '<input required type="text" name="txt_dockercompose_path_apl" id="txt_dockercompose_path_apl" value="/opt/docker-compose/..."><br>';   

//Champs txt_hsm_apl --> label du HSM auquel est associé l'ArchiPEL HA
echo '<label class="label_form required">Sur quel HSM est-il branché ?</label>',
    '<input required type="text" name="txt_hsm_apl" id="txt_hsm_apl" list="list_hsm"><br>';

//Champs txt_link_ihm_apl --> lien vers l'ihm de l'ArchiPEL HA
echo '<label class="label_form required">Quel est le lien vers l\'IHM ?</label>',
    '<input required type="text" name="txt_link_ihm_apl" id="txt_link_ihm_apl"><br>';

//On récupère tous les HSM dans un tableau
$array_hsm = get_array_hsm($pdo);

//On va créer la datalist dans laquelle on a tous les label d'HSM d'inscrits
echo '<datalist id=list_hsm>';

foreach ($array_hsm as $item){
    echo '<option>', $item->label_hsm, '</option>';
}

echo '</datalist>';


echo '<button class="btn btn_monitoring"> Ajouter l\'ArchiPEL</button>';
            
echo '</div>';

echo '</div>',
'</section>',
'</div>',
'</div>';

//On inclue les scripts JS (mis à la fin pour l'optimisation)
print_scripts();

echo '</body>','</html>';