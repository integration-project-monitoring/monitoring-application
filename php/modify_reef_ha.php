<?php 
require_once "library_monitoring.php";

session_start();

if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
        if(isset($_GET["id"])){
            $current_id = $_GET["id"];
        }else{
            header("Location: reef_ha_overview.php");
            exit;
        }
    }else{
        header("Location: right_error.php");
        exit;
    }
}else{
    if(isset($_GET["id"])){
        $current_id = $_GET["id"];
        header("Location: login.php?errno=modify_reef_ha&id=$current_id");
        exit;
    }else{
        header("Location: reef_ha_overview.php");
        exit;
    }
    
    $username ="";
    $is_connected = 0;
}

//Affichage de l'entete en html 
print_head('Modification Reef HA - EPI','monitoring.css');


//Connexion a la base de donnee 
$pdo = connectToBdd();

$current_reef = get_reef($pdo,$current_id);
$vm1 = get_name_vm_from_id($pdo,$current_reef->vm1);

if(strlen($current_reef->vm2) > 0 ){
    $vm2 = get_name_vm_from_id($pdo, $current_reef->vm2);
}

if(strlen($current_reef->vm3) > 0){
    $vm3 = get_name_vm_from_id($pdo, $current_reef->vm3);
}

$hsm_label = get_hsm_label_from_id($pdo,$current_reef->id_hsm_associated);

echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

print_header($is_connected,$username);

print_sidebar();

echo '<div class="content-wrapper">',
        '<section class="content-header">',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3>Modification de Reef ',$current_reef->name_reef,' HA :</h3>';

echo '<form action="reef_ha_modified_new.php?id=',$current_reef->id,'" method="post">';

echo '<label class="label_form required">Quelle est la première VM ? </label> <input required type="text" name="txt_name_vm" id="txt_name_vm" list="list_name_vm" value="',$vm1,'">';


$array_vm = get_array_vm($pdo);

echo '<datalist id=list_name_vm>';

foreach ($array_vm as $item){
    echo '<option>' , $item->name_vm , '</option>';
}

echo '</datalist><span style="margin-left:10px">Vous ne trouvez pas votre VM ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">L\'ajouter</button><br>';

echo '<label class="label_form required">Quelle est la deuxième VM ?</label> <input required type="text" name="txt_name_vm2" id="txt_name_vm2" list="list_name_vm" value="',$vm2,'">';
echo '<span style="margin-left:10px">Vous ne trouvez pas votre VM ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">L\'ajouter</button><br>';

echo '<label class="label_form required">Quelle est la troisième VM ?</label> <input required type="text" name="txt_name_vm3" id="txt_name_vm3" list="list_name_vm" value = "',$vm3,'">';
echo '<span style="margin-left:10px">Vous ne trouvez pas votre VM ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">L\'ajouter</button><br>';


echo '<label class="label_form required">Quel est le nom du Reef ?</label>',
        '<input required type="text" name="txt_name_reef" id="txt_name_reef" value="',$current_reef->name_reef,'"><br>';


echo '<label class="label_form required">Quel est le nom de la stack ?</label>',
    '<input required type="text" name="txt_name_stack_reef" id="txt_name_stack_reef" value="',$current_reef->name_stack,'"><br>';

echo '<label class="label_form required">Quel est le path d\'installation de reef ?</label>',
    '<input required type="text" name="txt_installation_path_reef" id="txt_installation_path_reef" value="',$current_reef->name_stack,'"><br>';

echo '<label class="label_form required">Quel est le path vers le docker compose ?</label>',
    '<input required type="text" name="txt_dockercompose_path_reef" id="txt_dockercompose_path_reef" value="',$current_reef->docker_compose,'"><br>';   

echo '<label class="label_form required">Sur quel HSM est-il branché ?</label>',
    '<input required type="text" name="txt_hsm_reef" id="txt_hsm_reef" list="list_hsm" value="',htmlentities($hsm_label),'"><br>';

echo '<label class="label_form">Quel est le port Neops ?</label>',
    '<input type="number" name="int_neops" id="int_neops" value="',$current_reef->port_neops,'"><br>';


echo '<label class="label_form ">Quel est le port Ethernet ?</label>',
    '<input type="number" name="int_ethernet" id="int_ethernet" value="',$current_reef->port_ethernet,'"><br>';


echo '<label class="label_form ">Quel est le port NSquare ?</label>',
    '<input type="number" name="int_nsquare" id="int_nsquare" value="',$current_reef->port_nsquare,'"><br>';


echo '<label class="label_form ">Quel est le port Simuhost ?</label>',
    '<input type="number" name="int_simuhost" id="int_simuhost" value="',$current_reef->port_simuhost,'"><br>';

$array_hsm = get_array_hsm($pdo);

echo '<datalist id=list_hsm>';

foreach ($array_hsm as $item){
    echo '<option>', $item->label_hsm, '</option>';
}

echo '</datalist>';


echo '<button class="btn btn_monitoring"> Appliquer les Modifs</button>';
            
echo '</div>';

echo '</div>',
'</section>',
'</div>',
'</div>';

print_scripts();

echo '</body>','</html>';