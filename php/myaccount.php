<?php 
/**
 * Page qui va afficher l'espace "Mon compte" Avec une interface de changement de mot de passe
 * Pour les administrateurs on aura un bouton pour ajouter un utilisateur 
 * 
 * Paramètres de la page : 
 *      - FACULTATIF : 
 *          @param GET int $errno code d'erreur 
 */

//On inclue la librairie de fonctions
require_once "library_monitoring.php";

//On initialise la session
session_start();

//Vérification du rôle de l'utilisateur connecté 
// Roles acceptés : tous
//Si l'utilisateur est bien connecté, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
if(isset($_SESSION["username_logged"])){
    $username = $_SESSION["username_logged"];
    $role = $_SESSION["role_logged"];
    $is_connected = 1;
}else{
    //L'utilisateur n'est pas connecté, on redirige vers la page de login 
    $username ="";
    $is_connected = 0;
    header("Location: login.php?errno=my_account");
    exit;
}

//Affichage de l'entete html 
print_head("Monitoring - EPI", "monitoring.css");

//Connexion à la base de données 
$pdo = connectToBdd();

//On regarde si on a un message d'erreur, si oui, on affiche un message d'erreur
$message_error = "";
if(isset($_GET["errno"])){
    $errno = $_GET["errno"];

    switch($errno){
        case "1":
            $message_error = "Mot de passe incorrect, veuillez ré-essayer."; 
            break;
    }
}


//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();


echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            "<div class=\"box box-solid box-warning px-3 flex_login\">",
            
            //Début du formulaire de changement de mot de passe 
            "<form action=\"mdp_modified.php\" method=\"post\" class=\"py-2\">",

                //Message d'erreur s'il y en a un 
                "<p class = \"validate_status_ok\" id=\"error_message\">$message_error</p>",

                //Affichage du nom d'utilisateur
                "<label>Nom d'utilisateur : $username</label><br>",

                //Champs mdp_old --> Mot de passe actuel 
                "<label class=\"label_form\">Mot de passe actuel : </label>",
                "<input type=\"password\" name=\"mdp_old\" id=\"mdp_old\"><br>",

                //Champs mdp_new --> Nouveau mot de passe
                "<label class=\"label_form\">Nouveau mot de passe : </label>",
                "<input type=\"password\" name=\"mdp_new\" id=\"mdp_new\"><br>",

                //Champs mdp_new2 --> confirmation du nouveau mot de passe 
                "<label class=\"label_form\">Confirmation du nouveau mot de passe : </label>",
                "<input type=\"password\" name=\"mdp_new2\" id=\"mdp_new2\"><br>",

                //Texte de validation (verification si les mdp sont bien identiques etc...)
                "<p id=\"validate_status\" class=\"validate_status_ok\"></p><br>",
                "<button class=\"btn btn_monitoring my-3\" id=\"btn_change_mdp\" disabled>Changer de mot de passe</button>",
            "</form>";

//Affichage du bouton "Ajouter un utilisateur" 

echo "<section class=\"content-footer flex_buttons\">",
        "<button class=\"btn btn_monitoring\" onclick=\"window.location.replace('add_user.php');\">Ajouter un utilisateur</button>",
        //"<button class=\"btn btn_suppress\" onclick=\"\">Supprimer un utilisateur</button>",
    "</section>";

echo "</div></div></div></section></div></div>";



print_scripts();
echo '<script src="../js/change_mdp.js"></script>';


echo "</body></html>";

