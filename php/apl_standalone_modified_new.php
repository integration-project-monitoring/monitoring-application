<?php 

/**
 * Page qui va construire la requete de modification d'ArchiPEL Standalone et indiquer si l'archipel a bien ete modifié
 * On va dans un premier temps construire la requete, avec le parsage des champs entrés dans la derniere page,
 * puis ensuite on fera un petit affichage indiquant a l'utilisateur si c'est OK ou KO
 * 
 * Paramètres de la page : 
 *      - OBLIGATOIRE : 
 *          @param GET int $id : ID de l'ArchiPEL Standalone que l'on va modifier
 */

//On inclut la librairie de fonctions
require_once "library_monitoring.php";

//On initialise la session
session_start();


//Vérification du rôle de l'utilisateur connecté 
// Roles acceptés : "integrator" ; "administrator"
//Si l'utilisateur est bien connecté, et a les bons droits, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
        //On vérifie que le paramètre $id est bien initié
        if(isset($_GET["id"])){
            $current_id = $_GET["id"];
        }else{
            //Le paramètre $id n'existe pas, on redirige vers la page d'accueil d'ArchiPEL HA
            header("Location: apl_standalone_overview.php");
            exit;
        }
    }else{
        //L'utilisateur n'a pas les bons droits
        header("Location: right_error.php");
        exit;
    }
}else{
    //L'utilisateur n'est pas connecté
    if(isset($_GET["id"])){
        //L'utilisateur n'est pas connecté mais l'id est bien initialisé, on redirige vers la page de login qui redirigera vers la page de modification d'ArchiPEL
        $current_id = $_GET["id"];
        header("Location: login.php?errno=modify_apl_standalone&id=$current_id");
        exit;
    }else{
        //$id n'est pas présente dans GET, et l'utilisateur n'est pas connecté, on redirige vers la page d'accueil
        header("Location: apl_standalone_overview.php");
        exit;
    }
    
    $username ="";
    $is_connected = 0;
}

//Affichage de l'entete en html 
print_head('Modification d\'ArchiPEL Standalone - EPI ', 'monitoring.css');

//Connexion a la base de donnee
$pdo = connectToBdd();

 //Affichage du conteneur
 echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header();

//Affichage de la sidebar
print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">';


 //On commence par parser la requete 

 //1. on extrait tous les inputs utilisateurs 
 //On applique htmlentities() et addslashes() pour éviter les injections

 //On récupère l'ArchiPEL HA qui a l'id passé en paramètre GET
 $current_apl = get_apl($pdo,$current_id);
 $name_apl = "'" . addslashes(htmlentities($_POST["txt_name_apl"])) . "'";

 //Vu qu'on a que le nom de la vm on récupère son id
 $id_vm_apl = get_vm_id_from_name($pdo,htmlentities($_POST["txt_name_vm"]));

 $path_installation_apl = "'" . addslashes(htmlentities($_POST["txt_installation_path_apl"])) . "'";

 //Vu qu'on a que le label du HSM on get son ID
 $id_hsm = get_hsm_id_from_label($pdo, $_POST["txt_hsm_apl"]);
 
 $uri_ihm = "'" . htmlentities($_POST["txt_link_ihm_apl"]) . "'";

 $sql = "UPDATE ARCHIPEL 
        SET name_archipel = $name_apl, id_vm_associated = $id_vm_apl, installation_path = $path_installation_apl, id_hsm_associated = $id_hsm, url_ihm = $uri_ihm
        WHERE id_apl=$current_apl->id";

 //2. On éxecute la requête, s'il y a un problème on rentrera dans le catch, ou on affichera un message d'erreur
 // Le message d'erreur contient la requête SQL, pour pouvoir débugger aisèment

 try{
    $pdo->query($sql); 

    
   echo "<h3>L'ArchiPEL <strong>$name_apl</strong> a bien été modifié ! </h3>", 
       '<p>Vous allez être redirigé vers la liste des archipel standalone dans <strong id="countdown">5 secondes</strong></p><br>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';


   //Script de compte à rebours
   echo '<script>', 
           'let i = 5;', 
           'let tmp = setInterval(countdown, 1000);',
           'function countdown() {',
               'if(i!=0) {',
                   'document.getElementById("countdown").innerHTML = i + " secondes";',
                   'i = i -1; ',
               '}',
               'else {',
                   'window.location.replace("apl_standalone_overview.php");',
               '}',
           '}',
       '</script>';
   
    //On inclue les scripts JS (mis à la fin pour l'optimisation)
   print_scripts();

   echo '</body>','</htlm>';
}catch (Exception $e){

    //Si on rentre ici c'est qu'il y a un problème dans la requête
    //Soit une erreur de saisie, un champs trop long etc... 
    //Astuce : copier coller la requête SQL dans phpmyadmin, l'erreur sera plus explicite !
   echo "<h3>Il y a eu une erreure technique dans la modification l'ArchiPEL $name_apl</h3>", 
       '<p>La commande est : ',$sql,'<br>Veuillez contacter le développeur en lui copiant collant cette commande pour voir quel est le soucis </p>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';

   //On inclue les scripts JS (mis à la fin pour l'optimisation)
   print_scripts();

   echo '</body>','</htlm>';
}
