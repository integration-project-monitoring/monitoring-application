<?php 

/**
 * Page qui va construire la requete et indiquer si l'archipel a bien ete ajouté
 * On va dans un premier temps construire la requete, avec le parsage des champs entrés dans la derniere page,
 * puis ensuite on fera un petit affichage indiquant a l'utilisateur si c'est OK ou KO
 */


require_once "library_monitoring.php";

session_start();

if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
    }else{
        header("Location: right_error.php");
        exit;
    }
}else{
    header("Location: login.php?errno=add_reef_ha");
    exit;
    $username ="";
    $is_connected = 0;
}

print_head('Ajout Reef HA - EPI ', 'monitoring.css');

$pdo = connectToBdd();

 echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

    print_header($is_connected,$username);

    print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">';


 //On commence par parser la requete 

 //1. on extrait tous les inputs utilisateurs 


 $name_reef = "'" . addslashes(htmlentities($_POST["txt_name_reef"])) . "'";

 $name_stack = "'" . addslashes(htmlentities($_POST["txt_name_stack_reef"])) . "'";
 
 $id_vm_reef = get_vm_id_from_name($pdo,htmlentities($_POST["txt_name_vm"]));


 if(strlen($_POST["txt_name_vm2"]) > 0){
    $id_vm2_reef = get_vm_id_from_name($pdo,htmlentities($_POST["txt_name_vm2"]));
 }else{
     $id_vm2_reef = "NULL";
 }

 if(strlen($_POST["txt_name_vm3"]) > 0){
    $id_vm3_reef = get_vm_id_from_name($pdo,htmlentities($_POST["txt_name_vm3"]));
 }else{
     $id_vm3_reef = "NULL";
 }
 

 $path_installation_reef = "'" . addslashes(htmlentities($_POST["txt_installation_path_reef"])) . "'";
 $path_dockercompose_reef = "'" . addslashes(htmlentities($_POST["txt_dockercompose_path_reef"])) . "'";

 $id_hsm = get_hsm_id_from_label($pdo, $_POST["txt_hsm_reef"]);
 
 
 if(strlen($_POST["int_neops"]>0)){
    $port_neops = "'" . addslashes(htmlentities($_POST["int_neops"])) . "'";    
 }else{
     $port_neops = "NULL";
 }

 if(strlen($_POST["int_ethernet"]>0)){
    $port_ethernet = "'" . addslashes(htmlentities($_POST["int_ethernet"])) . "'";    
 }else{
     $port_ethernet = "NULL";
 }

 if(strlen($_POST["int_nsquare"]>0)){
    $port_nsquare = "'" . addslashes(htmlentities($_POST["int_nsquare"])) . "'";    
 }else{
     $port_nsquare = "NULL";
 }

 if(strlen($_POST["int_simuhost"]>0)){
    $port_simuhost = "'" . addslashes(htmlentities($_POST["int_simuhost"])) . "'";    
 }else{
     $port_simuhost = "NULL";
 }


 
 $sql = "INSERT INTO REEF (name_stack, id_vm_associated, id_vm_associated2, id_vm_associated3, installation_path, id_hsm_associated, docker_compose, name_reef, port_neops, port_ethernet, port_nsquare, port_simuhost, id_pki_associated) VALUES ($name_stack, $id_vm_reef, $id_vm2_reef, $id_vm3_reef, $path_installation_reef, $id_hsm, $path_dockercompose_reef, $name_reef, $port_neops, $port_ethernet, $port_nsquare, $port_simuhost, 1)";

 try{
    $pdo->query($sql); 

    
   echo "<h3>Le Reef <strong>$name_reef</strong> HA a bien été ajouté ! </h3>", 
       '<p>Vous allez être redirigé vers la liste des Reef HA dans <strong id="countdown">5 secondes</strong></p><br>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';

   echo '<script>', 
           'let i = 5;', 
           'let tmp = setInterval(countdown, 1000);',
           'function countdown() {',
               'if(i!=0) {',
                   'document.getElementById("countdown").innerHTML = i + " secondes";',
                   'i = i -1; ',
               '}',
               'else {',
                   'window.location.replace("reef_ha_overview.php");',
               '}',
           '}',
       '</script>';
   
   print_scripts();

   echo '</body>','</htlm>';
}catch (Exception $e){

   echo "<h3>Il y a eu une erreure technique dans l'ajout du reef $name_reef</h3>", 
       '<p>La commande est : ',$sql,'<br>Veuillez contacter le développeur en lui copiant collant cette commande pour voir quel est le soucis </p>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';


   print_scripts();

   echo '</body>','</htlm>';
}
