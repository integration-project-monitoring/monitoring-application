<?php 

/**
 * Page qui va supprimer une VM (avec l'id passé par GET)
 */


require_once "library_monitoring.php";



session_start();

if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
        if(isset($_GET["id"])){
            $current_id = $_GET["id"];
        }else{
            header("Location: vms_overview.php");
            exit;
        }
    }else{
        header("Location: right_error.php");
        exit;
    }
}else{
    if(isset($_GET["id"])){
        $current_id = $_GET["id"];
        header("Location: login.php?errno=suppress_vm&id=$current_id");
        exit;
    }else{
        header("Location: vms_overview.php");
        exit;
    }
    
    $username ="";
    $is_connected = 0;
}

print_head('Suppression de VM - EPI ', 'monitoring.css');

$pdo = connectToBdd();

$current_vm = get_vm($pdo,$current_id);

 echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

    print_header();

    print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">';


 $sql = "DELETE FROM VMS WHERE id_vm = $current_vm->id ";

 try{
    $pdo->query($sql); 

    
   echo "<h3>La VM <strong>$current_vm->name_vm</strong> a bien été supprimée ! </h3>", 
       '<p>Vous allez être redirigé vers la liste des VMs dans <strong id="countdown">5 secondes</strong></p><br>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';

   echo '<script>', 
           'let i = 5;', 
           'let tmp = setInterval(countdown, 1000);',
           'function countdown() {',
               'if(i!=0) {',
                   'document.getElementById("countdown").innerHTML = i + " secondes";',
                   'i = i -1; ',
               '}',
               'else {',
                   'window.location.replace("vms_overview.php");',
               '}',
           '}',
       '</script>';

   print_scripts();

   echo '</body>','</htlm>';
}catch (Exception $e){

   echo "<h3>Il y a eu une erreure technique dans la suppression de la VM $current_vm->name_vm</h3>", 
       '<p>La commande est : ',$sql,'<br>Veuillez contacter le développeur en lui copiant collant cette commande pour voir quel est le soucis </p>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';


   print_scripts();

   echo '</body>','</htlm>';
}
