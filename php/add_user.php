<?php
/**
 * Page qui va afficher un formulaire pour ajouter un nouvel utilisateur à la plateforme 
 * Champs nécessaires : 
 *      - Nom d'utilisateur du nouvel utilisateur
 *      - Mot de passe du nouvel utilisateur (qu'il devra changer par la suite)
 *      - Confirmation du mot de passe du nouvel utilisateur
 * 
 * Paramètres de la page : 
 *      - FACULTATIF : 
 *          @param GET int $errno : numéro d'erreur 
 */

//On inclut la librairie de fonctions 
require_once "library_monitoring.php";

//On initialise la session
session_start();

//Vérification du rôle de l'utilisateur connecté 
// Role accepté : "administrator"
//Si l'utilisateur est bien connecté, et a les bons droits, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
    }else{
        //L'utilisateur n'a pas les bons droits
        header("Location: right_error.php");
        exit;
    }
}else{
    //L'utilisateur n'est pas connecté
    header("Location: login.php?errno=add_user");
    exit;
}


//Connexion à la base de données 
$pdo = connectToBdd();

//On initialise $message_error 
$message_error = "";

/**
 * ERRNO : CODE D'ERREUR 
 * 1 : PROBLEME TECHNIQUE (EXEPTION DANS LA PAGE USER ADDED, DONC PROBLEME SQL
 *                          --> DETAILS DANS LOGS APACHE /var/log/apache2/error.log)
 * 
 */

if(isset($_GET["errno"])){
    $errno = $_GET["errno"];

    switch ($errno) {
        case 1 : 
            $message_error = "Un problème technique est survenu, veuillez ré-essayer."; 
            break;
    }
}

//Affichage de l'entete html 
print_head("Login - EPI",'monitoring.css');

//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
'<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();


echo "<div class=\"content-wrapper\">",
        "<section class=\"content container-fluid\">",
            "<div class=\"box box-warning\">",
                "<div class=\"box-header with-border\">",
                    "<div class=\"box box-solid box-warning px-3 flex_login\">",

                        //Début du formulaire 
                        "<form action=\"user_added.php\" method=\"post\">",

                            //Le message d'erreur 
                            "<p class = \"validate_status_ok\" id=\"error_message\">$message_error</p>",

                            //Champs txt_username : nom d'utilisateur du nouvel user
                            "<label class=\"label_form required\">Username : </label>",
                            "<input required type=\"text\" id=\"txt_username\" name=\"txt_username\"><br>",

                            //Champs txt_mdp : mot de passe du nouvel user 
                            "<label class=\"label_form required\">Mot de passe : </label>",
                            "<input required id=\"txt_mdp\" name=\"txt_mdp\" type=\"password\"></input><br>",

                            //Champs txt_mdp_confirm : Confirmation du mot de passe 
                            "<label class=\"label_form required\">Confirmer le mot de passe : </label>",
                            "<input required id=\"txt_mdp_confirm\" name=\"txt_mdp_confirm\" type=\"password\"></input><br>",
                            
                            
                            //Champs txt_role_user : Role de l'user ("administrator", "integrator", "none")
                            "<label class=\"label_form required\">Quel est le role de l'utilisateur ? </label>",
                            "<input required type=\"text\" name=\"txt_role_user\" id=\"txt_role_user\" list=\"list_roles\"><br>",

                            //Création de la datalist dans laquelle on a tous les roles 
                            "<datalist id=\"list_roles\">",
                                "<option>administrator</option>",
                                "<option>integrator</option>",
                                "<option>none</option>",
                            "</datalist>",

                            //Validate status : message qui dit par exemple "les deux mots de passes ne correspondent pas"
                            "<p id=\"validate_status\" class=\"validate_status_ok\"></p><br>",
                            "<button class=\"btn btn_commentating\" id=\"btn_register\" disabled>Ajouter l'utilisateur</button>",
                            
                        "</form>",
                    "</div>",
                "</div>",
            "</div>",
        "</section>",
    "</div>";

echo "</div>";

//On inclue les scripts JS (mis à la fin pour l'optimisation)
print_scripts();
echo '<script src="../js/register.js"></script>';


echo "</body></html>";