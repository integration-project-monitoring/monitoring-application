<?php

/**
 * Page qui va afficher les détails d'un ArchiPEL HA 
 * 
 * Paramètres de la page : 
 *      - OBLIGATOIRE : 
 *          @param GET int $id : ID de l'ArchiPEL HA dont on veut les détails 
 */

//On inclut la librairie de fonctions, et la classe ArchiPEL
require_once "library_monitoring.php";
require_once "classes/Archipel.php";

//On initialise la session
session_start();

//Vérification du rôle et de l'utilisateur connecté 
//Cette page est en accès libre, mais ces informations nous serons utiles par la suite
$role = "";
if(isset($_SESSION["username_logged"])){
    $username = $_SESSION["username_logged"];
    $is_connected = 1;
    $role = $_SESSION["role_logged"];
}else{
    $username ="";
    $is_connected = 0;
}

//On vérifie que le paramètre $id est bien passé en GET, si ce n'est pas le cas, on redirige vers la page d'accueil des ArchiPEL HA
if(isset($_GET["id"])){
    $current_id = $_GET["id"];
}else{
    header("Location: new_index.php");
    exit;
}

//Connexion à la base de donnees
$pdo = connectToBdd();



//On récupère l'ArchiPEL correspondant à l'ID passé en paramètre 
$current_archipel = get_apl($pdo,$current_id);

//Affichage de l'entête html
print_head("$current_archipel->nom HA - EPI",'monitoring.css');


//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
'<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();

echo '<div class="content-wrapper">',
'<section class="content-header">',

//Grand titre au dessus du tableau 
"<h1>Informations ArchiPEL $current_archipel->nom HA</h1>",
'</section>';

echo '<section class="content container-fluid">';

//Début de la box dans laquelle se trouve le tableau
echo '<div class="box box-warning">',
'<div class="box-header with-border">',

//Sous-titre 
"<h3 class='box-title'>Liste de toutes les infos sur ArchiPEL $current_archipel->nom</h3>",
'</div>',
'<div class="box-body table-responsive no-padding">',

//Début du premier tableau
'<table class="table table-stripped table-condensed">';

//On affiche les premières entêtes du tableau
//  - Nom d'Archipel
//  - (Vide) pour l'icone de monitoring
//  - Nom de la stack docker 
//  - IHM
//  - LAB
//  - vm1
//  - vm2
//  - vm3
//  - Schémas oracle
//  - Instance oracle
//  - UUID Publication
//  - Version APL
//  - Version Core
//  - Version Java
//  - HSM
//  - Docker compose
//  - Path Installation
print_thead1_archipel_ha_details();


//On affiche les valeures correspondantes
echo '<tbody>';
print_tbody_archipel_ha($current_archipel, $pdo);
echo '</tbody>';

//Fin du premier tableau
echo '</table>';

//Début du deuxième tableau
echo '<table class="table table-stripped table-condensed">';

//On affiche les deuxièmes entêtes du tableau
// - IP vm1
// - SSH vm1
// - Port test auto vm1
// - IP vm2
// - SSH vm2
// - Port test auto vm2
// - IP vm3
// - SSH vm3
// - Port test auto vm3
print_thead2_archipel_ha_details();

//On affiche les valeures correspondantes
echo '<tbody>';
print_tbody_archipel_ha_details_vm($current_archipel, $pdo);
echo '</tbody>';

//On affiche les troisièmes entêtes du tableau
// - Version NFast
// - Version Docker
// - Version GlusterFS
// - Version Wildfly
// - Version Synchro
// - Version Ubuntu
print_thead3_archipel_ha_details();

//On affiche les valeures correspondantes
echo '<tbody>';
print_tbody_archipel_ha_details_versions($current_archipel, $pdo);
echo '</tbody>';

//On affiche l'entête et les valeurs de la partie service d'ArchiPEL HA
// - Nom service n
// - Port service n 
print_service_archipel_ha_details($current_archipel, $pdo);

//On crée deux lignes pour afficher l'id de l'ArchiPEL
print_id_archipel_details($current_archipel);

//Fin du deuxième tableau
echo '</table>';

//On affiche les boutons "supprimer l'ArchiPEL" et "Modifier l'ArchiPEL"
echo "<section class=\"content-footer flex_buttons\">",
        "<button class=\"btn btn_monitoring \" onclick=\"location.href='modify_apl_ha.php?id=$current_archipel->id';\">Modifer l'ArchiPEL</button>",
        "<button class=\"btn btn_suppress btn_add\" onclick=\"suppress_ha($current_archipel->id,'$current_archipel->nom');\"> Supprimer l'ArchiPEL</button>",
    "</section>",
'</div>',
'</div>',
'</section>';

//On affiche la partie commentaires ("Le formulaire de saisie d'un nouveau commentaire, et l'affichage de tous les commentaires passés)
print_comments($pdo,$current_archipel->id,"archipel_ha",$username,$role);

echo '</div>',
'</div>';





//Finalement, on inclue les scripts (Toujours à la fin pour l'optimisation)
echo '<script src="../js/suppress.js"></script>';
print_scripts();

echo '</body>','</html>';