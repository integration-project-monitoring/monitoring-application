<?php 

/**
 * Page qui va supprimer une VM (avec l'id passé par GET)
 */


require_once "library_monitoring.php";

session_start();

if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
        if(isset($_GET["id"])){
            $current_id = $_GET["id"];
        }else{
            header("Location: new_index.php");
            exit;
        }
    }else{
        header("Location: right_error.php");
        exit;
    }
}else{
    if(isset($_GET["id"])){
        $current_id = $_GET["id"];
        header("Location: login.php?errno=suppress_reef_ha&id=$current_id");
        exit;
    }else{
        header("Location: new_index.php");
        exit;
    }
    
    $username ="";
    $is_connected = 0;
}

print_head('Suppression de Reef - EPI ', 'monitoring.css');



$pdo = connectToBdd();

$current_reef = get_reef($pdo,$_GET["id"]);


 echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

    print_header();

    print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">';


 $sql = "DELETE FROM REEF WHERE id_reef = $current_reef->id ";

 try{
    $pdo->query($sql); 

    
   echo "<h3>Le Reef <strong>$current_reef->name_reef</strong> HA a bien été supprimé ! </h3>", 
       '<p>Vous allez être redirigé vers la liste des Reef HA dans <strong id="countdown">5 secondes</strong></p><br>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';

   echo '<script>', 
           'let i = 5;', 
           'let tmp = setInterval(countdown, 1000);',
           'function countdown() {',
               'if(i!=0) {',
                   'document.getElementById("countdown").innerHTML = i + " secondes";',
                   'i = i -1; ',
               '}',
               'else {',
                   'window.location.replace("reef_ha_overview.php");',
               '}',
           '}',
       '</script>';

   print_scripts();

   echo '</body>','</htlm>';
}catch (Exception $e){

   echo "<h3>Il y a eu une erreure technique dans la suppression du Reef $current_reef->name_reef HA</h3>", 
       '<p>La commande est : ',$sql,'<br>Veuillez contacter le développeur en lui copiant collant cette commande pour voir quel est le soucis </p>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';


   print_scripts();

   echo '</body>','</htlm>';
}
