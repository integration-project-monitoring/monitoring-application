<?php

/**
 * Page qui va afficher les détails d'un ArchiPEL Standalone
 * 
 * Paramètre de la page : 
 *      - OBLIGATOIRE : 
 *          @param GET int $id : ID de l'ArchiPEL Standalone dont on veut les détails
 */

//On inclut la librairie de fonctions, et la classe ArchiPEL
require_once "library_monitoring.php";
require_once "classes/Archipel.php";

//On initialise la session
session_start();

//Vérification du rôle et de l'utilisateur connecté 
//Cette page est en accès libre, mais ces informations nous serons utiles par la suite
$role = "";
if(isset($_SESSION["username_logged"])){
    $username = $_SESSION["username_logged"];
    $is_connected = 1;
    $role = $_SESSION["role_logged"];
}else{
    $username ="";
    $is_connected = 0;
}

//On vérifie que le paramètre $id est bien passé en GET, si ce n'est pas le cas, on redirige vers la page d'accueil des ArchiPEL Standalones
if(isset($_GET["id"])){
    $current_id = $_GET["id"];
}else{
    header("Location: apl_standalone_overview.php");
    exit;
}


//Connexion à la base de donnees
$pdo = connectToBdd();

//On récupère l'ArchiPEL correspondant à l'ID passé en paramètre
$current_archipel = get_apl($pdo,$_GET["id"]);


//Affichage de l'entête html
print_head("$current_archipel->nom Standalone - EPI",'monitoring.css');


//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
'<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();

echo '<div class="content-wrapper">',
'<section class="content-header">',

//Grand titre au dessus du tableau
"<h1>Informations ArchiPEL $current_archipel->nom (Standalone)</h1>",
'</section>';

echo '<section class="content container-fluid">';

//Début de la box dans laquelle se trouve le tableau
echo '<div class="box box-warning">',
'<div class="box-header with-border">',

//Sous-titre
"<h3 class='box-title'>Liste de toutes les infos sur ArchiPEL $current_archipel->nom</h3>",
'</div>',
'<div class="box-body table-responsive no-padding">',

//Début du premier tableau
'<table class="table table-stripped table-condensed">';

//On affiche les premières entêtes du tableau
// - Nom ArchiPEL
// - (vide) pour l'icone de monitoring
// - URL IHM 
// - LAB
// - VM
// - Schémas Oracle
// - Instance Oracle
// - UUID Publication
// - Version APL
// - Version Core
// - Version Java
// - HSM
// - Path installation
print_thead1_archipel_standalone_details();

//On affiche les valeures correspondantes
echo '<tbody>';
print_tbody_archipel_standalone($current_archipel, $pdo);
echo '</tbody>';

//Fin du premier tableau
echo '</table>';

//Début du deuxième tableau
echo '<table class="table table-stripped table-condensed">';

//On affiche les deuxièmes entêtes du tableau 
// - IP VM 
// - SSH VM
// - Port test auto 
print_thead2_archipel_standalone_details();

//On affiche les valeurs correspondantes
echo '<tbody>';
print_tbody_archipel_standalone_details_vm($current_archipel, $pdo);
echo '</tbody>';

//On affiche les troisièmes entêtes du tableau 
// - Version NFast
// - Version Wildfly
// - Version Synchro
// - Version Ubuntu
print_thead3_archipel_standalone_details();

//Affichage des valeurs correspondantes
echo '<tbody>';
print_tbody_archipel_standalone_details_versions($current_archipel, $pdo);
echo '</tbody>';

//On crée deux lignes pour afficher l'id de l'ArchiPEL
print_id_archipel_details($current_archipel);

//Fin du deuxième tableau
echo '</table>';

echo "<section class=\"content-footer flex_buttons\">",
        "<button class=\"btn btn_monitoring \" onclick=\"location.href='modify_apl_standalone.php?id=$current_archipel->id';\">Modifer l'ArchiPEL</button>",
        "<button class=\"btn btn_suppress btn_add\" onclick=\"suppress_standalone($current_archipel->id,'$current_archipel->nom');\"> Supprimer l'ArchiPEL</button>",
    "</section>",
'</div>',
'</div>',
'</section>';

//On affiche la partie commentaires ("Le formulaire de saisie d'un nouveau commentaire, et l'affichage de tous les commentaires passés)
print_comments($pdo,$current_archipel->id,"archipel_standalone",$username,$role);

echo '</div>',
'</div>';





//Finalement, on inclue les scripts (Toujours à la fin pour l'optimisation)
echo '<script src="../js/suppress.js"></script>';
print_scripts();

echo '</body>','</html>';