<?php 

require_once "library_monitoring.php";

session_start();

if(isset($_SESSION["username_logged"])){
    $username = $_SESSION["username_logged"];
    $is_connected = 1;
}else{
    $username ="";
    $is_connected = 0;
}


print_head("Monitoring - EPI", "monitoring.css");

$pdo = connectToBdd();

echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

print_header($is_connected,$username);

print_sidebar();

echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            "<div class=\"box box-solid box-warning px-3 flex_login\">",
                "<h2>Vous n'avez pas les droits nécessaires pour faire cette opération</h2>",
                "<h3>Veuillez demander les droits à un administrateur</h3><br>";

echo "</div></div></div></section></div></div>";

print_scripts();


echo "</body></html>";

