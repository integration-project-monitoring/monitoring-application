<?php 

require_once "library_monitoring.php";
require_once "classes/VM.php";

session_start();

$role = "";
if(isset($_SESSION["username_logged"])){
    $username = $_SESSION["username_logged"];
    $is_connected = 1;
    $role = $_SESSION["role_logged"];
}else{
    $username ="";
    $is_connected = 0;
}

//Connexion a la base de donnee 
$pdo = connectToBdd();

$current_vm = get_vm($pdo,$_GET["id"]);

$count_apl_linked = get_nb_apl_linked($pdo,$current_vm->id);
$is_apl_linked = 0;


$count_reef_linked = get_nb_reef_linked($pdo, $current_vm->id);
$is_reef_linked = 0;

if($count_reef_linked>0){
    $is_reef_linked = 1;
    $array_reef_linked = get_array_reef_linked($pdo, $current_vm->id);
}



if($count_apl_linked>0){
    $is_apl_linked = 1;
    $array_apl_linked = get_array_apl_linked($pdo,$current_vm->id);
}
//Affichage de l'entete en html 
print_head(" $current_vm->name_vm - EPI",'monitoring.css');



echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

print_header($is_connected,$username);

print_sidebar();

echo '<div class="content-wrapper">',
        '<section class="content-header">',
            '<h1>VM EPI (',$current_vm->name_vm,', utilisée dans ',$count_apl_linked,' vms)</h1>',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3 class="box-title">Détails de la VM ',$current_vm->name_vm,'</h3>',
        '</div>',
        '<div class="box-body table-responsive no-padding">',
            '<table class="table table-stripped table-condensed">';

print_thead_vms();


echo '<tbody>'; 


print_tbody_vm($current_vm, $pdo);



echo '</tbody>';


print_thead_vms_details();


echo '<tbody><tr>';


if($is_apl_linked){
    echo '<td>';
    foreach($array_apl_linked as $item){
        
        if(strlen($item->docker_compose)>0){
            echo "<a href=\"new_index.php?apl=$item->id\">$item->nom</a><br>";
        }else{
            echo "<a href=\"apl_standalone_overview.php?apl=$item->id\">$item->nom</a><br>";
        }
        
    }
    echo "</td>";
}else{
    echo '<td>Aucun</td>';
}

if($is_reef_linked){
    echo '<td>';
    foreach($array_reef_linked as $item){
        
        if(strlen($item->docker_compose)>0){
            echo "<a href=\"reef_ha_overview.php?reef=$item->id\">$item->name_reef</a><br>";
        }else{
            echo "<a href=\"reef_standalone_overview.php?reef=$item->id\">$item->name_reef</a><br>";
        }
        
    }
    echo "</td>";
}else{
    echo '<td>Aucun</td>';
}

echo '</tr></tbody>';

echo '</table>';

echo "<section class=\"content-footer flex_buttons\">",
        "<button class=\"btn btn_monitoring \" onclick=\"location.href='modify_vm.php?id=$current_vm->id';\">Modifer la VM</button>",
        "<button class=\"btn btn_suppress btn_add\" onclick=\"suppress_vm($current_vm->id,'$current_vm->name_vm',$is_apl_linked, $is_reef_linked);\"> Supprimer la VM</button>",
    "</section>",
'</div>',
'</div>',
'</section>';

print_comments($pdo,$current_vm->id,"vm",$username,$role);


echo '</div>',
'</div>';




//Finalement, on inclue les scripts
echo '<script src="../js/suppress.js"></script>';

print_scripts();

echo '</body>','</html>';