<?php

/*
 * Ce fichier sert a afficher le monitoring reef
 */

require_once "library_infoserv.php";

//Affichage de l'entete HTML

print_head('Monitoring EPI','perso.css');

//Connexion à la base de données
$pdo = connectToBdd();

$arrayReef = getArrayReef($pdo);

echo '<body>',
        '<div class="container">',
            '<header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">',
                '<a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">',
                    '<span class="fs-4">Monitoring Plateforme EPI</span>',
                '</a>',
                '<ul class="nav nav-pills">',
                    '<li class="nav-item" style="font-size: 1.2em!important;"><a href="add_reef.php" class="nav-link active">Ajouter un Reef</a></li>',
                    '<li class="nav-item" style="font-size: 1.2em!important;"><a href="index.php" class="nav-link ">ArchiPEL</a></li>',
                '</ul>',
            '</header>',
        '</div>',
        '<h1>REEF</h1>',
        '<div id="html" class="demo">';

//Affichage de l'arbre
$cpt = 0;
$current_platform = '';

foreach ($arrayReef as $reef){
    if(strcmp($reef->whatHA,$current_platform)!=0 && $cpt!=0){
        $cpt=-1;
    }
    if($cpt==0 || $cpt == -1){
        $current_platform = $reef->whatHA;
        if($cpt==0){
                echo "<ul>";
        }

        if($cpt==-1){
            echo '</ul></li></ul><ul>';
        }

        echo "<li data-jstree='{\"opened\" : true}'>$current_platform",
                    "<ul>";

        $cpt=4;
    }

    echo "<li data-jstree='{\"opened\" : true}'><strong>$reef->nom ($reef->version)</strong>",
            "<ul>",
                "<li>Utilisé sur : $reef->contexte</li>",
                "<li>Port TCP exterieur : $reef->portExt</li>",
                "<li>Port AFC/Simuhost : $reef->portSimuhost</li>",
                "<li>Port Neops : $reef->portNeops</li>",
                "<li>Port Ethernet Neops : $reef->portEthernet</li>",
                "<li>Port Nsquare : $reef->portNsquare</li>",
                "<li>Ports tests auto : $reef->testAuto</li>",
            "</ul>",
        "</li>";
}

echo '</li></ul></ul></div>';

echo '<script src="../jstree/dist/jstree.js"></script>',
            '<script>$(document).ready(function () {',
                '$("#html").jstree({"plugins" : ["themes","html_data","ui"] });',
                '$("#html li").on("click", "a.link",',
                'function() {',
                    'window.open(this,\'_blank\');',
                '}',
            ');',
        '});</script>';


echo '</body></html>';