<?php

class Reef
{
    //Id du reef en base
    public $id;

    //Nom du reef
    public $nom;

    //Version du reef
    public $version;

    //Port TCP Acces exterieur
    public $portExt;

    //Port AFC Simuhost
    public $portSimuhost;

    //Port Neops
    public $portNeops;

    //PortEthernetNeops
    public $portEthernet;

    //Port Nsquare
    public $portNsquare;

    //Recap contexte
    public $contexte;

    //Port de test auto
    public $testAuto;

    //Plateformes sur laquelle est installé le conteneur
    public $whatHA;

    /**
     * Reef constructor.
     * @param $id
     * @param $nom
     * @param $version
     * @param $portExt
     * @param $portSimuhost
     * @param $portNeops
     * @param $portEthernet
     * @param $portNsquare
     * @param $contexte
     * @param $testAuto
     * @param $whatHA
     */
    public function __construct($id, $nom, $version, $portExt, $portSimuhost, $portNeops, $portEthernet, $portNsquare, $contexte, $testAuto, $whatHA)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->version = $version;
        $this->portExt = $portExt;
        $this->portSimuhost = $portSimuhost;
        $this->portNeops = $portNeops;
        $this->portEthernet = $portEthernet;
        $this->portNsquare = $portNsquare;
        $this->contexte = $contexte;
        $this->testAuto = $testAuto;
        $this->whatHA = $whatHA;
    }

    public function __toString()
    {
        return $this->id . " " . $this->nom . " ". $this->version . " " . $this->portExt . " " . $this->portSimuhost . " " . $this->portNeops . " " . $this->portEthernet . " " . $this->portNsquare . " " . $this->contexte . " " . $this->testAuto . " " . $this->whatHA;
    }


}