<?php


require_once "library_monitoring.php";


session_start();


if(isset($_SESSION["username_logged"])){
    $username = $_SESSION["username_logged"];
    $is_connected = 1;
}else{
    $username ="";
    $is_connected = 0;
}


//Affichage de l'entête html 

print_head('ArchiPEL HA - EPI','monitoring.css');

//Connexion à la base de donnees
$pdo = connectToBdd();

echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

print_header($is_connected,$username);

print_sidebar();

echo '<div class="content-wrapper">',
        '<section class="content-header">',
            '<h1>Serveurs ArchiPEL HA</h1>',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3 class="box-title">Liste des différents ArchiPEL HA et de leur informations</h3>',
            '<button class = "btn btn_monitoring btn_add" onclick="location.href=\'add_apl_ha.php\';"> Ajouter un ArchiPEL</button>',
        '</div>',
        '<div class="box-body table-responsive no-padding">',
            '<table class="table table-stripped table-condensed">';

print_thead_archipel_ha();

$array_apl_ha = get_array_apl_ha($pdo);
echo '<tbody>';


foreach ($array_apl_ha as $item){
    print_tbody_archipel_ha($item, $pdo);
}





echo '</tbody>';

echo '</table>',
    '</div>',
'</div>',
'</section>',
'</div>',
    '</div>';





//Finalement, on inclue les scripts
print_scripts();

echo '</body>','</html>';

