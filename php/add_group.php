<?php 
/**
 * Page qui va afficher un formulaire pour ajouter un groupe de VMs
 * Champs nécessaires : 
 *      - Nom du groupe 
 *      - Description du groupe
 */

//On inclut la librairie de fonctions 
require_once "library_monitoring.php";

//On initialise la session
session_start();

//Vérification du rôle de l'utilisateur connecté 
// Roles acceptés : "integrator" ; "administrator"
//Si l'utilisateur est bien connecté, et a les bons droits, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
        if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
                $username = $_SESSION["username_logged"];
                $is_connected = 1;
                $role = $_SESSION["role_logged"];
        }else{
                //L'utilisateur n'a pas les bons droits
                header("Location: right_error.php");
                exit;
        }
}else{
        //L'utilisateur n'est pas connecté
        header("Location: login.php?errno=add_group");
        exit;
}

//Affichage de l'entete html 
print_head('Ajout Groupe VM - EPI', 'monitoring.css'); 

//Connexion à la base de données 
$pdo = connectToBdd(); 

//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();

echo '<div class="content-wrapper">',
        '<section class="content-header">',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3>Ajout d\'un groupe de VM :</h3>';

//DEBUT DU FORMULAIRE 
echo '<form action="group_added.php" method="post" enctype="multipart/form-data">';

//Champs txt_name_group --> Nom du groupe
echo '<label class="label_form required">Quel est le nom du groupe ? (Par exemple HA1) </label>',
        '<input required type="text" name="txt_name_group" id="txt_name_group"></br>';

//Champs txt_label_group --> description du groupe
echo '<label class="label_form required">Quel est la description du groupe ? </label>',
'<input required type="text" name="txt_label_group" id="txt_label_group"></br>';

echo '<button class="btn btn_monitoring"> Ajouter le groupe </button> ';
echo '</form>';
echo '</div>';

echo '</div>',
'</section>',
'</div>',
'</div>';

//On inclue les scripts JS (mis à la fin pour l'optimisation)
print_scripts();

echo '</body>','</html>';