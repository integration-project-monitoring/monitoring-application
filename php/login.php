<?php

/**
 * Page qui va afficher un formulaire pour se connecter à un compte sur la plateforme
 * 
 * Paramètres de la page : 
 *      - FALCULTATIF : 
 *          @param GET mixed $errno Soit un nombre pour afficher un message d'erreur, soit une String pour rediriger vers une page précise à la connexion
 */

require_once "library_monitoring.php";
require_once "classes/Archipel.php";

//Connexion à la base de données 
$pdo = connectToBdd();

session_start();


if(isset($_SESSION["username_logged"])){
    $_SESSION["username_logged"] ="";
}
if(isset($_SESSION["role_logged"])){
    $_SESSION["role_logged"] ="";
}



$message_error = "";

//$url_action --> L'url vers laquelle on va rediriger lorsque l'utilisateur cliquera sur "Se connecter"
$url_action = "logged.php";

/**
 * ERRNO : CODE D'ERREUR 
 * 1 : MDP OU USERNAME INCORRECT
 * "xxx" : L'UTILISATEUR VEUT ACCEDER A xxx MAIS N'EST PAS CONNECTE 
 */

if(isset($_GET["errno"])){
    $errno = $_GET["errno"];

    switch ($errno) {
        case "1" : 
            $message_error = "Pseudo ou mot de passe incorrect, veuillez ré-essayer."; 
            break;
        case "add_apl_ha":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            $url_action = "logged.php?link=add_apl_ha";
            break;
        case "add_apl_standalone":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            $url_action = "logged.php?link=add_apl_standalone";
            break;
        case "add_group":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            $url_action = "logged.php?link=add_group";
            break;
        case "add_lab":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            $url_action = "logged.php?link=add_lab";
            break;
        case "add_service_ha":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            if(isset($_GET["id"])){
                $current_id = $_GET["id"];
                $url_action = "logged.php?link=add_service_ha&id=$current_id";
            }else{
                $url_action = "logged.php";
            }
            break;
        case "add_reef_ha":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            $url_action = "logged.php?link=add_reef_ha";
            break;
        case "add_user":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            $url_action = "logged.php?link=add_user";
            break;    
        case "add_vm":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            $url_action = "logged.php?link=add_vm";
            break;
        case "modify_apl_ha":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            if(isset($_GET["id"])){
                $current_id = $_GET["id"];
                $url_action = "logged.php?link=modify_apl_ha&id=$current_id";
            }else{
                $url_action = "logged.php";
            }
            break;
        case "modify_apl_standalone":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            if(isset($_GET["id"])){
                $current_id = $_GET["id"];
                $url_action = "logged.php?link=modify_apl_standalone&id=$current_id";
            }else{
                $url_action = "logged.php";
            }
            break;
        case "modify_reef_ha":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            if(isset($_GET["id"])){
                $current_id = $_GET["id"];
                $url_action = "logged.php?link=modify_reef_ha&id=$current_id";
            }else{
                $url_action = "logged.php";
            }
            break;
        case "modify_vm":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            if(isset($_GET["id"])){
                $current_id = $_GET["id"];
                $url_action = "logged.php?link=modify_vm&id=$current_id";
            }else{
                $url_action = "logged.php";
            }
            break;
        case "suppress_apl_ha":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            if(isset($_GET["id"])){
                $current_id = $_GET["id"];
                $url_action = "logged.php?link=suppress_apl_ha&id=$current_id";
            }else{
                $url_action = "logged.php";
            }
            break;
        case "suppress_apl_standalone":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            if(isset($_GET["id"])){
                $current_id = $_GET["id"];
                $url_action = "logged.php?link=suppress_apl_standalone&id=$current_id";
            }else{
                $url_action = "logged.php";
            }
            break;
        case "suppress_reef_ha":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            if(isset($_GET["id"])){
                $current_id = $_GET["id"];
                $url_action = "logged.php?link=suppress_reef_ha&id=$current_id";
            }else{
                $url_action = "logged.php";
            }
            break;
        case "suppress_vm":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            if(isset($_GET["id"])){
                $current_id = $_GET["id"];
                $url_action = "logged.php?link=suppress_vm&id=$current_id";
            }else{
                $url_action = "logged.php";
            }
            break;
        case "my_account":
            $message_error = "Vous avez besoin de vous connecter pour accèder à cette page";
            $url_action ="logged.php?link=myaccount";
            break;      
    }

}



print_head("Login - EPI",'monitoring.css');

echo '<body class="hold-transition skin-black sidebar-mini">',
'<div class="wrapper">';

print_header();

print_sidebar();

echo "<div class=\"content-wrapper\">",
        "<section class=\"content container-fluid\">",
            "<div class=\"box box-warning\">",
                "<div class=\"box-header with-border\">",
                    "<div class=\"box box-solid box-warning px-3 flex_login\">",

                        //Début du formulaire 
                        "<form action=\"$url_action\" method=\"post\">",

                            //Message d'erreur 
                            "<p class = \"validate_status_ok\" id=\"error_message\">$message_error</p>",

                            //Champs txt_username --> Username
                            "<label class=\"label_form required\">Username : </label>",
                            "<input type=\"text\" id=\"txt_username\" name=\"txt_username\"><br>",

                            //Champs txt_mdp --> mot de passe
                            "<label class=\"label_form required\">Mot de passe : </label>",
                            "<input id=\"txt_mdp\" name=\"txt_mdp\" type=\"password\"></input><br>",
                            "<button class=\"btn btn_commentating\">CONNEXION</button>",
                        "</form>",
                    "</div>",
                "</div>",
            "</div>",
        "</section>",
    "</div>";

echo "</div>";



print_scripts();
echo '<script src="../js/login.js"></script>';


echo "</body></html>";