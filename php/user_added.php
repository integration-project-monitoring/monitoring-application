<?php 

//TO FINISH 


require_once "library_monitoring.php";


session_start();

if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
    }else{
        header("Location: right_error.php");
        exit;
    }
}else{
    header("Location: login.php?errno=add_user");
    exit;
    $username ="";
    $is_connected = 0;
}

print_head("Monitoring - EPI", "monitoring.css");

$pdo = connectToBdd();

echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

    print_header();

    print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">',
                "<div class=\"box box-solid box-warning px-3 flex_login\">";


$user_name = "'" . addslashes(htmlentities($_POST["txt_username"])) . "'";

$passwd = "'" . password_hash($_POST["txt_mdp"], PASSWORD_DEFAULT) . "'"; 

$date = date("Y-m-d"); 

if(isset($_POST["role"])){
    $role = "'" . $_POST["role"] . "'";
}else{
    $role = "'none'";
}
$sql = "INSERT INTO USERS (username_user, mdp_user, role_user, last_modified) VALUES ($user_name, $passwd, $role, '$date')";

try{
    $pdo->query($sql);

    echo "<h2>Le compte $user_name a bien été crée</h2>",

    "<h3>Veuillez demander les droits à un administrateur pour pouvoir ajouter des entrées dans l'outil</h3><br>",
    "<h4>Vous allez être redirigé vers la page d'accueil dans <strong id=\"countdown\">5 Secondes</strong></h4><br>";
    $_SESSION["username_logged"] = $user_name;


}
catch (Exception $e){
    header("Location: add_user.php?errno=1");
}

echo "</div></div></div></section></div></div>";

echo '<script>', 
'let i = 5;', 
'let tmp = setInterval(countdown, 1000);',
'function countdown() {',
    'if(i!=0) {',
        'document.getElementById("countdown").innerHTML = i + " secondes";',
        'i = i -1; ',
    '}',
    'else {',
        'window.location.replace("new_index.php");',
    '}',
'}',
'</script>';

print_scripts();


echo "</body></html>";

