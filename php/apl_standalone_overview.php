<?php
/**
 * Page d'accueil des ArchiPEL Standalone, on va afficher des informations en bref de tous les archiPEL standalones
 * 
 * Paramètres de la page : 
 *      - FACULTATIF : 
 *          @param GET int $apl L'id de l'ArchiPEL standalone que l'on va surligner (dans le cas où on est arrivé sur la page en cliquant sur un lien d'ArchiPEL) 
 */

//On inclut la librairie de fonctions
require_once "library_monitoring.php";

//On initialise la session 
session_start();

//Vérification du rôle et de l'utilisateur connecté 
//Cette page est en accès libre, mais ces informations nous serons utiles par la suite
if(isset($_SESSION["username_logged"])){
    $username = $_SESSION["username_logged"];
    $is_connected = 1;
}else{
    $username ="";
    $is_connected = 0;
}

//Affichage de l'entête html
print_head('ArchiPEL Standalone - EPI','monitoring.css');

//Connexion à la base de donnees
$pdo = connectToBdd();

//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
'<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();

echo '<div class="content-wrapper">',
'<section class="content-header">',
'<h1>Serveurs ArchiPEL Standalones</h1>',
'</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
'<div class="box-header with-border">',
'<h3 class="box-title">Liste des différents ArchiPEL Standalones et de leur informations</h3>',
'<button class = "btn btn_monitoring btn_add" onclick="location.href=\'add_apl_standalone.php\';"> Ajouter un ArchiPEL</button>',
'</div>',
'<div class="box-body table-responsive no-padding">',
'<table class="table table-stripped table-condensed">';

print_thead_archipel_standalone();

$array_apl_ha = get_array_apl_standalone($pdo);
echo '<tbody>';


foreach ($array_apl_ha as $item){
    print_tbody_archipel_standalone($item, $pdo);
}





echo '</tbody>';

echo '</table>',
'</div>',
'</div>',
'</section>',
'</div>',
'</div>';





//Finalement, on inclue les scripts
print_scripts();

echo '</body>','</html>';