<?php 

/**
 * Classe qui représente un commentaire sur une vm archipel, reef etc...
 */
class Comment
{
    //Id du commentaire en base
    public $id;

    //Contenu du commentaire
    public $txt_comment; 

    //Nom de l'auteur du commentaire
    public $name_author;

    //Id de l'archipel/reef/vm associé au commentaire
    public $id_associated;

    //Type d'objet auquel le commentaire est associé (peut prendre la valeur "archipel";"reef";"vm")
    public $type_associated;

    //Datetime généré quand le commentaire est ajouté
    public $time_comment;

    /**
     * Comment Constructor
     * @param $id
     * @param $txt_comment
     * @param $name_author
     * @param $id_associated
     * @param $type_associated
     * @param $time_comment
     */
    public function __construct($id, $txt_comment, $name_author, $id_associated, $type_associated, $time_comment)
    {
        $this->id = $id;
        $this->txt_comment = $txt_comment;
        $this->name_author = $name_author;
        $this->id_associated = $id_associated;
        $this->type_associated = $type_associated;
        $this->time_comment = $time_comment;
    }
}