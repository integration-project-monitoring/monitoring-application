<?php 

/**
 * Classe qui représente un service APL (HA ou Standalone Dockerisé)
 */
class ServiceApl
{
    //Nom du service
    public $name_service;

    //Port du service
    public $port_service; 


    /**
     * ServiceApl Constructor 
     * 
     * @param $name_service
     * @param $port_service
     * 
     */
    public function __construct($name_service, $port_service)
    {
        $this->name_service = $name_service;
        $this->port_service = $port_service;
    }

    public function __toString()
    {
        return $this->name_service . " (" . $this->port_service . ")";
    }
}