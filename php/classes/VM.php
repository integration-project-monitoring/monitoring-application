<?php 
/**
 * Classe qui represente une VM
 */
class VM
{
    //id de la vm
    public $id; 

    //Nom de la vm (rd-srvXXX)
    public $name_vm; 

    //Ip de la vm
    public $ip_vm;
    
    //Id du lab attaché a la VM
    public $id_lab;
    
    //Port ssh de la vm
    public $port_ssh;
    
    //Port sur lequel le sensor de test auto écoute 
    public $port_test_auto;
    
    //Version d'ubuntu sur la vm 
    public $version_ubuntu;
    
    //Version docker sur la vm
    public $version_docker;
    
    //Version glusterfs sur la vm
    public $version_glusterfs;
    
    //Disque total 
    public $total_disk;
    
    //Disque restant
    public $free_disk;

    //RAM totale
    public $total_ram;

    //Id du groupe de la VM en base
    public $id_group;

    //Type de la vm ("archipel", "reef" etc...)
    public $type_vm;

    //Booléen qui prend la valeur True si le script de monitoring est dans la crontab de la vm
    public $is_monitored;

    /**
     * VM Constructor 
     * 
     * @param $id
     * @param $name_vm
     * @param $ip_vm
     * @param $id_lab
     * @param $port_ssh
     * @param $port_test_auto
     * @param $version_ubuntu
     * @param $version_docker
     * @param $version_glusterfs
     * @param $total_disk
     * @param $free_disk
     * @param $total_ram
     * @param $id_group
     * @param $type_vm
     * @param $is_monitored
     */
    public function __construct($id, $name_vm, $ip_vm, $id_lab, $port_ssh, $port_test_auto, $version_ubuntu, $version_docker, $version_glusterfs, $total_disk, $free_disk, $total_ram, $id_group, $type_vm, $is_monitored)
    {
        $this->id = $id;
        $this->name_vm = $name_vm;
        $this->ip_vm = $ip_vm;
        $this->id_lab = $id_lab;
        $this->port_ssh = $port_ssh;
        $this->port_test_auto = $port_test_auto;
        $this->version_ubuntu = $version_ubuntu;
        $this->version_docker = $version_docker;
        $this->version_glusterfs = $version_glusterfs;
        $this->total_disk = $total_disk;
        $this->free_disk = $free_disk;
        $this->total_ram = $total_ram;
        $this->id_group = $id_group;
        $this->type_vm = $type_vm;
        $this->is_monitored = $is_monitored;
    }

    public function __toString()
    {
        return $this->id . " " .  $this->name_vm . " " .  $this->ip_vm . " " .  $this->id_lab . " " .  $this->port_ssh . " " .  $this->port_test_auto . " " .  $this->version_ubuntu . " " .  $this->version_docker . " " .  $this->version_glusterfs . " " .  $this->total_disk . " " .  $this->free_disk . " " .  $this->total_ram . " " . $this->id_group;
    }
}