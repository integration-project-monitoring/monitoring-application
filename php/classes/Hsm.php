<?php
/**
 * Classe qui représente un HSM
 */
class Hsm
{
    //ID de l'hsm en base
    public $id;

    //ip_hsm
    public $ip_hsm;

    //port_hsm
    public $port_hsm;

    //ip_rfs
    public $ip_rfs;

    //port_rfs
    public $port_rfs;

    //version hsm
    public $version_hsm;

    //label hsm
    public $label_hsm;


    /**
     * HSM Constructor 
     * @param $id
     * @param $ip_hsm
     * @param $port_hsm
     * @param $version_hsm
     * @param $label_hsm
     */
    public function __construct($id,$ip_hsm,$port_hsm,$version_hsm, $label_hsm)
    {
        $this->id = $id;
        $this->ip_hsm = $ip_hsm;
        $this->port_hsm = $port_hsm;
        $this->version_hsm = $version_hsm;
        $this->label_hsm = $label_hsm;
    }
}

