<?php 
/**
 * Classe qui représente un reef (HA ou Standalone)
 */
class Reef 
{
    //Id du reef en base
    public $id;

    //VM associée au reef, (la 1ère si le reef est HA)
    public $vm1;

    //2ème VM associée au reef HA
    public $vm2; 

    //3ème VM associée au reef HA
    public $vm3;

    //Nom du reef
    public $name_reef;

    //Nom de la stack du reef HA
    public $name_stack;

    //Port sur lequel écoute le reef pour des messages qui viennent d'une neops (ou simuhost/pytest)
    public $port_neops;

    //Port sur lequel écoute le reef en ethernet
    public $port_ethernet;

    //Port sur lequel écoute le reef pour des message qui viennent d'une machine nsquare
    public $port_nsquare;

    //Port sur lequel écoute le reef pour des messages qui viennent d'un simulateur (simuhost/pytest/simulateur DBR)
    public $port_simuhost;

    //Version du reef
    public $version_reef;

    //Version java (de la vm dans un reef standalone, de l'image docker dans un reef HA)
    public $version_java;

    //Version du client nfast
    public $version_nfast;

    //Version ubuntu (de la vm dans un reef standalone, de l'image docker dans un reef HA)
    public $version_ubuntu;
    
    //ID de la PKI associée au reef
    public $id_pki_associated;

    //ID du HSM associé au reef
    public $id_hsm_associated;

    //Path absolu vers le docker compose du reef HA
    public $docker_compose; 

    //Path vers le dossier du reef
    public $installation_path;

    //Type du reef (HA ou Standalone)
    public $type;

    //Booléen à True si le script de monitoring est dans la crontab de la vm associée au reef
    public $is_monitored;

    /**
     * Reef Constructor. 
     * @param $id
     * @param $vm1
     * @param $vm2
     * @param $vm3
     * @param $name_reef
     * @param $name_stack
     * @param $port_neops
     * @param $port_ethernet
     * @param $port_nsquare
     * @param $port_simuhost
     * @param $version_reef
     * @param $version_java
     * @param $version_nfast
     * @param $version_ubuntu
     * @param $id_pki_associated
     * @param $id_hsm_associated
     * @param $docker_compose
     * @param $installation_path
     * @param $type
     * @param $is_monitored
     */
    public function __construct($id, $vm1, $vm2, $vm3, $name_reef, $name_stack, $port_neops, $port_ethernet, $port_nsquare, $port_simuhost, $version_reef, $version_java, $version_nfast, $version_ubuntu, $id_pki_associated, $id_hsm_associated, $docker_compose, $installation_path, $type, $is_monitored)
    {
        $this->id = $id ;
        $this->vm1 = $vm1 ;
        $this->vm2 = $vm2 ;
        $this->vm3 = $vm3 ;
        $this->name_reef = $name_reef ;
        $this->name_stack = $name_stack ;
        $this->port_neops = $port_neops ;
        $this->port_ethernet = $port_ethernet ;
        $this->port_nsquare = $port_nsquare ;
        $this->port_simuhost = $port_simuhost ;
        $this->version_reef = $version_reef ;
        $this->version_java = $version_java ;
        $this->version_nfast = $version_nfast ;
        $this->version_ubuntu = $version_ubuntu ;
        $this->id_pki_associated = $id_pki_associated ;
        $this->id_hsm_associated = $id_hsm_associated ;
        $this->docker_compose = $docker_compose;
        $this->installation_path = $installation_path;
        $this->type = $type;
        $this->is_monitored = $is_monitored;
    }


    
}