<?php
/**
 * Classe qui représente un Archipel (HA ou Standalone)
 */
class Archipel
{
    //Id de l'archiPEL en base
    public $id;

    //Nom de l'ArchiPEL
    public $nom;

    //Nom de la stack de l'archipel
    public $name_stack;

    //id de la vm1
    public $vm1;

    //id de la vm2
    public $vm2;

    //id de la vm3
    public $vm3;

    //Schemas de la db oracle
    public $schemas_db;

    //Instance de la db oracle
    public $instance_db;

    //UUID de publication
    public $uuid;

    //Version d'apl
    public $version;

    //Version core
    public $version_core;

    //Version java
    public $version_java;

    //Version client nfast
    public $version_nfast;

    //Version du serveur wildfly
    public $version_wildfly;

    //Version d'ubuntu de la stack
    public $version_ubuntu_apl;

    //Version de l'ArchiPELPublicationResynchronizer
    public $version_synchro;

    //Id en base du hsm utilisé
    public $id_hsm;

    //url ihm
    public $url_ihm;

    //chemin vers le docker compose
    public $docker_compose;

    //path installation
    public $path_installation;

    //si l'archipel est monitoré ou pas 
    public $is_monitored;



    /**
     * Archipel constructor.
     * @param $id
     * @param $nom
     * @param $name_stack
     * @param $vm1
     * @param $vm2
     * @param $vm3
     * @param $schemas_db
     * @param $instance_db
     * @param $uuid
     * @param $version
     * @param $version_core
     * @param $version_java
     * @param $version_nfast
     * @param $version_wildfly
     * @param $version_ubuntu_apl
     * @param $version_synchro
     * @param $id_hsm
     * @param $url_ihm
     * @param $docker_compose
     * @param $path_installation
     * @param $is_monitored
     */
    public function __construct($id, $nom, $name_stack, $vm1, $vm2, $vm3, $schemas_db, $instance_db, $uuid, $version, $version_core, $version_java, $version_nfast, $version_wildfly, $version_ubuntu_apl, $version_synchro, $id_hsm, $url_ihm, $docker_compose, $path_installation, $is_monitored)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->name_stack = $name_stack;
        $this->vm1 = $vm1;
        $this->vm2 = $vm2;
        $this->vm3 = $vm3;
        $this->schemas_db = $schemas_db;
        $this->instance_db = $instance_db;
        $this->uuid = $uuid;
        $this->version = $version;
        $this->version_core = $version_core;
        $this->version_java = $version_java;
        $this->version_nfast = $version_nfast;
        $this->version_wildfly = $version_wildfly;
        $this->version_ubuntu_apl = $version_ubuntu_apl;
        $this->version_synchro = $version_synchro;
        $this->id_hsm = $id_hsm;
        $this->url_ihm = $url_ihm;
        $this->docker_compose = $docker_compose;
        $this->path_installation = $path_installation;
        $this->is_monitored = $is_monitored;
    }

    public function __toString() {
        return $this->id . " " .  $this->nom . " " .  $this->name_stack . " " .  $this->vm1 . " " .  $this->vm2 . " " .  $this->vm3 . " " .  $this->schemas_db . " " .  $this->instance_db . " " .  $this->uuid . " " .  $this->version . " " .  $this->version_core . " " .  $this->version_java . " " .  $this->version_nfast . " " .  $this->version_wildfly . " " .  $this->version_ubuntu_apl . " " .  $this->version_synchro . " " .  $this->id_hsm . " " .  $this->url_ihm . " " .  $this->docker_compose . " " .  $this->path_installation;
    }

}

