<?php 

/**
 * Page qui va construire la requete pour ajouter un groupe et indiquer si celui-ci a bien ete ajouté
 * On va dans un premier temps construire la requete, avec le parsage des champs entrés dans la derniere page,
 * puis ensuite on fera un petit affichage indiquant a l'utilisateur si c'est OK ou KO
 */

 //On inclut la librairie de fonctions 
 require_once "library_monitoring.php";

 //On initialise la session
 session_start();

 //Vérification du rôle de l'utilisateur connecté 
// Roles acceptés : "integrator" ; "administrator"
//Si l'utilisateur est bien connecté, et a les bons droits, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
 if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
         if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
                 $username = $_SESSION["username_logged"];
                 $is_connected = 1;
                 $role = $_SESSION["role_logged"];
         }else{
                //L'utilisateur n'a pas les bons droits
                 header("Location: right_error.php");
                 exit;
         }
 }else{
         //L'utilisateur n'est pas connecté
         header("Location: login.php?errno=add_group");
         exit;
 }

 //Affichage de l'entete en html 
 print_head('Ajout de groupe VM - EPI ', 'monitoring.css');

 //Connexion a la base de donnee 
 $pdo = connectToBdd();

 //Affichage du conteneur
 echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">';


 //On commence par parser la requete 

 //1. on extrait tous les inputs utilisateurs 
//On applique htmlentities() et addslashes() pour éviter les injections 

 $name_group = "'" . addslashes(htmlentities($_POST["txt_name_group"])) . "'";

 $label_group = "'" . addslashes(htmlentities($_POST["txt_label_group"])) . "'";

 $sql = "INSERT INTO VMS_GROUPS (name_grp, label_grp) VALUES ($name_group, $label_group)";

  //2. On éxecute la requête, s'il y a un problème on rentrera dans le catch, ou on affichera un message d'erreur
 // Le message d'erreur contient la requête SQL, pour pouvoir débugger aisèment
 try{
     $pdo->query($sql); 

     
    echo "<h3>Le groupe $name_group a bien été ajouté ! </h3>", 
        '<p>Vous allez être redirigé vers la page d\'ajout de vm dans <strong id="countdown">5 secondes</strong></p><br>';

    echo '</div>';

    echo '</div>',
    '</section>',
    '</div>',
    '</div>';

    //Script de compte à rebours
    echo '<script>', 
            'let i = 5;', 
            'let tmp = setInterval(countdown, 1000);',
            'function countdown() {',
                'if(i!=0) {',
                    'document.getElementById("countdown").innerHTML = i + " secondes";',
                    'i = i -1; ',
                '}',
                'else {',
                    'window.location.replace("add_vm.php");',
                '}',
            '}',
        '</script>';

    //On inclue les scripts JS (mis à la fin pour l'optimisation)
    print_scripts();

    echo '</body>','</htlm>';
 }catch (Exception $e){

    //Si on rentre ici c'est qu'il y a un problème dans la requête
    //Soit une erreur de saisie, un champs trop long etc... 
    //Astuce : copier coller la requête SQL dans phpmyadmin, l'erreur sera plus explicite ! 
    echo "<h3>Le groupe $name_group n'a pas bien été ajouté ! </h3>", 
        '<p>La commande est : ',$sql,'<br>Veuillez contacter le développeur en lui copiant collant cette commande pour voir quel est le soucis </p>';

    echo '</div>';

    echo '</div>',
    '</section>',
    '</div>',
    '</div>';

    //On inclue les scripts JS (mis à la fin pour l'optimisation)
    print_scripts();

    echo '</body>','</htlm>';
 }