<?php 
/**
 * Page qui va afficher un formulaire pour ajouter un lab 
 * Champs nécessaires : 
 *      - Nom du lab (tout le temps de la forme LABX)
 *      - IP du lab 
 *      - Nom de la vm qui gère le PFsense
 */

//On inclut la librairie de fonctions 
require_once "library_monitoring.php";

//On initialise la session
session_start();

//Vérification du rôle de l'utilisateur connecté 
// Roles acceptés : "integrator" ; "administrator"
//Si l'utilisateur est bien connecté, et a les bons droits, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
    }else{
        //L'utilisateur n'a pas les bons droits
        header("Location: right_error.php");
        exit;
    }
}else{
    //L'utilisateur n'est pas connecté
    header("Location: login.php?errno=add_lab");
    exit;
}



//Affichage de l'entete html 
print_head('Ajout LAB VM - EPI', 'monitoring.css'); 

//Connexion à la base de données 
$pdo = connectToBdd(); 

//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();

echo '<div class="content-wrapper">',
        '<section class="content-header">',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3>Ajout d\'un lab de VM :</h3>';

//DEBUT DU FORMULAIRE 
echo '<form action="lab_added.php" method="post" enctype="multipart/form-data">';

//Champs txt_name_lab --> Nom du lab (Tout le temps de la forme LABX)
echo '<label class="label_form required">Quel est le nom du lab ? (Par exemple LAB5) </label>',
        '<input required type="text" name="txt_name_lab" id="txt_name_lab"></br>';

//Champs txt_ip_lab --> IP du lab 
echo '<label class="label_form required">Quel est l\'ip du lab ? </label>',
'<input required type="text" name="txt_ip_lab" id="txt_ip_lab"></br>';

//Champs txt_name_vm_lab --> nom de la VM qui gère le PFSense
echo '<label class = "label_form required">Quelle est le nom de la vm du pfsense ? </label> <input type="text" name="txt_name_vm_lab" id="txt_name_vm_lab" list="list_name_vm">';
            
//On va récupérer dans un tableau toutes les vm 
$array_vm = get_array_vm($pdo);

//On va créer la datalist dans laquelle on a tous les noms de vm d'inscrits
echo '<datalist id=list_name_vm>';

foreach ($array_vm as $item){
    echo '<option>' , $item->name_vm , '</option>';
}

echo '</datalist><span style="margin-left:10px">Vous ne trouvez pas votre VM ? Veuillez l\'ajouter en mentionnant "NO LAB" dans la page précédente ! <span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">Ici</button><br>';


echo '<button class="btn btn_monitoring"> Ajouter le lab </button> ';
echo '</form>';
echo '</div>';

echo '</div>',
'</section>',
'</div>',
'</div>';

//On inclue les scripts JS (mis à la fin pour l'optimisation)
print_scripts();

echo '</body>','</html>';