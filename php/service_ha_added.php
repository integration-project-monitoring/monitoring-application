<?php 

/**
 * Page qui va construire la requete et indiquer si l'archipel a bien ete ajouté
 * On va dans un premier temps construire la requete, avec le parsage des champs entrés dans la derniere page,
 * puis ensuite on fera un petit affichage indiquant a l'utilisateur si c'est OK ou KO
 */

 require_once "library_monitoring.php";
 require_once "classes/ServiceApl.php";

 session_start();

 if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
     if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
         $username = $_SESSION["username_logged"];
         $is_connected = 1;
         $role = $_SESSION["role_logged"];
     }else{
         header("Location: right_error.php");
         exit;
     }
 }else{
     header("Location: login.php?errno=add_service_ha");
     exit;
     $username ="";
     $is_connected = 0;
 }
 

 print_head('Ajout de groupe VM - EPI ', 'monitoring.css');



 $pdo = connectToBdd();

 echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

    print_header($is_connected,$username);

    print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">';


 //On commence par parser la requete 

 //1. on extrait tous les inputs utilisateurs 

 $apl_id = $_GET["id"];
 


 $name_service1 = "'" . addslashes(htmlentities($_POST["txt_name_service1"])) . "'";

 if(strlen($_POST["txt_name_service2"]) > 0){
    $name_service2 = "'" . addslashes(htmlentities($_POST["txt_name_service2"])) . "'";
 }else{
     $name_service2 = NULL;
 }

 if(strlen($_POST["txt_name_service3"]) > 0){
    $name_service3 = "'" . addslashes(htmlentities($_POST["txt_name_service3"])) . "'";
 }else{
     $name_service3 = NULL;
 }

 $port_service1 = $_POST["int_port_service1"];

 if(strlen($_POST["int_port_service2"]) > 0 ){
     $port_service2 = $_POST["int_port_service2"];
 }else{
     $port_service2 = NULL;
 }

 if(strlen($_POST["int_port_service3"]) > 0 ){
    $port_service3 = $_POST["int_port_service3"];
}else{
    $port_service3 = NULL;
}

$array_services = []; 
array_push($array_services,new ServiceApl($name_service1,$port_service1));

if(isset($name_service2) && isset($port_service2)){
    array_push($array_services,new ServiceApl($name_service2,$port_service2));
}

if(isset($name_service3) && isset($port_service3)){
    array_push($array_services,new ServiceApl($name_service3,$port_service3));
}

$type_ha = "";

if(count($array_services) > 1){
    $type_ha = "'HA'";
}else{
    $type_ha = "'StandaloneDocker'";
}

 try{
    $current_sql = "";
    $current_service = NULL;

    foreach ($array_services as $item) {
        $current_sql = "INSERT INTO GRP_APL_HA (id_apl_associated, port_service, name_service, type_ha) VALUES ($apl_id, $item->port_service,$item->name_service,$type_ha)";
        $current_service = $item;
        $pdo->query($current_sql);
    }

      
    echo "<h3>";

    if(count($array_services) > 1){
        echo "Les ", count($array_services), " services ("; 
        foreach ($array_services as $item){
            echo $item;
        }
        echo ") ont bien été ajoutés";
    }else{
        echo "Le service $array_services[0] a bien été ajouté";
    }
     
    
    echo '<p>Vous allez être redirigé vers la page de l\'ArchiPEL dans <strong id="countdown">5 secondes</strong></p><br>';

    echo '</div>';

    echo '</div>',
    '</section>',
    '</div>',
    '</div>';

    echo '<script>', 
            'let i = 5;', 
            'let tmp = setInterval(countdown, 1000);',
            'function countdown() {',
                'if(i!=0) {',
                    'document.getElementById("countdown").innerHTML = i + " secondes";',
                    'i = i -1; ',
                '}',
                'else {',
                    'window.location.replace("add_vm.php");',
                '}',
            '}',
        '</script>';
    
    print_scripts();

    echo '</body>','</htlm>';
 }catch (Exception $e){

    echo "<h3>Le service $current_service n'a pas bien été ajouté ! </h3>", 
        '<p>La commande est : ',$current_sql,'<br>Veuillez contacter le développeur en lui copiant collant cette commande pour voir quel est le soucis </p>';

    echo '</div>';

    echo '</div>',
    '</section>',
    '</div>',
    '</div>';


    print_scripts();

    echo '</body>','</htlm>';
 }