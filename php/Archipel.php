<?php
/**
 * OBSOLETE : ANCIENNE PLATEFORME
 */

class Archipel
{
    //Id de l'archipel en base
    public $id;

    //Nom de l'archipel
    public $nom;

    //Description de l'archipel
    public $description;

    //uuid de l'archipel
    public $uuid;

    //Repertoire ou est installe l'archipel
    public $path_install;

    //Booleen qui dit si l'archipel est PCI ou non
    public $pci;

    //Version de l'archipel
    public $versionApl;

    //Version de l'archipel core
    public $versionCore;

    //Version de wildfly
    public $wildfly;

    //Version de Java
    public $java;

    //Version de l'archipel Synchronizer
    public $synchro;

    //Version du client nfast
    public $nfast;

    //outils additionnels contenus dans l'archipel
    public $tools;

    //Version de gluster fs
    public $glusterfs;

    //Version de docker
    public $docker;

    //Nom et version de l'os
    public $os;

    //Booleen qui dit si l'archipel est HA ou non
    public $ha;

    //Sur quel kit de HA est installe archipel
    public $whatHa;

    //Entier qui dit le numero du lab
    public $lab;

    //sur quelles plateformes est installe archipel (pour les HA mettre "srvxxx;srvxxx;srvxxx")
    public $srv;

    //Quelles sont les ip d'archipel (pour les HA mettre "xxx.xxx.xxx.xxx;xxx.xxx.xxx.xxx;xxx.xxx.xxx.xxx")
    public $ip;

    //Port de test auto
    public $portTestAuto;

    //L'instance de la DB oracle d'archipe
    public $instanceDB;

    //Le nom des users de la DB oracle de l'archpel
    public $shemasDB;

    //Lien vers l'IHM D'archipel
    public $lienIHM;

    //Port ssh
    public $ssh;

    //Port neops
    public $neops;

    //Port ethernet
    public $ethernet;

    //Port Nsquare
    public $nsquare;

    //Port simuhost (port du reef dédié)
    public $simuhost;

    //Port acces exterieur
    public $portExt;

    //Disque alloué a archipel (pour les HA mettre "disqueVM1;disqueVM2;disqueVM3")
    public $disque;

    //Ram alloué a archipel (pour les HA mettre "ramVM1;ramVM2;ramVM3")
    public $ram;

    //Port et stacks correspondantes (exemple : "18108:eisop-ha-tx;18208:eisop-ha-admin;18308:eisop-ha-param")
    public $apl;

    //Reef dédié (pour les HA la plupart du temps)
    public $reef;

    //Nom du serveur de publication dédié
    public $publication;

    //Certification ou non
    public $certif;
    /**
     * Archipel constructor.
     * @param $id
     * @param $nom
     * @param $description
     * @param $uuid
     * @param $path_install
     * @param $pci
     * @param $versionApl
     * @param $versionCore
     * @param $wildfly
     * @param $java
     * @param $synchro
     * @param $nfast
     * @param $tools
     * @param $glusterfs
     * @param $docker
     * @param $os
     * @param $ha
     * @param $whatHa
     * @param $lab
     * @param $srv
     * @param $ip
     * @param $portTestAuto
     * @param $instanceDB
     * @param $shemasDB
     * @param $lienIHM
     * @param $ssh
     * @param $neops
     * @param $ethernet
     * @param $nsquare
     * @param $simuhost
     * @param $portExt
     * @param $disque
     * @param $ram
     * @param $apl
     * @param $reef
     * @param $publication
     * @param $certif
     */
    public function __construct($id, $nom, $description, $uuid, $path_install, $pci, $versionApl, $versionCore, $wildfly, $java, $synchro, $nfast, $tools, $glusterfs, $docker, $os, $ha, $whatHa, $lab, $srv, $ip, $portTestAuto, $instanceDB, $shemasDB, $lienIHM, $ssh, $neops, $ethernet, $nsquare, $simuhost, $portExt, $disque, $ram, $apl, $reef, $publication, $certif)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->description = $description;
        $this->uuid = $uuid;
        $this->path_install = $path_install;
        $this->pci = $pci;
        $this->versionApl = $versionApl;
        $this->versionCore = $versionCore;
        $this->wildfly = $wildfly;
        $this->java = $java;
        $this->synchro = $synchro;
        $this->nfast = $nfast;
        $this->tools = $tools;
        $this->glusterfs = $glusterfs;
        $this->docker = $docker;
        $this->os = $os;
        $this->ha = $ha;
        $this->whatHa = $whatHa;
        $this->lab = $lab;
        $this->srv = $srv;
        $this->ip = $ip;
        $this->portTestAuto = $portTestAuto;
        $this->instanceDB = $instanceDB;
        $this->shemasDB = $shemasDB;
        $this->lienIHM = $lienIHM;
        $this->ssh = $ssh;
        $this->neops = $neops;
        $this->ethernet = $ethernet;
        $this->nsquare = $nsquare;
        $this->simuhost = $simuhost;
        $this->portExt = $portExt;
        $this->disque = $disque;
        $this->ram = $ram;
        $this->apl = $apl;
        $this->reef = $reef;
        $this->publication = $publication;
        $this->certif = $certif;
    }

    public function __toString()
    {
        return $this->id . "  " . $this->nom . "  " . $this->description . "  " . $this->uuid . "  " . $this->path_install . "  " . $this->pci . "  " . $this->versionApl . "  " . $this->versionCore . "  " . $this->wildfly . "  " . $this->java . "  " . $this->synchro . "  " . $this->nfast . "  " . $this->tools . "  " . $this->glusterfs . "  " . $this->docker . "  " . $this->os . "  " . $this->ha . "  " . $this->whatHa . "  " . $this->lab . "  " . $this->srv . "  " . $this->ip . "  " . $this->portTestAuto . "  " . $this->instanceDB . "  " . $this->shemasDB . "  " . $this->lienIHM . "  " . $this->ssh . "  " . $this->neops . "  " . $this->ethernet . "  " . $this->nsquare . "  " . $this->simuhost . "  " . $this->portExt . "  " . $this->disque . "  " . $this->ram . "  " . $this->apl . " " . $this->reef . " " . $this->publication . " " . $this->certif;
    }

    public static function compare_archipel_by_kitHA($a,$b){
        $value_a = $a->whatHa;
        $value_b = $b->whatHa;

        return strcmp($value_a,$value_b);
    }


}