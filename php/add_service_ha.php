<?php 
/**
 * Page qui va afficher un formulaire pour ajouter un ou plusieurs service pour archipel HA
 * 
 * Champs nécessaires : 
 *      - Nom du premier service 
 *      - Port du premier service 
 * Champs facultatifs (invisible à l'affichage de la page, puis révelés lorsque l'on clique sur "n-eme service") : 
 *      - Nom du n-ieme service
 *      - Port du n-ieme service
 * 
 * Paramètres de la page : 
 *      - OBLIGATOIRE : 
 *          @param GET int $id : ID de l'archiPEL HA auquel on veut ajouter le service 
 * 
 */

//On inclut la librairie de fonctions 
require_once "library_monitoring.php";

//On initialise la session
session_start();

//On récupère le parametre id qu'on récupère en GET, qui représente l'id de l'ArchiPEL auquel le service est associé 
if(isset($_GET["id"])){
    $current_id = $_GET["id"];
}else{
    //Il n'y a pas d'id dans l'url, revient à la page d'accueil d'archiPEL HA
    header("Location: new_index.php");
    exit;
}


//Vérification du rôle de l'utilisateur connecté 
// Roles acceptés : "integrator" ; "administrator"
//Si l'utilisateur est bien connecté, et a les bons droits, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
    }else{
        //L'utilisateur n'a pas les bons droits
        header("Location: right_error.php");
        exit;
    }
}else{
    //L'utilisateur n'est pas connecté
    header("Location: login.php?errno=add_service_ha&id=$current_id");
    exit;
}

//Affichage de l'entete html 
print_head('Ajout Service VM - EPI', 'monitoring.css'); 

//Connexion à la base de données 
$pdo = connectToBdd(); 

//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();


echo '<div class="content-wrapper">',
        '<section class="content-header">',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3>Ajout de services ArchiPEL HA :</h3>';

//DEBUT DU FORMULAIRE 
echo '<form action="service_ha_added.php?id=', $current_id ,'" method="post" enctype="multipart/form-data">';

//Champs txt_name_service1 --> nom du service 1 
echo '<label class="label_form required">Quel est le nom du premier service ? (Par exemple archipel-eisop-tx) </label>',
        '<input required type="text" name="txt_name_service1" id="txt_name_service1"></br>';

//Champs int_port_service1 --> port du service 1 
echo '<label class="label_form required">Quel est le port du premier service ?</label>',
'<input required type="number" name="int_port_service1" id="int_port_service1"></br>';

//Bloc service 2 
echo '<div id="div1" style="display:none;">';

    echo '<label class="label_form">Quel est le nom du deuxieme service ? (Par exemple archipel-eisop-tx) </label>',
    '<input type="text" name="txt_name_service2" id="txt_name_service2"></br>';

    echo '<label class="label_form ">Quel est le port du deuxieme service ?</label>',
    '<input type="number" name="int_port_service2" id="txt_port_service2"></br>';

echo '</div>';

//Bloc service 3
echo '<div id="div2" style="display:none;">';

    echo '<label class="label_form ">Quel est le nom du troisieme service ? (Par exemple archipel-eisop-tx) </label>',
    '<input  type="text" name="txt_name_service3" id="txt_name_service3"></br>';

    echo '<label class="label_form">Quel est le port du troisieme service ?</label>',
    '<input  type="number" name="int_port_service3" id="txt_port_service3"></br>';

echo '</div>';

//Bloc service 4
echo '<div id="div3" style="display:none;">';

    echo '<label class="label_form ">Quel est le nom du quatrième service ? (Par exemple archipel-eisop-tx) </label>',
    '<input  type="text" name="txt_name_service4" id="txt_name_service4"></br>';

    echo '<label class="label_form">Quel est le port du quatrième service ?</label>',
    '<input  type="number" name="int_port_service4" id="txt_port_service4"></br>';

echo '</div>';

//Bloc service 5
echo '<div id="div4" style="display:none;">';

    echo '<label class="label_form ">Quel est le nom du cinquième service ? (Par exemple archipel-eisop-tx) </label>',
    '<input  type="text" name="txt_name_service5" id="txt_name_service5"></br>';

    echo '<label class="label_form">Quel est le port du cinquième service ?</label>',
    '<input  type="number" name="int_port_service5" id="txt_port_service5"></br>';

echo '</div>';

echo '<button type="button" class="btn btn_monitoring" id ="btn_dynamic">2eme service</button><br>';

echo '<button class="btn btn_monitoring"> Ajouter le groupe </button> ';
echo '</form>';
echo '</div>';

echo '</div>',
'</section>',
'</div>',
'</div>';

//On inclue les scripts JS (mis à la fin pour l'optimisation)
echo '<script src="../js/add_service.js"></script>';
print_scripts();

echo '</body>','</html>';