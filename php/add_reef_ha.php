<?php 
/**
 * Page qui va afficher un formulaire pour ajouter un reef HA 
 * Champs nécessaires : 
 *      - Nom de la première VM
 *      - Nom de la deuxième VM 
 *      - Nom de la troisième VM 
 *      - Nom du reef HA
 *      - Nom de la stack du reef HA
 *      - Path absolu vers l'installation du reef HA 
 *      - Path absolu vers le docker compose du reef HA 
 *      - HSM vers lequel est branché le reef HA 
 * Champs facultatifs : 
 *      - Port Neops 
 *      - Port Ethernet
 *      - Port NSquare
 *      - Port Simuhost
 */

//On inclut la librairie de fonctions 
require_once "library_monitoring.php";

//On initialise la session
session_start();

//Vérification du rôle de l'utilisateur connecté 
// Roles acceptés : "integrator" ; "administrator"
//Si l'utilisateur est bien connecté, et a les bons droits, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
    }else{
        //L'utilisateur n'a pas les bons droits
        header("Location: right_error.php");
        exit;
    }
}else{
    //L'utilisateur n'est pas connecté
    header("Location: login.php?errno=add_reef_ha");
    exit;
}



//Affichage de l'entete en html 
print_head('Ajout Reef HA - EPI','monitoring.css');


//Connexion a la base de donnee 
$pdo = connectToBdd();

//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();

echo '<div class="content-wrapper">',
        '<section class="content-header">',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3>Ajout d\'un Reef HA :</h3>';

//DEBUT DU FORMULAIRE 
echo '<form action="reef_ha_added_new.php" method="post">';

//Champs txt_name_vm --> Nom de la première VM
echo '<label class="label_form required">Quelle est la première VM ? </label> <input required type="text" name="txt_name_vm" id="txt_name_vm" list="list_name_vm">';
            
//On va récupérer dans un tableau toutes les vm 
$array_vm = get_array_vm($pdo);

//On va créer la datalist dans laquelle on a tous les noms de vm d'inscrits
echo '<datalist id=list_name_vm>';

foreach ($array_vm as $item){
    echo '<option>' , $item->name_vm , '</option>';
}

echo '</datalist><span style="margin-left:10px">Vous ne trouvez pas votre VM ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">L\'ajouter</button><br>';

//Champs txt_name_vm2 --> Nom de la deuxième VM
echo '<label class="label_form required">Quelle est la deuxième VM ?</label> <input required type="text" name="txt_name_vm2" id="txt_name_vm2" list="list_name_vm">';
echo '<span style="margin-left:10px">Vous ne trouvez pas votre VM ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">L\'ajouter</button><br>';

//Champs txt_name_vm3 --> Nom de la troisième VM 
echo '<label class="label_form required">Quelle est la troisième VM ?</label> <input required type="text" name="txt_name_vm3" id="txt_name_vm3" list="list_name_vm">';
echo '<span style="margin-left:10px">Vous ne trouvez pas votre VM ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">L\'ajouter</button><br>';

//Champs txt_name_reef --> nom du reef HA
echo '<label class="label_form required">Quel est le nom du Reef ?</label>',
        '<input required type="text" name="txt_name_reef" id="txt_name_reef"><br>';

//Champs txt_name_stack_reef --> nom de la stack Reef HA
echo '<label class="label_form required">Quel est le nom de la stack ?</label>',
    '<input required type="text" name="txt_name_stack_reef" id="txt_name_stack_reef"><br>';

//Champs txt_installation_path_reef --> Path absolu vers le dossier d'installation du reef HA
echo '<label class="label_form required">Quel est le path d\'installation de reef ?</label>',
    '<input required type="text" name="txt_installation_path_reef" id="txt_installation_path_reef" value="/opt/reef"><br>';

//Champs txt_dockercompose_path_reef --> Path absolu vers le docker compose du reef HA
echo '<label class="label_form required">Quel est le path vers le docker compose ?</label>',
    '<input required type="text" name="txt_dockercompose_path_reef" id="txt_dockercompose_path_reef" value="/opt/docker-compose/..."><br>';   

//Champs txt_hsm_reef --> label du HSM auquel le reef est connecté
echo '<label class="label_form required">Sur quel HSM est-il branché ?</label>',
    '<input required type="text" name="txt_hsm_reef" id="txt_hsm_reef" list="list_hsm"><br>';

//Champs int_neops --> port neops 
echo '<label class="label_form">Quel est le port Neops ?</label>',
    '<input type="number" name="int_neops" id="int_neops"><br>';

//Champs int_ethernet --> port ethernet 
echo '<label class="label_form ">Quel est le port Ethernet ?</label>',
    '<input type="number" name="int_ethernet" id="int_ethernet"><br>';

//Champs int_nsquare --> port nsquare 
echo '<label class="label_form ">Quel est le port NSquare ?</label>',
    '<input type="number" name="int_nsquare" id="int_nsquare"><br>';

//Champs int_simuhost --> port simuhost 
echo '<label class="label_form ">Quel est le port Simuhost ?</label>',
    '<input type="number" name="int_simuhost" id="int_simuhost"><br>';

//On récupère tous les HSM dans un tableau
$array_hsm = get_array_hsm($pdo);

//On va créer la datalist dans laquelle on a tous les label d'HSM d'inscrits
echo '<datalist id=list_hsm>';

foreach ($array_hsm as $item){
    echo '<option>', $item->label_hsm, '</option>';
}

echo '</datalist>';


echo '<button class="btn btn_monitoring"> Ajouter le REEF</button>';
            
echo '</div>';

echo '</div>',
'</section>',
'</div>',
'</div>';

//On inclue les scripts JS (mis à la fin pour l'optimisation)
print_scripts();

echo '</body>','</html>';