<?php 
require_once "library_monitoring.php";

session_start();

if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
        if(isset($_GET["id"])){
            $current_id = $_GET["id"];
        }else{
            header("Location: vms_overview.php");
            exit;
        }
    }else{
        header("Location: right_error.php");
        exit;
    }
}else{
    if(isset($_GET["id"])){
        $current_id = $_GET["id"];
        header("Location: login.php?errno=modify_vm&id=$current_id");
        exit;
    }else{
        header("Location: vms_overview.php");
        exit;
    }
    
    $username ="";
    $is_connected = 0;
}

//Affichage de l'entete html 
print_head('Modification VM - EPI', 'monitoring.css'); 

//Connexion à la base de données 
$pdo = connectToBdd(); 

$current_vm = get_vm($pdo, $current_id);

if(strlen($current_vm->id_group) > 0){
    $current_group = get_group_name_from_id($pdo,$current_vm->id_group);
}else{
    $current_group = "";
}



if(strlen($current_vm->id_lab)>0){
    $current_lab = "LAB" . $current_vm->id_lab;
}else{
    $current_lab = "NO LAB";
}


echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

print_header();

print_sidebar();

echo '<div class="content-wrapper">',
        '<section class="content-header">',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3>Modification de ',$current_vm->name_vm,' :</h3>';

echo '<form action="vm_modified.php?id=',$current_vm->id,'" method="post">';

echo    '<label class="label_form">De quel groupe fait parti la VM ? (Laisser vide si standalone) </label> <input type="text" name="txt_name_group" id="txt_name_group" list="list_name_group" value="',$current_group,'">';
                
$array_group = get_array_group($pdo);
    
echo '<datalist id=list_name_group>';
    
    foreach ($array_group as $item){
        echo '<option>' , $item , '</option>';
    }
    
echo '</datalist><span style="margin-left:10px">Vous ne trouvez pas votre groupe ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_group.php\';">L\'ajouter</button> <br>';
    
echo '<label class="label_form required">De quel lab fait parti la VM ? ("NO LAB" pour une vm qui est hors lab) </label>', 
    '<input required type="text" name="txt_lab" id="txt_lab" list="list_lab" value="',$current_lab,'">';

$array_lab = get_array_lab($pdo);

echo '<datalist id=list_lab>'; 

foreach ($array_lab as $item){
    echo '<option>', $item, '</option>';
}

echo '<option>NO LAB</option></datalist>';

echo '<span style="margin-left:10px">Vous ne trouvez pas votre lab ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_lab.php\';">L\'ajouter</button> <br>';

echo     '<label class="label_form required">Quel est le nom de la VM ? (Par exemple rd-srv528) </label>',
        '<input required type="text" name="txt_name_vm" id="txt_name_vm" value="',$current_vm->name_vm,'"></br>';            
    
echo     '<label class="label_form required">Quel est l\'ip de la VM ? </label>',
        '<input required type="text" name="txt_ip_vm" id="txt_ip_vm" value="',$current_vm->ip_vm,'"></br>';      

echo '<label class="label_form required">Quel est le type de la VM ? </label>', 
    '<input required type="text" name="txt_type_vm" id="txt_type_vm" list="list_type" value="',$current_vm->type_vm,'"><br>';

$array_type_vm = get_array_type_vm($pdo);

echo '<datalist id=list_type>'; 

foreach ($array_type_vm as $item){
    echo '<option>', $item, '</option>';
}

echo '</datalist>';

echo     '<label class="label_form required">Quel est le port ssh de la VM ? </label>',
        '<input required type="text" name="txt_ssh_vm" id="txt_ssh_vm" value="',$current_vm->port_ssh,'"></br>';    

echo     '<label class="label_form ">Quel est le port de test auto de la VM ? </label>',
        '<input type="text" name="txt_portauto_vm" id="txt_portauto_vm" value="',$current_vm->port_test_auto,'"></br>';    

echo '<button class="btn btn_monitoring"> Appliquer les modifs</button> ';

echo '</form>';


echo '</div>';

echo '</div>',
'</section>',
'</div>',
'</div>';

print_scripts();

echo '</body>', '</html>';