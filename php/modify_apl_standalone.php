<?php 
require_once "library_monitoring.php";

session_start();

if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
        if(isset($_GET["id"])){
            $current_id = $_GET["id"];
        }else{
            header("Location: apl_standalone_overview.php");
            exit;
        }
    }else{
        header("Location: right_error.php");
        exit;
    }
}else{
    if(isset($_GET["id"])){
        $current_id = $_GET["id"];
        header("Location: login.php?errno=modify_apl_standalone&id=$current_id");
        exit;
    }else{
        header("Location: apl_standalone_overview.php");
        exit;
    }
    
    $username ="";
    $is_connected = 0;
}



//Affichage de l'entete en html 
print_head('Modification ArchiPEL STANDALONE - EPI','monitoring.css');


//Connexion a la base de donnee 
$pdo = connectToBdd();

$current_apl = get_apl($pdo, $current_id);
$name_vm_associated = get_name_vm_from_id($pdo, $current_apl->id);
$label_hsm = get_hsm_label_from_id($pdo, $current_apl->id_hsm);

echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

print_header($is_connected,$username);

print_sidebar();

echo '<div class="content-wrapper">',
        '<section class="content-header">',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3>Modification d\'ArchiPEL', $current_apl->nom,' Standalone :</h3>';

echo "<form action=\"apl_standalone_modified_new.php?id=$current_apl->id\" method=\"post\">";




echo '<label class="label_form required">Sur quelle VM est installé ArchiPEL ? </label> <input type="text" name="txt_name_vm" id="txt_name_vm" list="list_name_vm" value="',$name_vm_associated,'">';
            
$array_vm = get_array_vm($pdo);

echo '<datalist id=list_name_vm>';

foreach ($array_vm as $item){
    echo '<option>' , $item->name_vm , '</option>';
}

echo '</datalist><span style="margin-left:10px">Vous ne trouvez pas votre VM ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">L\'ajouter</button><br>';

echo '<label class="label_form required">Quel est le nom de l\'archipel ?</label>',
        '<input required type="text" name="txt_name_apl" id="txt_name_apl" value="',$current_apl->nom,'"><br>';

echo '<label class="label_form required">Quel est le path d\'installation d\'archipel ?</label>',
    '<input required type="text" name="txt_installation_path_apl" id="txt_installation_path_apl" value="',$current_apl->path_installation,'"><br>';

echo '<label class="label_form required">Sur quel HSM est-il branché ?</label>',
    '<input required type="text" name="txt_hsm_apl" id="txt_hsm_apl" list="list_hsm" value="',htmlentities($label_hsm),'"><br>';

echo '<label class="label_form required">Quel est le lien vers l\'IHM ?</label>',
    '<input required type="text" name="txt_link_ihm_apl" id="txt_link_ihm_apl" value="',$current_apl->url_ihm,'"><br>';

$array_hsm = get_array_hsm($pdo);

echo '<datalist id=list_hsm>';

foreach ($array_hsm as $item){
    echo '<option>', $item->label_hsm, '</option>';
}

echo '</datalist>';


echo '<button class="btn btn_monitoring"> Appliquer les modifs</button>';
            
echo '</div>';

echo '</div>',
'</section>',
'</div>',
'</div>';

print_scripts();

echo '</body>','</html>';