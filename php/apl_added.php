<?php

/**
 * ANCIENNE VERSION DE LA PLATEFORME : OBSOLETE !!!!!!!!!!!!
 */

/**
 * Page qui va construire la requete et indiquer si l'archipel a bien ete ajouté
 * On va dans un premier temps construire la requete, avec le parsage des champs entrés dans la derniere page,
 * puis ensuite on fera un petit affichage indiquant a l'utilisateur si c'est OK ou KO
 */

require_once "library_infoserv.php";


print_head('Ajout d\'ARCHIPEL','perso.css');


//On commence par parser la requete

$pdo = connectToBdd();
//1. on extrait tous les inputs utilisateurs
$nom = "'" . htmlentities($_POST["txt_nom"]) . "'";
$description = "'" . htmlentities($_POST["txt_description"]) . "'";
$uuid = "'" . htmlentities($_POST["txt_uuid"]) . "'";
$path_install = "'" . htmlentities($_POST["txt_path_install"]) . "'";
$pci = "'" . htmlentities($_POST["radio_pci"]) . "'";
$version_apl = "'" . htmlentities($_POST["txt_version_apl"]) . "'";
$version_core = "'" . htmlentities($_POST["txt_version_core"]) . "'";
$wildfly = "'" . htmlentities($_POST["txt_version_wildfly"]) . "'";
$java = "'" . htmlentities($_POST["txt_version_java"]) . "'";
$synchro = "'" . htmlentities($_POST["txt_version_synchro"]) . "'";
$nfast = "'" . htmlentities($_POST["txt_version_nfast"]) . "'";
$tools = "NULL";

if(strlen(htmlentities($_POST["txt_version_glusterfs"]))==0){
    $glusterfs = "NULL";
}else {
    $glusterfs = "'" . htmlentities($_POST["txt_version_glusterfs"]) . "'";
}

if(strlen(htmlentities($_POST["txt_version_docker"]))==0){
    $docker = "NULL";
}else {
    $docker = "'" . htmlentities($_POST[""]) . "'";
}

$os = "'" . htmlentities($_POST["txt_version_os"]) . "'";
$ha = "'" . htmlentities($_POST["radio_ha"]) . "'";

if(strlen(htmlentities($_POST["txt_kit_ha"]))==0){
    $whatHa = "NULL";
}else {
    $whatHa = "'" . substr(htmlentities($_POST["txt_kit_ha"]),-1) . "'";
}

$lab = "'" . substr(htmlentities($_POST["txt_lab"]),-1) . "'";
$srv = "'" . htmlentities($_POST["txt_srv"]) . "'";
$ip = "'" . htmlentities($_POST["txt_ip"]) . "'";

if(strlen(htmlentities($_POST["txt_port_testauto"]))==0){
    $portTestAuto = "NULL";
}else {
    $portTestAuto = "'" . htmlentities($_POST["txt_port_testauto"]) . "'";
}


$instanceDB = "'" . htmlentities($_POST["txt_instance_db"]) . "'";
$schemasDB = "'" . htmlentities($_POST["txt_schemas_db"]) . "'";
$lienIHM = "'" . htmlentities($_POST["url_ihm"]) . "'";

if(strlen(htmlentities($_POST["txt_port_ssh"]))==0){
    $ssh = "NULL";
}else {
    $ssh = "'" . htmlentities($_POST["txt_port_ssh"]) . "'";
}

if(strlen(htmlentities($_POST["txt_port_neops"]))==0){
    $neops = "NULL";
}else {
    $neops = "'" . htmlentities($_POST["txt_port_neops"]) . "'";
}

if(strlen(htmlentities($_POST["txt_port_ethernet"]))==0){
    $ethernet = "NULL";
}else {
    $ethernet = "'" . htmlentities($_POST["txt_port_ethernet"]) . "'";
}

if(strlen(htmlentities($_POST["txt_port_nsquare"]))==0){
    $nsquare = "NULL";
}else {
    $nsquare = "'" . htmlentities($_POST["txt_port_nsquare"]) . "'";
}

if(strlen(htmlentities($_POST["txt_port_simuhost"]))==0){
    $simuhost = "NULL";
}else {
    $simuhost = "'" . htmlentities($_POST["txt_port_simuhost"]) . "'";
}

if(strlen(htmlentities($_POST["txt_port_exterieur"]))==0){
    $portExt = "NULL";
}else {
    $portExt = "'" . htmlentities($_POST["txt_port_exterieur"]) . "'";
}

if(strlen(htmlentities($_POST["txt_disque"]))==0){
    $disque = "NULL";
}else {
    $disque = "'" . htmlentities($_POST["txt_disque"]) . "'";
}

if(strlen(htmlentities($_POST["txt_ram"]))==0){
    $ram = "NULL";
}else {
    $ram = "'" . htmlentities($_POST["txt_ram"]) . "'";
}
$ram = "'" . htmlentities($_POST["txt_ram"]) . "'";

if(strlen(htmlentities($_POST["txt_apl"]))==0){
    $apl = "NULL";
}else {
    $apl = "'" . htmlentities($_POST["txt_apl"]) . "'";
}


if(strlen(htmlentities($_POST["txt_reef"]))==0){
    $reef = "NULL";
}else {
    $reef = "'" . htmlentities($_POST["txt_reef"]) . "'";
}

$publication = "'" . htmlentities($_POST["txt_publication"]) . "'";


$sql = "INSERT INTO `archipel` (`Nom`, `Description`, `UUID`, `Path_Install`, `PCI`, `VersionApl`, `VersionCore`, `Wildfly`, `Java`, `Synchro`, `Nfast`, `Tools`, `GlusterFS`, `Docker`, `OS`, `HA`, `What_HA`, `Lab`, `Srv`, `IP`, `PortTestAuto`, `InstanceDB`, `SchemasDB`, `LienIHM`, `SSH`, `Neops`, `Ethernet`, `Nsquare`, `Simuhost`, `PortExt`, `Disque`, `RAM`, `APL`, `Reef`, `Publication`) VALUES ";
$sql = $sql . "($nom, $description, $uuid, $path_install, $pci, $version_apl, $version_core, $wildfly, $java, $synchro, $nfast, $tools, $glusterfs, $docker, $os, $ha, $whatHa, $lab, $srv, $ip, $portTestAuto, $instanceDB, $schemasDB, $lienIHM, $ssh, $neops, $ethernet, $nsquare, $simuhost, $portExt, $disque, $ram, $apl, $reef, $publication)";


try {
    $pdo->query($sql);


    echo '<body>',
            '<div class="container">',
                '<div class="card text-white bg-success">',
                    "<div class=\"card-header\"><h3>L'ArchiPEL $nom a bien été ajouté à la base de données</h3></div>",
                    "<div class=\"card-body\">Vous allez être redirigé vers la page d'accueil dans <strong id='countdown'>5 secondes</strong></br>Si cela ne fonctionne pas, cliquez <a href='index.php'>ICI</a></div>",
                '</div>',
            '</div>',
            '<script>',
                'let i = 5;',
                'let tmp = setInterval(countdown, 1000);',
                'function countdown() {',
                    'if (i != 0) {',
                        'document.getElementById("countdown").innerHTML = i + " secondes";',
                        'i = i - 1;',
                    '} else {',
                        'window.location.replace("index.php");',
                   '}',
                '}',
            '</script>',
         '</body>';



}catch (Exception $e){
    echo '<body>',
            '<div class="container">',
                '<div class="card text-white bg-danger">',
                    "<div class=\"card-header\"><h3>L'ArchiPEL $nom n'a pas été ajouté à la base de données</h3></div>",
                    "<div class=\"card-body\">IL Y A EU UN SOUCIS DANS LA COMMANDE SQL SUIVANTE : </br><strong>$sql</strong></br>Contactez l'administrateur pour en savoir +</div>",
                '</div>',
            '</div>';


}




