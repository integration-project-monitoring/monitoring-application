<?php


require_once "library_monitoring.php";
require_once "classes/Reef.php";

session_start();

$role = "";
if(isset($_SESSION["username_logged"])){
    $username = $_SESSION["username_logged"];
    $is_connected = 1;
    $role = $_SESSION["role_logged"];
}else{
    $username ="";
    $is_connected = 0;
}

//Connexion à la base de donnees
$pdo = connectToBdd();

$current_reef = get_reef($pdo, $_GET["id"]);

print_head("$current_reef->name_reef HA - EPI", 'monitoring.css');


echo '<body class="hold-transition skin-black sidebar-mini">',
'<div class="wrapper">';

print_header($is_connected,$username);

print_sidebar();

echo '<div class="content-wrapper">',
'<section class="content-header">',
"<h1>Informations Reef $current_reef->name_reef HA</h1>",
'</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
'<div class="box-header with-border">',
"<h3 class='box-title'>Liste de toutes les infos sur Reef $current_reef->name_reef</h3>",
'</div>',
'<div class="box-body table-responsive no-padding">',
'<table class="table table-stripped table-condensed">';

print_thead_reef_ha();

echo '<tbody>';

print_tbody_reef_ha($current_reef,$pdo);

echo '</tbody>';

print_thead_reef_ha_ports();

echo '<tbody>';

print_tbody_reef_ha_ports($current_reef,$pdo);

echo '</tbody>';

print_thead_reef_ha_versions();


echo '<tbody>';

print_tbody_reef_ha_versions($current_reef,$pdo);

echo '</tbody>';

echo '</table>';

echo "<section class=\"content-footer flex_buttons\">",
        "<button class=\"btn btn_monitoring \" onclick=\"location.href='modify_reef_ha.php?id=$current_reef->id';\">Modifer le Reef</button>",
        "<button class=\"btn btn_suppress btn_add\" onclick=\"suppress_reef_ha($current_reef->id,'$current_reef->name_reef');\"> Supprimer le Reef</button>",
    "</section>",
'</div>',
'</div>',
'</section>';

print_comments($pdo, $current_reef->id, "reef_ha",$username,$role);

echo '</div>',
'</div>';





//Finalement, on inclue les scripts
echo '<script src="../js/suppress.js"></script>';
print_scripts();

echo '</body>','</html>';
