<?php 

/**
 * Page qui va construire la requete et indiquer si l'archipel a bien ete ajouté
 * On va dans un premier temps construire la requete, avec le parsage des champs entrés dans la derniere page,
 * puis ensuite on fera un petit affichage indiquant a l'utilisateur si c'est OK ou KO
 */


require_once "library_monitoring.php";

session_start();

if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
    }else{
        header("Location: right_error.php");
        exit;
    }
}else{
    header("Location: login.php?errno=add_vm");
    exit;
    $username ="";
    $is_connected = 0;
}


print_head('Ajout de VM - EPI ', 'monitoring.css');

$pdo = connectToBdd();

 echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

    print_header();

    print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">';


 //On commence par parser la requete 

 //1. on extrait tous les inputs utilisateurs 

 if(strlen($_POST["txt_name_group"])>0){
    $name_group = "'" . addslashes(htmlentities($_POST["txt_name_group"])) . "'";
    $id_group = get_group_id_from_name($pdo, $name_group);
 }else{
     $id_group = "NULL";
 }
 

 $name_lab = "'" . addslashes(htmlentities($_POST["txt_lab"])) . "'";

 $id_lab = (int) filter_var($name_lab,FILTER_SANITIZE_NUMBER_INT);

 if($id_lab==0){
     $id_lab = "NULL";
 }

 $name_vm = "'" . addslashes(htmlentities($_POST["txt_name_vm"])) . "'";

 $ip = "'" . addslashes(htmlentities($_POST["txt_ip_vm"])) . "'";

 $type_vm = "'" . addslashes(htmlentities($_POST["txt_type_vm"])) . "'";

 $port_ssh = "'" . addslashes(htmlentities($_POST["txt_ssh_vm"])) . "'";

 if(strlen($_POST["txt_portauto_vm"]>0)){
    $port_auto = "'" . addslashes(htmlentities($_POST["txt_portauto_vm"])) . "'";    
 }else{
     $port_auto = "NULL";
 }
 
 
 $sql = "INSERT INTO VMS (name_vm, ip, vm_type, id_grp, id_lab, port_ssh, port_test_auto) VALUES ($name_vm, $ip, $type_vm, $id_group, $id_lab, $port_ssh, $port_auto)";

 
 try{
    $pdo->query($sql); 

    
   echo "<h3>La VM <strong>$name_vm</strong> a bien été ajoutée ! </h3>", 
       '<p>Vous allez être redirigé vers la liste des vms dans <strong id="countdown">5 secondes</strong></p><br>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';

   echo '<script>', 
           'let i = 5;', 
           'let tmp = setInterval(countdown, 1000);',
           'function countdown() {',
               'if(i!=0) {',
                   'document.getElementById("countdown").innerHTML = i + " secondes";',
                   'i = i -1; ',
               '}',
               'else {',
                   'window.location.replace("add_vm.php");',
               '}',
           '}',
       '</script>';
   
   print_scripts();

   echo '</body>','</htlm>';
}catch (Exception $e){

   echo "<h3>Il y a eu une erreure technique dans l'ajout de la VM $name_vm</h3>", 
       '<p>La commande est : ',$sql,'<br>Veuillez contacter le développeur en lui copiant collant cette commande pour voir quel est le soucis </p>';

   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';


   print_scripts();

   echo '</body>','</htlm>';
}
