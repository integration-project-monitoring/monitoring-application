<?php 

/**
 * Page qui va vérifier les informations de connexion d'un utilisateur pour lui permettre de se connecter 
 * 
 * Paramètres de la page : 
 *      - FACULTATIF : 
 *          @param GET String $link lien vers lequel on va rediriger après la connexion 
 *          @param GET int $id id vers lequel on va rediriger si le lien nécessite un id (pour modify_apl_ha.php par exemple)
 */


require_once "library_monitoring.php";



session_start();

print_head("Monitoring - EPI", "monitoring.css");

//On construit le lien vers lequel on va rediriger lorsque l'on quittera la page (à la connexion)

//Par défaut on redirige vers new_index
$redirect = "new_index.php";
if(isset($_GET["link"])){
    $redirect = $_GET["link"] . ".php";

    if(isset($_GET["id"])){
        $current_id = $_GET["id"];
        $redirect = $redirect . "?id=$current_id";
    }
}


$pdo = connectToBdd();

echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

    print_header();

    print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">';



//On parse les differents input pour faire une requete sql

$user_name = "'" . addslashes(htmlentities($_POST["txt_username"])) . "'";

$is_passwd_valid = false;

$date = date("Y-m-d"); 

$sql = "SELECT * FROM USERS WHERE username_user=$user_name";

//On essaie de faire la requete 
try{
    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    
    if($pdostat->rowCount()==0){
        //Pas de résultats --> Le nom d'utilisateur n'éxiste pas en base
        header("Location: login.php?errno=1");
        exit;
    }

    //On vérifie le mot de passe car l'username exist
    foreach ($pdostat as $item){

        $is_passwd_valid = password_verify($_POST["txt_mdp"],$item["mdp_user"]);

        if($is_passwd_valid){

            //Le mot de passe correspond, on ajoute aux variables de session le role de l'utilisateur et son nom d'utilisateur
            $_SESSION["username_logged"] = $item["username_user"];
            $_SESSION["role_logged"] = $item["role_user"];
            
            //On redirige maintenant vers $redirect
            header("Location: ".$redirect);
            exit;
        }else{
            
            //Le mot de passe n'est pas bon, on redirige vers la page de login en affichant un message d'erreur
            header("Location: login.php?errno=1");
            exit;
        }
        
   echo '</div>';

   echo '</div>',
   '</section>',
   '</div>',
   '</div>';

   print_scripts();
   echo '</body>','</htlm>';
    }

    //


}
catch (Exception $e){
    //Problème technique, on redirige vers la page de login en affichant un message d'erreur 
    header("Location: login.php?errno=1");
    exit;
}

