<?php


/*
 * OBSOLETE !!!!!!!!!!!!!!!! 
 * CE FICHIER PHP SERT A AFFICHER LA LISTE DES ARCHIPEL
 */


require_once"library_infoserv.php";

//Affichage de l'entete de l'html
print_head('Monitoring EPI', 'perso.css');


//Connexion à la base de donnees
$pdo = connectToBdd();

$cpt = 0;
$arrayApl = getArrayApl($pdo);
echo '<body>',
        '<div class="container">',
            '<header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">',
                '<a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">',
                    '<span class="fs-4">Monitoring Plateforme EPI</span>',
                '</a>',
                '<ul class="nav nav-pills">',
                    '<li class="nav-item" style="font-size: 1.2em!important;"><a href="add_apl.php" class="nav-link active">Ajouter un ArchiPEL</a></li>',
                    '<li class="nav-item" style="font-size: 1.2em!important;"><a href="mn_reef.php" class="nav-link ">Reef</a></li>',
                '</ul>',
            '</header>',
        '</div>',
        '<h1>ArchiPEL</h1>',
        '<div id="html" class="demo">';




//AFFICHAGE DE L'ARBRE :

//On commence par les HA

$cpt = 0;
usort($arrayApl[0],array("Archipel","compare_archipel_by_kitHA"));
$currentHAKIT = '';

foreach ($arrayApl[0] as $apl){
     if($cpt == 0){
         $cpt=1;
         echo '<ul>',
                '<li data-jstree=\'{"opened" : true }\'> HA';
         $currentHAKIT = $apl->whatHa;
     }
         echo '<ul>';


     if($apl->whatHa != $currentHAKIT && $cpt!=0){
         $cpt=1;
         $currentHAKIT = $apl->whatHa;
         echo "</li></ul>";
     }

     if($cpt==1){
         echo "<li data-jstree='{\"opened\" : true }'>$apl->whatHa";
         echo"<ul>";
         $cpt=2;
     }


     echo "<li data-jstree='{\"opened\" : false}'><strong> $apl->nom - $apl->versionApl</strong>";

    if (strlen($apl->reef) > 0){
        echo "  |  $apl->reef  ";
    }

    echo " | <strong>$apl->instanceDB</strong> ('$apl->shemasDB') | $apl->publication ('$apl->uuid') <strong>LAB $apl->lab</strong>";

    echo '<ul>',
            "<li>$apl->description</li>",
            "<li><a href='$apl->lienIHM' class='link'>IHM</a> </li>",
            "<li>$apl->os</li>",
            "<li>$apl->java</li>";

    $split = explode(";",$apl->apl);
    echo "<li>Ports APL",
            "<ul>",
                "<li>$split[0]</li>",
                "<li>$split[1]</li>",
                "<li>$split[2]</li>",
            "</ul>",
        "</li>";

        $split = explode(";",$apl->ip);

        echo "<li>IPs",
                "<ul>",
                    "<li>Tx1 : $split[0]</li>",
                    "<li>Tx2 : $split[1]</li>",
                    "<li>Worker : $split[2]</li>",
                "</ul>",
            "</li>";

    echo "<li>Voir plus ...";

    echo '<ul>';


    echo "<li>Tools",
            "<ul>",
                "<li>Docker : $apl->docker</li>",
                "<li>GlusterFS : $apl->glusterfs</li>",
            "</ul>",
          "</li>";

    echo '<li>Ports ',
            '<ul>',
                "<li>Port tests auto : $apl->portTestAuto</li>";

        $split = explode(";",$apl->ssh);
        echo "<li data-jstree='{\"opened\" : true}'>Ports SSH : ",
                "<ul>",
                    "<li>Tx1 : $split[0]</li>",
                    "<li>Tx2 : $split[1]</li>",
                    "<li>Worker : $split[2]</li>",
                "</ul></li>";


    echo    "<li>Port SimuHost : $apl->simuhost",
    "<li>Port Neops : $apl->neops</li>",
    "<li>Port Ethernet : $apl->ethernet</li>",
    "<li>Port Nsquare : $apl->nsquare</li>",
    "<li>Port Acces extérieur : $apl->portExt</li>";


    echo '</ul></ul></li></ul>';

    echo '</li></ul>';

}


//On continue avec les standalones

$cpt = 0;

foreach ($arrayApl[1] as $apl){
    if($cpt == 0){
        $cpt++;
        echo '</li></ul></li></ul><ul>',
        '<li data-jstree=\'{"opened" : true }\'>Standalone';
    }
    echo '<ul>';
    echo "<li data-jstree='{\"opened\" : false}'><strong> $apl->nom - $apl->versionApl</strong>";

    if (strlen($apl->reef) > 0){
        echo "  |  $apl->reef  ";
    }

    echo " | <strong>$apl->instanceDB</strong> ('$apl->shemasDB') | $apl->publication ('$apl->uuid') <strong>LAB $apl->lab</strong>";

    echo '<ul>',
            "<li>$apl->description</li>",
            "<li><a href='$apl->lienIHM' class='link'>IHM</a> </li>",
            "<li>$apl->os</li>",
            "<li>$apl->java</li>";

    echo "<li>IP : $apl->ip</li>";

    echo "<li>Voir plus ...";

    echo '<ul>';

    echo '<li>Ports ',
    '<ul>',
    "<li>Port tests auto : $apl->portTestAuto</li>";

    echo "<li>Port SSH : $apl->ssh</li>";


    echo    "<li>Port SimuHost : $apl->simuhost",
    "<li>Port Neops : $apl->neops</li>",
    "<li>Port Ethernet : $apl->ethernet</li>",
    "<li>Port Nsquare : $apl->nsquare</li>",
    "<li>Port Acces extérieur : $apl->portExt</li>";


    echo '</ul></li></ul></li></ul>';

    echo '</li></ul>';

}


//On continue avec les certifs

$cpt = 0;

foreach ($arrayApl[2] as $apl){
    if($cpt == 0){
        $cpt++;
        echo '</li></ul><ul>',
        '<li data-jstree=\'{"opened" : true }\'>CERTIFS';
    }
    echo '<ul>';
    echo "<li data-jstree='{\"opened\" : false}'><strong> $apl->nom - $apl->versionApl</strong>";

    if (strlen($apl->reef) > 0){
        echo "  |  $apl->reef  ";
    }

    echo " | <strong>$apl->instanceDB</strong> ('$apl->shemasDB') | $apl->publication ('$apl->uuid') <strong>LAB $apl->lab</strong>";

    echo '<ul>',
    "<li>$apl->description</li>",
    "<li><a href='$apl->lienIHM' class='link'>IHM</a> </li>",
    "<li>$apl->os</li>",
    "<li>$apl->java</li>";

    echo "<li>IP : $apl->ip</li>";

    echo "<li>Voir plus ...";

    echo '<ul>';

    echo '<li>Ports ',
    '<ul>',
    "<li>Port tests auto : $apl->portTestAuto</li>";

    echo "<li>Port SSH : $apl->ssh</li>";


    echo    "<li>Port SimuHost : $apl->simuhost",
    "<li>Port Neops : $apl->neops</li>",
    "<li>Port Ethernet : $apl->ethernet</li>",
    "<li>Port Nsquare : $apl->nsquare</li>",
    "<li>Port Acces extérieur : $apl->portExt</li>";


    echo '</ul></li></ul></li></ul>';

    echo '</li></ul>';

}

echo '</li></ul></div>',
        '<script src="../jstree/dist/jstree.js"></script>',
            '<script>$(document).ready(function () {',
            '$("#html").jstree({"plugins" : ["themes","html_data","ui"] });',
            '$("#html li").on("click", "a.link",',
                'function() {',
                    'window.open(this,\'_blank\');',
                '}',
            ');',
        '});</script>';


echo '</body></html>';

