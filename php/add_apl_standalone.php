<?php 
/**
 * Page qui va afficher un formulaire pour ajouter un ArchiPEL standalone 
 * Champs nécessaires : 
 *      - Nom de la VM associée à l'ArchiPEL standalone
 *      - Nom de l'ArchiPEL
 *      - Path d'installation d'archiPEL (la plupart du temps en standalone ça sera /opt/ArchiPEL)
 *      - HSM auquel l'ArchiPEL est lié
 *      - Lien vers l'IHM de l'ArchiPEL standalone
 */

//On inclut la librairie de fonctions 
require_once "library_monitoring.php";

//On initialise la session
session_start();

//Vérification du rôle de l'utilisateur connecté 
// Roles acceptés : "integrator" ; "administrator"
//Si l'utilisateur est bien connecté, et a les bons droits, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
    }else{
        //L'utilisateur n'a pas les bons droits
        header("Location: right_error.php");
        exit;
    }
}else{
    //L'utilisateur n'est pas connecté
    header("Location: login.php?errno=add_apl_standalone");
    exit;
}

//Affichage de l'entete en html 
print_head('Ajout ArchiPEL STANDALONE - EPI','monitoring.css');


//Connexion a la base de donnee 
$pdo = connectToBdd();

//Affichage du conteneur
echo '<body class="hold-transition skin-black sidebar-mini">',
        '<div class="wrapper">';

//Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
print_header($is_connected,$username);

//Affichage de la sidebar
print_sidebar();


echo '<div class="content-wrapper">',
        '<section class="content-header">',
        '</section>';

echo '<section class="content container-fluid">';

echo '<div class="box box-warning">',
        '<div class="box-header with-border">',
            '<h3>Ajout d\'un ArchiPEL Standalone :</h3>';

//DEBUT DU FORMULAIRE 
echo '<form action="apl_standalone_added_new.php" method="post">';

//Champs txt_name_vm --> nom de la vm associée à l'ArchiPEL Standalone
echo '<label class="label_form required">Sur quelle VM est installé ArchiPEL ? </label> <input type="text" name="txt_name_vm" id="txt_name_vm" list="list_name_vm">';
            
//On va récupérer dans un tableau toutes les vm 
$array_vm = get_array_vm($pdo);

//On va créer la datalist dans laquelle on a tous les noms de vm d'inscrits
echo '<datalist id=list_name_vm>';

foreach ($array_vm as $item){
    echo '<option>' , $item->name_vm , '</option>';
}

echo '</datalist><span style="margin-left:10px">Vous ne trouvez pas votre VM ?<span><button style="margin-left:10px" class = "btn btn_monitoring" onclick="location.href=\'add_vm.php\';">L\'ajouter</button><br>';

// Champs txt_name_apl --> nom de l'ArchiPEL Standalone
echo '<label class="label_form required">Quel est le nom de l\'archipel ?</label>',
        '<input required type="text" name="txt_name_apl" id="txt_name_apl"><br>';

//Champs txt_installation_path_apl --> Path absolu vers l'installation d'ArchiPEL Standalone (La plupart du temps /opt/ArchiPEL)
echo '<label class="label_form required">Quel est le path d\'installation d\'archipel ?</label>',
    '<input required type="text" name="txt_installation_path_apl" id="txt_installation_path_apl" value="/opt/ArchiPEL"><br>';

//Champs txt_hsm_apl --> Label de l'HSM vers lequel est connecté l'ArchiPEL standalone
echo '<label class="label_form required">Sur quel HSM est-il branché ?</label>',
    '<input required type="text" name="txt_hsm_apl" id="txt_hsm_apl" list="list_hsm"><br>';

//Champs txt_link_ihm_apl --> Lien vers l'IHM d'archiPEL standalone 
echo '<label class="label_form required">Quel est le lien vers l\'IHM ?</label>',
    '<input required type="text" name="txt_link_ihm_apl" id="txt_link_ihm_apl"><br>';

//On récupère tous les HSM dans un tableau
$array_hsm = get_array_hsm($pdo);

//On va créer la datalist dans laquelle on a tous les label d'HSM d'inscrits
echo '<datalist id=list_hsm>';

foreach ($array_hsm as $item){
    echo '<option>', $item->label_hsm, '</option>';
}

echo '</datalist>';


echo '<button class="btn btn_monitoring"> Ajouter l\'ArchiPEL</button>';
            
echo '</div>';

echo '</div>',
'</section>',
'</div>',
'</div>';

//On inclue les scripts JS (mis à la fin pour l'optimisation)
print_scripts();

echo '</body>','</html>';