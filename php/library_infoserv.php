<?php

require_once "Archipel.php";
require_once "Reef.php";

/**
 * BIBLIOTHEQUE DE FONCTIONS POUR LE MONITORING D'INFOS SERVEURS
 */


/**
 * Fonction qui va retourner vers la sortie standard le code de l'entete HTML
 * @param string $titre Le titre de la page
 * @param string $css Le nom du css que l'on va utiliser
 */
function print_head($titre = '', $css = '')
{
    $titre = htmlentities($titre, ENT_COMPAT, 'ISO-8859-1');

    echo '<!DOCTYPE html>',
    '<html lang=fr>',
    '<head>',
    '<meta charset="UTF-8">',
    '<title>', $titre, '</title>',
    '<meta name="viewport" content="width=device-width, initial-scale=1">',
        //JS
    '<script src="../js/jquery-3.6.0.min.js"></script>',
    '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>',
    '<script src="../js/bootstrap.js"></script>',
        //CSS
    '<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />',
    '<link href="../jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css"/>',
        //NOTRE CSS
    "<link href='../css/$css' rel='stylesheet' type='text/css'>",
    '</head>';
}

/**
 * Fonction qui se connecte avec la base de données
 * @return PDO L'objet PDO representant la connexion avec la BD
 */
function connectToBdd()
{
    $dsn = "mysql:dbname=info_vms;host=172.16.105.52";
    $user = "vis";
    $pass = "ArchiPEL$1";
    $pdo = new PDO($dsn, $user, $pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
}

/**
 * Fonction qui renvoie un tableau constitué de 3 tableaux, un tableau avec tous les archiPEL HA, un tableau avec tous les archipel Standalone et un avec les archipel Certif
 * @param PDO $pdo
 * @return array Le tableau contenant les tableaux d'archipel (array[0] --> HA ; array[1] --> Standalone ; array[2] --> Certif)
 */
function getArrayApl(PDO $pdo)
{
    $sql = "SELECT * FROM archipel"; //ORDER BY archipel.What_HA DESC";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);

    $array = array();
    array_push($array, array());
    array_push($array, array());
    array_push($array, array());

    foreach ($pdostat as $item) {
        $archipel = new Archipel($item["ID"], $item["Nom"], $item["Description"], $item["UUID"], $item["Path_Install"], $item["PCI"], $item["VersionApl"], $item["VersionCore"], $item["Wildfly"], $item["Java"], $item["Synchro"], $item["Nfast"], $item["Tools"], $item["GlusterFS"], $item["Docker"], $item["OS"], $item["HA"], $item["What_HA"], $item["Lab"], $item["Srv"], $item["IP"], $item["PortTestAuto"], $item["InstanceDB"], $item["SchemasDB"], $item["LienIHM"], $item["SSH"], $item["Neops"], $item["Ethernet"], $item["Nsquare"], $item["Simuhost"], $item["PortExt"], $item["Disque"], $item["RAM"], $item["APL"], $item["Reef"], $item["Publication"], $item["Certif"]);
        if ($item["Certif"] == 1) {
            array_push($array[2],$archipel);
        }else {
            if($item["HA"] == 1){
                array_push($array[0],$archipel);
            }else {
                array_push($array[1],$archipel);
            }
        }
    }

    return $array;
}


/**
 * Fonction qui retourne un tableau avec tous les reef inscrits en base
 * @param PDO $pdo objet de connection a la base
 * @return array Le tableau avec tous les reefs
 */

function getArrayReef(PDO $pdo){
    $sql = "SELECT * FROM reef ORDER BY What_HA ASC";

    $pdostat = $pdo->query($sql);
    $pdostat->setFetchMode(PDO::FETCH_ASSOC);
    $array = array();
    $cpt = 0;
    foreach ($pdostat as $item){
        $cpt++;
        $reef = new Reef($item["ID"], $item["Nom"],$item["Version"], $item["PortExt"], $item["PortSimuhost"], $item["PortNeops"], $item["PortEthernetNeops"], $item["PortNsquare"], $item["Contexte"], $item["PortTestAuto"], $item["What_HA"]);
        array_push($array,$reef);

    }

    return $array;
}