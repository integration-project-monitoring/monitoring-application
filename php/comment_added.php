<?php 

/**
 * Page qui va construire une requete pour ajouter un commentaire, s'il a bien ete ajouté, on redirige vers la page où l'on a ajouté le comment
 * On va dans un premier temps construire la requete, avec le parsage des champs entrés dans la derniere page,
 * puis ensuite on redirigera si c'est OK, sinon on affiche un message d'erreur
 * 
 * Paramètres de la page : 
 *      - OBLIGATOIRE : 
 *          @param GET int $id_redirect l'id de l'objet auquel on veut ajouter le commentaire 
 *          @param GET string $type le type de l'objet auquel on veut ajouter le commentaire
 */

 //On inclut la librairie de fonctions 
 require_once "library_monitoring.php";

 //On initialise la session
 session_start();

 //Vérification du rôle de l'utilisateur connecté 
// Roles acceptés : "integrator" ; "administrator"
//Si l'utilisateur est bien connecté, et a les bons droits, on met $is_connected à 1 et $role prend la valeur du rôle de l'utilisateur
if(isset($_SESSION["username_logged"]) && isset($_SESSION["role_logged"])){
    if($_SESSION["role_logged"]=="integrator" || $_SESSION["role_logged"]=="administrator"){
        $username = $_SESSION["username_logged"];
        $is_connected = 1;
        $role = $_SESSION["role_logged"];
    }else{
        //L'utilisateur n'a pas les bons droits
        header("Location: right_error.php");
        exit;
    }
}else{
    //L'utilisateur n'est pas connecté
    header("Location: login.php");
    exit;
}

 //Affichage de l'entete en html 
 print_head('Ajout de commentaire - EPI ', 'monitoring.css');

 //Connexion a la base de donnee 
 $pdo = connectToBdd();

 //On vérifie que les paramètres sont bien présents dans la requête
 if(isset($_GET["id"]) && isset($_GET["type"])){
    $id_redirect = $_GET["id"];
    $type = $_GET["type"];   
 }else{
     //Les paramètres ne sont pas présents, on redirige vers l'accueil
     header("Location: new_index.php");
     exit;
 }



 //On commence par parser la requete 

 //1. on extrait tous les inputs utilisateurs 
 //On applique htmlentities() et addslashes() pour éviter les injections 

 $name_author = "'" . $username . "'";

 $txt_comment = "'" . addslashes(htmlentities($_POST["txt_comment"])) . "'";

 $sql = "INSERT INTO COMMENTS (txt_comment, name_author, id_associated, type_associated, time_comment) VALUES ($txt_comment, $name_author, $id_redirect, '$type', NOW())";


 //2. On éxecute la requête, s'il y a un problème on rentrera dans le catch, ou on affichera un message d'erreur
 // Le message d'erreur contient la requête SQL, pour pouvoir débugger aisèment
 try{
     $pdo->query($sql);
     
     //La redirection après l'éxecution de la requete
     if($type == "archipel_ha"){
        header("Location: apl_ha_details.php?id=$id_redirect");
        exit;
     }else{
         if($type == "archipel_standalone"){
             header("Location: apl_standalone_details.php?id=$id_redirect");
             exit;
         }else{
             if($type == "reef_ha"){
                 header("Location: reef_ha_details.php?id=$id_redirect");
                 exit;
             }else{
                 if($type == "vm"){
                     header("Location: vms_details.php?id=$id_redirect");
                     exit;
                 }
             }
         }
     }
     
 }catch (Exception $e){
     //Si on rentre ici c'est qu'il y a un problème dans la requête
    //Soit une erreur de saisie, un champs trop long etc... 
    //Astuce : copier coller la requête SQL dans phpmyadmin, l'erreur sera plus explicite ! 

    
    //Affichage du conteneur
    echo '<body class="hold-transition skin-black sidebar-mini">',
    '<div class="wrapper">';

    //Affichage de l'entête (avec le bouton qui replie la sidebar et le bouton "mon compte")
    print_header($is_connected,$username);

    //Affichage de la sidebar
    print_sidebar();

    echo '<div class="content-wrapper">',
            '<section class="content-header">',
            '</section>';

    echo '<section class="content container-fluid">';

    echo '<div class="box box-warning">',
            '<div class="box-header with-border">';

    echo "<h3>Le Commentaire n'a pas bien été ajouté ! </h3>", 
        '<p>La commande est : ',$sql,'<br>Veuillez contacter le développeur en lui copiant collant cette commande pour voir quel est le soucis </p>';

    echo '</div>';

    echo '</div>',
    '</section>',
    '</div>',
    '</div>';

    //On inclue les scripts JS (mis à la fin pour l'optimisation)
    print_scripts();

    echo '</body>','</htlm>';
 }