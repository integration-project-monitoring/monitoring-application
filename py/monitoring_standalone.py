import mysql.connector
import os
import sys
import configparser

#####################################################
#Script python de monitoring de DB                  #
#En fonction de l'ID de l'ArchiPEL en base, va      #
# récupérer toutes les infos pour les entrer dans   #
# la base de donnée SQL                             #
#                                                   #
#                                                   #
#ArchiPEL STANDALONE :                              #
#                                                   #
#####################################################

config = configparser.RawConfigParser()
config.read('config_monitoring.properties')

name_db = config.get('DB','name_db')
user_db = config.get('DB','user_db')
pass_db = config.get('DB','pass_db')
host_db = config.get('DB','host_db')

id_archipel = config.get('Monitoring','id_apl')
id_vm = config.get('Monitoring','id_vm')

is_monitored = 0

array_tmp = os.popen("sudo crontab -l").read().splitlines()

for line in array_tmp :
    if len(line)==0 : 
        continue 
    if line[0] == '#' :
        continue
    else :
        if "cron_monitor.sh" in line : 
            is_monitored = 1


stream = os.popen('sudo cat /opt/ArchiPEL/etc/version')

output = stream.readlines()

#for line in output :
#    x = line.split('=')
#    sys.stdout.write("line 1 : " + x[1])

version_core = output[0].split('=')[1].replace("\n","")
version_apl = output[1].split('=')[1].replace("\n","")

sys.stdout.write("version de core : " + version_core)
sys.stdout.write("version de apl : " + version_apl)


stream = os.popen('cat /etc/issue')

output = stream.readlines()


tmp =  output[0].split(" ")

version_ubuntu = tmp[0]+ " " + tmp[1] + " " + tmp[2]


sys.stdout.write("version d'ubuntu : " + version_ubuntu + "\n")


stream = os.popen("sudo grep -i 'db.tx.user' /opt/ArchiPEL/etc/archipel.conf")
output = stream.readlines()

tmp = output[0].split('=')

oracle_scheme = tmp[1][:-4]

sys.stdout.write("oracle scheme : " + oracle_scheme +"\n")

stream = os.popen("sudo grep -i 'db.sid' /opt/ArchiPEL/etc/archipel.conf")
output = stream.readlines()

instance_db = output[0].split('=')[1].replace("\n","")

sys.stdout.write("instance d'oracle : " + instance_db + "\n")

stream = os.popen("sudo grep -i 'server_name_uuid' /opt/ArchiPEL/share/ArchipelPublicationResynchronizer/etc/publication.conf")
output = stream.readlines()

pub_uuid = ""
try:
    pub_uuid = output[0].split('=')[1].replace("\n","")
except IndexError as err: 
    pub_uuid=""

sys.stdout.write("UUID publication : " + pub_uuid + "\n")


java_version = os.popen("java -version 2>&1").read()

java_version = java_version.splitlines()[0]

sys.stdout.write("JAVA : " + java_version.splitlines()[0] + "\n")


try:
    version_nfast = os.popen("/opt/nfast/bin/enquiry | grep -m 1 version").read()

    tmp = version_nfast.split()

    version_nfast = tmp[1]
except IndexError as err:
    version_nfast = ""

sys.stdout.write("NFAST : " + version_nfast + "\n")


wildfly_version = os.popen("ls -l /opt/wildfly").read()

tmp = wildfly_version.split('>')

wildfly_version = tmp[1]
tmp = wildfly_version.split('/')

wildfly_version = tmp[2]
tmp = wildfly_version.split('-')

wildfly_version = tmp[1]



sys.stdout.write("WILDFLY : " + wildfly_version + "\n")


version_synchro = os.popen("sudo cat /opt/ArchiPEL/share/ArchipelPublicationResynchronizer/etc/version").read()


try:
    tmp = version_synchro.split('=')
    version_synchro = tmp[1].replace("\n","")
except IndexError as err: 
    version_synchro=""



sys.stdout.write("SYNCHRO : " + version_synchro + "\n")

ram = os.popen("grep MemTotal /proc/meminfo").read()
tmp = ram.split()


ram ="%.3f" % (int(tmp[1])/1000000) + "G"

sys.stdout.write("Memoire ram total : " + ram + " (avant opération :"+tmp[1]+ ")\n")

disk_lines_array = os.popen("df -h | grep '/var\|/opt\|sda1\|/data'").read().splitlines()
total_disk = ""
disk_free = ""
for line in disk_lines_array : 
    tmp = line.split()
    name_dossier = tmp[5]
    total_disk = total_disk + name_dossier + " : " + tmp[1] + ";"
    disk_free = disk_free + name_dossier + " : " + tmp[3] + ";" 


sys.stdout.write("Total disk : " + total_disk + "\n")
sys.stdout.write("Free disk : " + disk_free + "\n")



##Maintenant qu'on a toutes les données, on va aller les mettre en base !



connexion = mysql.connector.connect(host=host_db,user=user_db,password=pass_db,database=name_db,auth_plugin ="mysql_native_password")
cursor = connexion.cursor()

#On rempli en premier les info d'ArchiPEL


sql = "UPDATE ARCHIPEL SET schemas_db='" + oracle_scheme + "',instance_db='" + instance_db + "',uuid_publication = '" + pub_uuid + "',version = '" + version_apl + "', version_core = '" + version_core + "', version_java = '" + java_version + "', version_wildfly = '" + wildfly_version + "', version_nfast = '" +version_nfast + "', version_ubuntu_apl = '" + version_ubuntu+ "', version_synchro = '" +version_synchro + "', is_monitored="+str(is_monitored)+" WHERE id_apl="+str(id_archipel)


sys.stdout.write(sql)
cursor.execute(sql)

connexion.commit()


sql = "UPDATE VMS SET total_disk = '" + total_disk + "', free_disk = '" + disk_free + "', total_ram = '" + ram + "', is_monitored="+str(is_monitored)+" WHERE id_vm = " + str(id_vm)
cursor.execute(sql)

connexion.commit()

connexion.close()
