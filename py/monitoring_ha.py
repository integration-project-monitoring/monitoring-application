import mysql.connector
import os
import sys
import configparser


#####################################################
#Script python de monitoring de DB                  #
#En fonction de l'ID de l'ArchiPEL en base, va      #
# récupérer toutes les infos pour les entrer dans   #
# la base de donnée SQL                             #
#                                                   #
#                                                   #
#ArchiPEL HA :                                      #
#                                                   #
#####################################################


config = configparser.RawConfigParser()
config.read('config_monitoring.properties')

name_db = config.get('DB','name_db')
user_db = config.get('DB','user_db')
pass_db = config.get('DB','pass_db')
host_db = config.get('DB','host_db')

is_main_ha_vm = config.get('Monitoring','is_main_ha_vm')

id_vm = config.get('Monitoring','id_vm')


connexion = mysql.connector.connect(host=host_db,user=user_db,password=pass_db,database=name_db,auth_plugin ="mysql_native_password")
cursor = connexion.cursor()

is_monitored = 0

array_tmp = os.popen("sudo crontab -l").read().splitlines()

for line in array_tmp : 
    if len(line)==0 :
        continue
    if line[0] == '#' :
        continue
    else :
        if "cron_monitor_ha.sh" in line : 
            is_monitored = 1


if(int(is_main_ha_vm)==1) :
    sys.stdout.write("La VM est la VM principale (is_main_ha_vm == 1)")

    docker_ps = os.popen("sudo docker ps").read()
    
    array_docker_ps = docker_ps.splitlines()
    
    array_docker_ps = array_docker_ps[1:]
    array_container_id = []
    array_container_name = []
    for line in array_docker_ps :
        tmp = line.split()
        #sys.stdout.write("\n" + str(tmp) + "\n")
        #sys.stdout.write("\n" + str(tmp[-1]) + "\n")
        #name_container = tmp[1].split('/')[1].split(':')[0]
        #name_container = tmp[-1].split('_')[0]
        name_container = ""
        
        for k in range(len(tmp[ -1].split('_'))) :
            foo_str = tmp[-1].split("_")[k].split(".")[0]
            if foo_str == "tx" or foo_str=="archipel"  : 
                is_apl = 1
            else : 
                #sys.stdout.write("\n in loop : " + tmp[-1].split('_')[k] + "\n")

                # a supprimer ???? 
                if(len(name_container)==0): 
                    name_container = tmp[-1].split('_')[k]
                else:
                    name_container = name_container +"_"+ tmp[-1].split('_')[k] 
                is_apl = 0

        #sys.stdout.write("\n" + name_container + "\n")
        if is_apl  :
            array_container_id.append(tmp[0])
            array_container_name.append(name_container)
        
    for i in range(len(array_container_id)) :
        sys.stdout.flush()
        sys.stdout.write("Traitement de l'Archipel n"+str(i)+" : "+array_container_name[i]+", id : " + array_container_id[i]+" \n")
        #On doit récuperer l'id de l'archipel pour chaque ligne, ensuite on fait des commandes et on peut actualiser les champs en base
        
        #sql = "SELECT id_apl_associated,installation_path FROM GRP_APL_HA INNER JOIN ARCHIPEL ON ARCHIPEL.id_apl = GRP_APL_HA.id_apl_associated WHERE name_service='"+ array_container_name[i] +"'"
        sql = "SELECT id_apl,installation_path FROM ARCHIPEL WHERE name_stack='" + array_container_name[i] + "' AND id_vm_associated= " + str(id_vm)
        sys.stdout.write("La requete SQL est : "+sql+" \n")
    
        cursor.execute(sql)
        result = cursor.fetchone()
        try:
            sys.stdout.write("Le result est : "+str(result[0])+" \n")
            sys.stdout.write("Le path est : "+str(result[1])+" \n")
            id_apl = result[0]
            installation_path = result[1]
        except:
            sys.stdout.write("Archipel "+str(array_container_name[i])+" introuvable \n")
            continue
        #On va faire les differentes commandes sur les differents archipel
    
        stream = os.popen('sudo cat '+ installation_path +'/etc/version')
        output = stream.readlines()
    
        version_core = output[0].split('=')[1].replace("\n","")
        version_apl = output[1].split('=')[1].replace("\n","")
        sys.stdout.write("version de core : " + version_core+" \n")
        sys.stdout.write("version de apl : " + version_apl+" \n")
    
        stream = os.popen('sudo docker exec -it '+ array_container_id[i] +' cat /etc/issue')
    
        output = stream.readlines()
        tmp =  output[0].split(" ")
    
        version_ubuntu = tmp[0]+ " " + tmp[1] + " " + tmp[2]
        sys.stdout.write("version d'ubuntu : " + version_ubuntu + " \n")
    
        stream = os.popen("sudo grep -i 'db.tx.user' "+ installation_path +"/etc/archipel.conf")
        output = stream.readlines()
    
        tmp = output[0].split('=')
    
        oracle_scheme = tmp[1][:-4]
        sys.stdout.write("oracle scheme : " + oracle_scheme +" \n")
    
        stream = os.popen("sudo grep -i 'db.sid' "+ installation_path +"/etc/archipel.conf")
        output = stream.readlines()
        
        instance_db = output[0].split('=')[1].replace("\n","")
        
        sys.stdout.write("instance d'oracle : " + instance_db + " \n")
    
    
        stream = os.popen("sudo grep -i 'server_name_uuid' "+ installation_path +"/share/ArchipelPublicationResynchronizer/etc/publication.conf")
        output = stream.readlines()
        
        pub_uuid = ""
        try:
            pub_uuid = output[0].split('=')[1].replace("\n","")
        except IndexError as err: 
            pub_uuid=""
        
        sys.stdout.write("UUID publication : " + pub_uuid + " \n")
    
        java_version = os.popen("sudo docker exec "+ array_container_id[i] + " java -version 2>&1").read()
    
        java_version = java_version.splitlines()[0]
    
        sys.stdout.write("JAVA : " + java_version.splitlines()[0] + " \n")
    
        output = os.popen("sudo docker exec "+ array_container_id[i] + " /opt/nfast/bin/enquiry  | grep -m 1 version")
        tmp1 = output.readlines()
        tmp = tmp1[0].split()
        
        version_nfast = tmp[1]
    
        sys.stdout.write("NFAST : " + version_nfast + " \n")
    
        wildfly_version = os.popen("sudo docker exec "+ array_container_id[i] +" ls -l /opt/wildfly").read()
    
        tmp = wildfly_version.split('>')
        
        wildfly_version = tmp[1]
        tmp = wildfly_version.split('/')
        
        wildfly_version = tmp[2]
        tmp = wildfly_version.split('-')
        
        wildfly_version = tmp[1]
        
        
        
        sys.stdout.write("WILDFLY : " + wildfly_version + "\n")
    
    
        version_synchro = os.popen("sudo cat "+ installation_path +"/share/ArchipelPublicationResynchronizer/etc/version").read()
    
    
        try:
            tmp = version_synchro.split('=')
            version_synchro = tmp[1].replace("\n","")
        except IndexError as err: 
            version_synchro=""
        
        
        
        sys.stdout.write("SYNCHRO : " + version_synchro + "\n")
        sql = "UPDATE ARCHIPEL SET schemas_db='" + oracle_scheme + "',instance_db='" + instance_db + "',uuid_publication = '" + pub_uuid + "',version = '" + version_apl + "', version_core = '" + version_core + "', version_java = '" + java_version + "', version_wildfly = '" + wildfly_version + "', version_nfast = '" +version_nfast + "', version_ubuntu_apl = '" + version_ubuntu+ "', version_synchro = '" +version_synchro + "', is_monitored="+str(is_monitored)+" WHERE id_apl="+str(id_apl)


        sys.stdout.write(sql)
        cursor.reset()
        cursor.execute(sql)

        connexion.commit()
else : 
    sys.stdout.write("La VM n'est pas la VM principale (is_main_ha_vm == 0)")


ram = os.popen("grep MemTotal /proc/meminfo").read()
tmp = ram.split()


ram ="%.3f" % (int(tmp[1])/1000000) + "G"

sys.stdout.write("\nMemoire ram total : " + ram + " (avant opération :"+tmp[1]+ ")\n")


stream = os.popen('cat /etc/issue')

output = stream.readlines()


tmp =  output[0].split(" ")

version_ubuntu = tmp[0]+ " " + tmp[1] + " " + tmp[2]


sys.stdout.write("version d'ubuntu : " + version_ubuntu + "\n")

disk_lines_array = os.popen("df -h | grep '/dev/sda1\|/dev/sdb1'").read().splitlines()
total_disk = ""
disk_free = ""
for line in disk_lines_array : 
    tmp = line.split()
    name_dossier = tmp[5]
    total_disk = total_disk + name_dossier + " : " + tmp[1] + ";"
    disk_free = disk_free + name_dossier + " : " + tmp[3] + ";" 


sys.stdout.write("Total disk : " + total_disk + "\n")
sys.stdout.write("Free disk : " + disk_free + "\n")

stream = os.popen("sudo docker --version")
version_docker = stream.readlines()[0].split(" ")[2].replace(",","")

sys.stdout.write("Version de docker : " + version_docker + "\n\n")

stream = os.popen("sudo glusterfs --version") 
array_tmp = stream.readlines()

if(len(array_tmp)>1) : 
    version_gluster = "'" + array_tmp[0].split(" ")[1].replace("\n","") + "'"
    sys.stdout.write("Version de glusterfs : " + version_gluster + "\n\n")
else:
    sys.stdout.write("Standalone dockerise, pas de version de glusterfs \n\n")
    version_gluster = "NULL"

sql = "UPDATE VMS SET total_disk = '" + total_disk + "', free_disk = '" + disk_free + "', total_ram = '" + ram + "', version_ubuntu = '"+ version_ubuntu +"', version_docker = '" + version_docker + "', version_glusterfs=" + version_gluster + ", is_monitored="+str(is_monitored)+" WHERE id_vm = " + str(id_vm)

sys.stdout.write("\n SQL DE VM : " + sql + "\n\n")
cursor.execute(sql)

connexion.commit()

connexion.close()











##HOW TO GET IN YAML STUFFS : 

#port_hsm = os.popen("grep -m 1 'HSM_PORT' /opt/docker-compose/eisop-docker-compose-deploy.yml").read().split('=')[1]
#sys.stdout.write("port_hsm : " + port_hsm + "\n")
#ip_hsm = os.popen("grep -m 1 'HSM_HOST' /opt/docker-compose/eisop-docker-compose-deploy.yml").read().split('=')[1]
#sys.stdout.write("ip_hsm : " + ip_hsm + "\n")
#port_rfs = os.popen("grep -m 1 'RFS_PORT' /opt/docker-compose/eisop-docker-compose-deploy.yml").read().split('=')[1]
#sys.stdout.write("port_rfs : " + port_rfs + "\n")
#ip_rfs = os.popen("grep -m 1 'RFS_HOST' /opt/docker-compose/eisop-docker-compose-deploy.yml").read().split('=')[1]
#sys.stdout.write("ip_rfs : " + ip_rfs + "\n")