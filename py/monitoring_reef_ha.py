import mysql.connector 
import os
import sys 
import configparser 


#####################################################
#Script python de monitoring de DB                  #
#En fonction de l'ID du reef en base, va            #
# récupérer toutes les infos pour les entrer dans   #
# la base de donnée SQL                             #
#                                                   #
#                                                   #
#REEF HA     :                                      #
#                                                   #
#####################################################


config = configparser.RawConfigParser() 
config.read('config_monitoring.properties')

name_db = config.get('DB','name_db')
user_db = config.get('DB','user_db')
pass_db = config.get('DB','pass_db')
host_db = config.get('DB','host_db')

is_main_ha_vm = config.get('Monitoring','is_main_ha_vm')

id_vm = config.get('Monitoring', 'id_vm')

connexion = mysql.connector.connect(host=host_db,user=user_db,password=pass_db,database=name_db,auth_plugin ="mysql_native_password")
cursor = connexion.cursor()


is_monitored = 0

array_tmp = os.popen("sudo crontab -l").read().splitlines()

for line in array_tmp : 
    if line[0] == '#' :
        continue
    else :
        if "cron_monitor_reef_ha.sh" in line : 
            is_monitored = 1

if(int(is_main_ha_vm)==1) : 
    sys.stdout.write("La VM est la VM principale (is_main_ha_vm == 1)")

    docker_ps = os.popen("sudo docker ps").read()

    array_docker_ps = docker_ps.splitlines()

    array_docker_ps = array_docker_ps[1:]
    
    array_container_id = []
    array_container_name = []

    for line in array_docker_ps : 
        tmp = line.split()
        name_container ="" 
        
        for k in range(len(tmp[-1].split('_'))) : 
            foo_str = tmp[-1].split("_")[k].split(".")[0]
            if foo_str == "reef" : 
                is_reef = 1
            else: 
                if(len(name_container)==0):
                    name_container = tmp[-1].split('_')[k]
                else:
                    name_container = name_container + "_" + tmp[-1].split('_')[k]
                is_reef = 0
        
        if is_reef : 
            if not name_container in array_container_name : 
                array_container_id.append(tmp[0])
                array_container_name.append(name_container)

    for i in range(len(array_container_id)) : 
        sys.stdout.flush()
        sys.stdout.write("Traitement du Reef n"+str(i)+" : " + array_container_name[i] + ", id : " + array_container_id[i] + " \n \n")
        
        #On doit récuperer l'id de l'archipel pour chaque ligne, ensuite on fait des commandes et on peut actualiser les champs en base 

        sql = "SELECT id_reef,installation_path FROM REEF WHERE name_stack='" + array_container_name[i] + "'"
        sys.stdout.write("La requete SQL est : " + sql + " \n \n")

        cursor.execute(sql)
        result = cursor.fetchone()
        
        try: 
            sys.stdout.write("Le resultat est : " + str(result[0])+ " \n")
            sys.stdout.write("Le path d'installation est : " + str(result[1])+ " \n")
            id_reef = result[0]
            installation_path = result[1]
        except:
            sys.stdout.write("Reef " + str(array_container_name[i]) + " introuvable  \n \n" )
            continue 
        #On va faire les differentes commandes sur les differents reef (qui sont en base)

        stream = os.popen('sudo cat ' + installation_path + '/etc/version')
        output = stream.readlines() 

        version_reef = output[0].split('=')[1].replace("\n","")
        sys.stdout.write("Version de Reef : " + version_reef + " \n")

        stream = os.popen('sudo docker exec -it ' + array_container_id[i] + ' cat /etc/issue')

        output = stream.readlines()
        tmp = output[0].split(" ")
        version_ubuntu = tmp[0] + " " + tmp[1] + " " + tmp[2]
        sys.stdout.write("version d'ubuntu : " + version_ubuntu + " \n")

        java_version = os.popen("sudo docker exec " + array_container_id[i] + " java -version 2>&1").read()
        java_version = java_version.splitlines()[0]
        sys.stdout.write("JAVA VERSION : " + java_version + " \n")

        output = os.popen("sudo docker exec " + array_container_id[i] + " /opt/nfast/bin/enquiry | grep -m 1 version")
        tmp1 = output.readlines()
        tmp = tmp1[0].split()
        version_nfast = tmp[1]
        sys.stdout.write("NFAST : " + version_nfast + " \n")

        sql = "UPDATE REEF SET version_reef='" + version_reef + "', version_java='" + java_version + "', version_nfast='" + version_nfast + "', version_ubuntu_reef='" + version_ubuntu + "', is_monitored=" + str(is_monitored) + " WHERE id_reef=" + str(id_reef)
        


        sys.stdout.write(sql)
        cursor.reset()
        cursor.execute(sql)

        connexion.commit()
else : 
    sys.stdout.write("La VM n'est pas la VM principale (is_main_ha_vm == 0)")

sys.stdout.write("PARTIE VM : \n\n\n")

ram = os.popen("grep MemTotal /proc/meminfo").read()
tmp = ram.split()

ram = "%.3f" % (int(tmp[1])/1000000) + "G"
sys.stdout.write("Memoire ram total : " + ram + " (avant opération : "+tmp[1] + ")\n")

stream = os.popen('cat /etc/issue')
output = stream.readlines()

tmp = output[0].split(" ")
version_ubuntu = tmp[0] + " " + tmp[1] + " " + tmp[2]

sys.stdout.write("version d'ubuntu de la VM : " + version_ubuntu + "\n")

disk_lines_array = os.popen("df -h | grep '/dev/sda1\|/dev/sdb1'").read().splitlines()
total_disk = ""
disk_free = ""

for line in disk_lines_array : 
    tmp = line.split()
    name_dossier = tmp[5]
    total_disk = total_disk + name_dossier + " : " + tmp[1] + ";"
    disk_free = disk_free + name_dossier + " : " + tmp[3] + ";"

sys.stdout.write("Total disk : " + total_disk + "\n")
sys.stdout.write("Free disk : " + disk_free + "\n")
stream = os.popen("sudo docker --version")
version_docker = stream.readlines()[0].split(" ")[2].replace(",","")

sys.stdout.write("Version de docker : " + version_docker + "\n\n")

stream = os.popen("sudo glusterfs --version") 
array_tmp = stream.readlines()

if(len(array_tmp)>1) : 
    version_gluster = "'" + array_tmp[0].split(" ")[1].replace("\n","") + "'"
    sys.stdout.write("Version de glusterfs : " + version_gluster + "\n\n")
else:
    sys.stdout.write("Standalone dockerise, pas de version de glusterfs \n\n")
    version_gluster = "NULL"

sql = "UPDATE VMS SET total_disk = '" + total_disk + "', free_disk = '" + disk_free + "', total_ram = '" + ram + "', version_ubuntu = '"+ version_ubuntu +"', version_docker = '" + version_docker + "', version_glusterfs=" + version_gluster + ", is_monitored="+str(is_monitored)+" WHERE id_vm = " + str(id_vm)
cursor.execute(sql)

connexion.commit()

connexion.close()

